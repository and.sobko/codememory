<?php

use System\Kernel\KernelLaunch;
use System\Routing\Router;

/**
 * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
 * & Connection file that contains all the functions
 * & of the framework
 * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
 *
 */
require_once __DIR__ . '/system/Libs/functions.php';

/**
 * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
 * & Handling all exceptions and running the framework
 * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
 */
try {
    $kernel = new KernelLaunch();
    $kernel->assembly();

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Assembling the router
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=
     */
    Router::callRouting();
} catch (ErrorException | ReflectionException $e) {
    exit($e->getMessage());
}
