<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define('ROOT', realpath(__DIR__) . '/..');

/**
 * Connect autoload file, for auto connection of classes
 */
require_once ROOT . '/vendor/autoload.php';

/**
 * Include a file in which Facade Aliases
 */
require_once ROOT . '/system/Kernel/Facades/Handler/classmap.php';

