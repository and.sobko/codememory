<?php

require './bootstrap.php';

/**
 * Connect the core of the framework
 */
require_once __DIR__ . '/../kernel.php';
