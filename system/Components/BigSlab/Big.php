<?php

namespace System\Components\BigSlab;

use File;
use System\Components\BigSlab\Compilers\CompilerConstructions;
use System\Components\BigSlab\Compilers\CompilerEchos;
use System\Components\BigSlab\Compilers\CompilerHTML;
use System\Components\BigSlab\Compilers\CompilerTypes;
use System\Components\BigSlab\Interfaces\BIGInterface;

/**
 * Class Big
 * @package System\Components\BigSlab
 *
 * @author  Codememory
 */
class Big extends SlabRules
{

    use CompilerEchos;
    use CompilerConstructions;
    use CompilerHTML;
    use CompilerTypes;

    /**
     * @var string
     */
    protected string $file;

    /**
     *
     * @var array
     */
    private array $parameters = [];

    /**
     * @var Caching
     */
    private Caching $caching;

    /**
     * @var array|Caching
     */
    private Caching|array $cache = [];

    /**
     * @var bool
     */
    private bool $withCache = false;

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method opens the file to be read. The system itself determines which file to open
     * & the cache or the virtual one
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     *
     * @param string $bigFile
     *
     * @return $this
     */
    public function open(string $bigFile): object
    {

        $this->file = $bigFile;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method turns on the template caching system
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param bool $caching
     *
     * @return object
     */
    public function onCache(bool $caching): object
    {

        $this->withCache = $caching;

        return $this;

    }

    /**
     * @return string
     */
    protected function pathFormation(): string
    {

        $path = str_replace('.', '/', $this->file);

        return $path . BIGInterface::EXPANSION;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & In this method, you need to pass an object of the Caching class so that the system could
     * & determine information about the cache and its saving
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param Caching $caching
     *
     * @return object
     */
    public function setCache(Caching $caching): object
    {

        $this->cache = [
            'caching' => $caching
        ];

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method is intended for passing variables to a template.
     * & "Key" - variable name => "variable value"
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $params
     *
     * @return $this
     */
    public function setParams(array $params): object
    {

        $this->parameters = $params;

        return $this;

    }

    /**
     * @param string $path
     * @param string $code
     */
    private function makeWithCache(string $path, string $code): void
    {

        if ($this->withCache === true && isset($this->cache['caching'])) {
            $this->cache = $this->cache['caching']
                ->setInformation(substr($code, 2), $path)
                ->save();
        }

    }

    /**
     * @return string|null
     */
    private function renderCode(): ?string
    {

        $code = File::read($this->pathFormation());
        $code = sprintf(
            '%s%s',
            '?>',
            $this->disassemble($code, File::read($this->pathFormation()))->getReplacedCode()
        );

        return $code;

    }

    /**
     * @param string $code
     *
     * @return mixed
     */
    private function render(string $code): mixed
    {

        if ($this->cache instanceof Caching) {
            if ($this->cache->loadFromCache === true) {
                return File::oneImport($this->cache->saved);
            }
        }

        ob_start();
        extract($this->parameters);
        eval($code);
        ob_get_flush();

        return null;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Launching debugger in case of error and if debugger is enabled in template engine settings
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return object
     * @throws Exceptions\IncorrectSettingException
     */
    private function withDebug(): object
    {

        if ($this->debugger !== null && $this->debug !== []) {
            $this->debugger->make();
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Handle system caching if cache system is zipped and cache information is specified
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param $code
     *
     * @return object
     */
    private function withCaching($code): object
    {

        if ($this->withCache === true) {
            $this->makeWithCache(File::getRealPath($this->pathFormation()), $code);
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method that starts the template engine
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return mixed
     * @throws Exceptions\IncorrectSettingException
     */
    public function make(): mixed
    {

        $code = $this->renderCode();

        $this->withDebug();
        $this->withCaching($code);

        return $this->render($code);

    }

}