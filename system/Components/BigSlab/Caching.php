<?php

namespace System\Components\BigSlab;

use File;
use System\Components\BigSlab\Interfaces\BIGInterface;
use System\FileSystem\Different\Exceptions\IncorrectPathException;

/**
 * Class Caching
 * @package System\Components\BigSlab
 *
 * @author  Codememory
 */
class Caching
{

    /**
     * @var string
     */
    private string $path;

    /**
     * @var bool
     */
    public bool $loadFromCache = true;

    /**
     * @var string[]
     */
    private array $information = [];

    /**
     * @var string
     */
    public string $saved;

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method will write information about the template so that the caching system
     * & can create a cache file. You shouldn't call this method
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $code
     * @param string $opened
     *
     * @return object
     */
    public function setInformation(string $code, string $opened): object
    {

        $this->information = [
            'code' => $code,
            'path' => $opened
        ];

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A method that takes 1 parameter, the path where the template cache will be saved
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return object
     */
    public function savePath(string $path): object
    {

        $this->path = str_replace('.', '/', trim($path, '/')) . '/';

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method will load data not from the virtual template but from the cache
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param bool $load
     *
     * @return $this
     */
    public function loadFromCache(bool $load): object
    {

        $this->loadFromCache = $load;

        return $this;

    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function renderCacheName(string $name): string
    {

        return str_replace(['.big', '/', '..'], ['', '.', '.'], $name);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Starting caching...
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return object
     * @throws IncorrectPathException
     */
    public function save(): object
    {

        if ($this->loadFromCache === true) {
            $path = $this->renderCacheName(
                $this->information['path'] . BIGInterface::EXPANSION
            );
            $defaultPath = sprintf('%s%s', $this->path, base64_encode($path . BIGInterface::EXPANSION));

            File::editor()->put($defaultPath, $this->information['code']);

            $this->saved = $defaultPath;
        }

        return $this;

    }

}