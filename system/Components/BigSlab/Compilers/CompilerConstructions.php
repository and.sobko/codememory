<?php

namespace System\Components\BigSlab\Compilers;

/**
 * Trait CompilerConstructions
 * @package System\Components\BigSlab\Compilers
 *
 * @author  Codememory
 */
trait CompilerConstructions
{

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerIf(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, function ($match) {
            return sprintf('<?php if (%s): ?>', $match[1]);
        }, $code);

    }

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerElseif(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn ($match) => sprintf('<?php elseif(%s): ?>', $match[1]), $code);

    }

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerElse(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn () => printf('<?php else: ?>'), $code);

    }

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerEndif(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn () => '<?php endif; ?>', $code);

    }

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerFor(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn ($match) => sprintf('<?php for (%s): ?>', $match[1]), $code);

    }

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerEndfor(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn () => sprintf('<?php endfor; ?>'), $code);

    }

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerClearPhp(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn () => '', $code);

    }

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    public function compilerTagsPhp(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn ($match) => sprintf('<?php %s; ?>', $match[1]), $code);

    }

}