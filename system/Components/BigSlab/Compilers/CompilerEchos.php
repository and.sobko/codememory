<?php

namespace System\Components\BigSlab\Compilers;

/**
 * Trait CompilerEchos
 * @package System\Components\BigSlab\Compilers
 *
 * @author  Codememory
 */
trait CompilerEchos
{

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerEcho(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn ($match) => sprintf('<?php echo htmlspecialchars(%s); ?>', $match[1]), $code);

    }

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerEchoRaw(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn ($match) => sprintf('<?php echo %s; ?>', $match[1]), $code);

    }

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerEchoRawBr(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn ($match) => sprintf('<?php echo nl2br(htmlspecialchars(%s)); ?>', trim($match[1])), $code);

    }

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerEchoBr(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn ($match) => sprintf('<?php echo nl2br(%s); ?>', $match[1]), $code);

    }

}