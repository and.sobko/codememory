<?php

namespace System\Components\BigSlab\Compilers;

/**
 * Trait CompilerHTML
 * @package System\Components\BigSlab\Compilers
 *
 * @author  Codememory
 */
trait CompilerHTML
{

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerSectionStart(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn ($match) => sprintf('<section section-name="%s">', $match[2] ?? md5(rand(1111, 99999))), $code);

    }

    /**
     * @param string $rules
     * @param        $code
     *
     * @return string|null
     */
    protected function compilerSectionEnd(string $rules, $code): ?string
    {

        return preg_replace_callback($rules, fn () => sprintf('</section>'), $code);

    }

}