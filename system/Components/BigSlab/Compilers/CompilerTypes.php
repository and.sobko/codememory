<?php

namespace System\Components\BigSlab\Compilers;

use System\Components\BigSlab\Compilers\Handling\TypeHandler;
use System\Components\BigSlab\Interfaces\Debugger\ErrorTypesInterface;

/**
 * Trait CompilerTypes
 * @package System\Components\BigSlab\Compilers
 *
 * @author  Codememory
 */
trait CompilerTypes
{

    private $allTypes = [
        'int',
        'string',
        'float',
        'bool',
        'var'
    ];

    /**
     * @param string $type
     * @param        $value
     *
     * @return bool|string|int
     */
    public function castToType(string $type, $value): bool|string|int
    {

        $value = match ($type) {
            'int', 'float' => preg_match('/^\"[0-9.]+\"$/', $value) ? substr($value, 1, -1) : $value,
            'string' => sprintf('"%s"', $value),
            'bool' => settype($value, 'boolean'),
            'var' => preg_match('/^\"\$[a-zA-Z0-9_]+\"$/', $value) ? substr($value, 1, -1) : $value,
            default => $value
        };

        return $value;

    }

    /**
     * @param       $code
     * @param array $templateArray
     *
     * @return string|null
     */
    protected function compilerArray($code, array $templateArray): ?string
    {

        preg_match_all('/{\s+(.*)\s+}/', $code, $match);

        $data = [];
        $matches = [];

        if ($match !== []) {
            foreach ($match[1] as $key => $array) {
                if (preg_match_all(
                    '/((?<type>[a-z]+\s+)?(?<key>[a-zA-Z_\-]+):\s+(?<value>[^,\s+\n]+))*/',
                    $array, $matches)) {

                    $this->line =
                        TypeHandler::CArraySetCode($templateArray)
                            ->CArrayRoomHandling($match[0][$key])
                            ->getLine();
                    $data['default'][] = $match[0][$key];
                    $assembly = null;

                    foreach ($matches['type'] as $typeKey => $type) {
                        if (!empty($type)) {
                            if (!in_array(trim($type), $this->allTypes)) {
                                $this->debugger = $this->debugger->setPosition(34);
                                $this->addMessageDebug(
                                    sprintf('Тип данных <b>%s</b> для массива не существует. Все типы данных: <b>[%s]</b>', trim($type), implode(', ', $this->allTypes))
                                )
                                    ->addTypeDebug(ErrorTypesInterface::E_BIG_TRAGIC);
                            }
                            $assembly .= TypeHandler::cArrayCollect($type, $matches, $typeKey, $this);
                        }
                    }

                    $data['processedString'][] = $assembly;
                }
            }

            if ($data !== []) {
                foreach ($data['processedString'] as $key => $assembledString) {
                    $string = sprintf('[%s]', substr($assembledString, 0, -1));
                    $default = $data['default'][$key];

                    $code = preg_replace_callback(sprintf('/%s/', preg_quote($default)), fn () => $string, $code);
                }
            }

        }

        return $code;

    }

}