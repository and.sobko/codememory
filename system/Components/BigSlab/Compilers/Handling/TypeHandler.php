<?php

namespace System\Components\BigSlab\Compilers\Handling;

/**
 * Class TypeHandler
 * @package System\Components\BigSlab\Compilers\Handling
 *
 * @author  Codememory
 */
final class TypeHandler
{

    /**
     * @var array
     */
    private static array $boilerplateCode = [];

    /**
     * @var int|string
     */
    private static int|string $line;

    /**
     * @param array $code
     *
     * @return object
     */
    final public static function CArraySetCode(array $code): object
    {

        self::$boilerplateCode = $code;

        return new self();

    }

    /**
     * @param string $templateString
     *
     * @return object
     */
    final public static function CArrayRoomHandling(string $templateString): object
    {

        foreach (self::$boilerplateCode as $line => $lineCode) {
            if (preg_match(sprintf('/%s/', $templateString), $lineCode)) {
                self::$line = ++$line;
            }
        }

        return new self();

    }

    /**
     * @param string     $type
     * @param array      $matches
     * @param int|string $typeKey
     * @param            $compiler
     *
     * @return string|null
     */
    final public static function cArrayCollect(string $type, array $matches, int|string $typeKey, $compiler): ?string
    {

        $type = trim($type);
        $keyName = $matches['key'][$typeKey] ?: null;
        $value = $matches['value'][$typeKey] ?: null;

        return sprintf('"%s" => %s,', $keyName, $compiler->castToType($type, $value));

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns the line number on which the error occurred
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     */
    final public static function getLine(): int
    {

        return self::$line;

    }

}