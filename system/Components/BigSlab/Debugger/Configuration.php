<?php

namespace System\Components\BigSlab\Debugger;

use System\Components\BigSlab\Exceptions\IncorrectSettingException;
use System\Components\BigSlab\Interfaces\Debugger\ErrorTypesInterface;
use Yaml;

/**
 * Class Configuration
 * @package System\Components\BigSlab\Debugger
 *
 * @author  Codememory
 */
class Configuration implements ErrorTypesInterface
{

    private const DISPLAY_ERRORS = true;

    private const TYPE = 'E_BIG_ALL';

    private const NUMBER_LINES = 20;

    private const CLOSE_FOLL_CODE = 'auto';

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns all template engine configuration from configs/packages/big.yaml
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    protected function getConfig(): array
    {

        return Yaml::open('configs/packages/big.yaml')
            ->parse();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns a boolean value. Is error output allowed
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     * @throws IncorrectSettingException
     */
    protected function getDisplay(): bool
    {

        return $this->valueValidation(
            (array_key_exists('display_errors', $this->getConfig())) ? $this->getConfig()['display_errors'] : self::DISPLAY_ERRORS,
            'is_bool true|false'
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns error types from configuration. By default E_BIG_ALL
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $allTypes
     *
     * @return array
     * @throws IncorrectSettingException
     */
    protected function getType(array $allTypes): array
    {

        if ($this->getConfig()['error']['types'] && $this->getConfig()['error']['types'] !== []) {
            $types = $this->getConfig()['error']['types'];
        } else {
            $types = constant('self::' . self::TYPE);
        }

        $delimiter = implode('|', $allTypes);
        $activeTypes = [];

        foreach ($types as $type) {
            $activeTypes[] = $this->valueValidation(
                $type,
                sprintf('is_int %s', $delimiter)
            );
        }

        return $activeTypes;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns the number of lines from the configuration to display in the error template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     * @throws IncorrectSettingException
     */
    protected function getNumberLines(): int
    {

        return $this->valueValidation(
            (array_key_exists('number_lines', $this->getConfig()['error'])) ? $this->getConfig()['error']['number_lines'] : self::NUMBER_LINES,
            'is_int '
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns the lock status of the following code from the configuration. The default is auto.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string|bool
     * @throws IncorrectSettingException
     */
    protected function getStatusFollCode(): string|bool
    {

        $status = (array_key_exists('close_following_code', $this->getConfig()['error'])) ? $this->getConfig()['error']['close_following_code'] : self::CLOSE_FOLL_CODE;

        return $this->valueValidation(
            $status,
            '- true|false|auto'
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Configuration value handler method
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param        $value
     * @param string $validation
     *
     * @return string
     * @throws IncorrectSettingException
     */
    private function valueValidation($value, string $validation): string
    {

        $explodeValidation = explode(' ', $validation);
        $values = null;

        [$is, $values] = $explodeValidation;

        $values = explode('|', $values);
        $value = match ($value) {
            false => 'false',
            true => 'true',
            default => $value
        };

        if ($is !== '-') {
            if (!$is($value) && ($values !== [] && !in_array($value, $values))) {
                throw new IncorrectSettingException();
            }
        } else {
            if (!in_array($value, $values)) {
                throw new IncorrectSettingException();
            }
        }

        return $value;

    }

}