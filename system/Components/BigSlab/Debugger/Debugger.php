<?php

namespace System\Components\BigSlab\Debugger;

use File;
use System\Components\BigSlab\Exceptions\IncorrectSettingException;
use System\Components\BigSlab\Exceptions\InvalidErrorTypeException;

/**
 * Class Debugger
 * @package System\Components\BigSlab
 *
 * @author  Codememory
 */
class Debugger extends Configuration
{

    protected const TYPES = [
        0 => 'E_BIG_ALL',
        3 => 'E_BIG_TOLERABLE',
        4 => 'E_BIG_WARNING',
        5 => 'E_BIG_TRAGIC'
    ];

    /**
     * @var int
     */
    private int $type = 0;

    /**
     * @var int|null
     */
    private ?int $line = null;

    /**
     * @var string|null
     */
    private ?string $file = null;

    /**
     * @var string|null
     */
    private ?string $message = null;

    /**
     * @var int|null
     */
    private ?int $position = null;

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Sets the type of error so the debugger can figure out what the error is and how to handle it
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $type
     *
     * @return $this
     * @throws InvalidErrorTypeException
     */
    public function setType(int $type): object
    {

        if (!array_key_exists($type, self::TYPES)) {
            throw new InvalidErrorTypeException($type);
        }

        $this->type = $type;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add the line number on which the error occurred. When an error occurs, the line in
     * & which the error will be highlighted
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $line
     *
     * @return $this
     */
    public function setLine(int $line): object
    {

        $this->line = $line;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Install the file in which the error occurred. When an error occurs, the debugger
     * & will read the entire file and look for the error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $file
     *
     * @return $this
     */
    public function setFile(string $file): object
    {

        $this->file = $file;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Adding an error message
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $message
     *
     * @return $this
     */
    public function setMessage(string $message): object
    {

        $this->message = $message;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method sets the position where the error occurred. If an error occurs, an arrow
     * & will be installed on the symbol (line), which will speak of the error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $position
     *
     * @return object
     */
    public function setPosition(int $position): object
    {

        $this->position = $position - 0;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Abbreviated method for creating error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $message
     * @param string $file
     * @param int    $line
     * @param int    $type
     *
     * @return $this
     * @throws InvalidErrorTypeException
     */
    public function declare(string $message, string $file, int $line, int $type = 0): object
    {

        $this
            ->setType($type)
            ->setFile($file)
            ->setLine($line)
            ->setMessage($message);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method that returns information for the error display template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     * @throws IncorrectSettingException
     */
    public function getData(): array
    {

        $line = $this->line - 1;
        $loaded = [
            'full' => $this->getNumberLines(),
            'half' => ceil($this->getNumberLines() / 2)
        ];

        $start = ($line < $loaded['half']) ? $line : ($line - $loaded['half']);
        $end = ($line < $loaded['half']) ? ($line + $loaded['full']) : ($line + $loaded['half']);

        return [
            'typeName'  => self::TYPES[$this->type],
            'typeInt'   => $this->type,
            'message'   => $this->message,
            'line'      => $line,
            'file'      => $this->file,
            'code'      => explode(PHP_EOL, $this->file ? File::read($this->file) : null),
            'loaded'    => $loaded,
            'startCode' => $start,
            'endCode'   => $end,
            'position'  => $this->position
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns a list of all available types
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string[]
     */
    public function getTypes(): array
    {

        return self::TYPES;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns the type of active error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     */
    public function getActiveType(): int
    {

        return $this->type;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method raises an error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws IncorrectSettingException
     */
    public function make(): void
    {

        $make = new Make($this);

        $make->make();

    }

}