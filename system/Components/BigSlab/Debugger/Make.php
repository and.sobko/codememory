<?php

namespace System\Components\BigSlab\Debugger;

use File;
use Response;
use System\Components\BigSlab\Exceptions\IncorrectSettingException;
use System\Components\BigSlab\Interfaces\Debugger\ErrorTypesInterface;

/**
 * Class Make
 * @package System\Components\BigSlab\Debugger
 *
 * @author  Codememory
 */
class Make extends Configuration
{

    /**
     * @var Debugger
     */
    private Debugger $debugger;

    /**
     * Make constructor.
     *
     * @param Debugger $debugger
     */
    public function __construct(Debugger $debugger)
    {

        $this->debugger = $debugger;

    }

    /**
     * =>=>>=>>=>>=>>=>>=>>=>>=>>=>>=>>=>>=>>=>>=>>=>>=>
     * & The method handles the type of debugger error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     * @throws IncorrectSettingException
     */
    private function handle(): bool
    {

        $activeType = $this->debugger->getActiveType();
        $types = $this->getType($this->debugger->getTypes());
        $make = false;

        foreach ($types as $typeNumber => $typeName) {
            if ($typeNumber === $activeType || $typeName == $types[ErrorTypesInterface::E_BIG_ALL]) {
                $make = true;
            }
        }

        return $make;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method handles the ban on the return of the following code after an error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     * @throws IncorrectSettingException
     */
    private function follCodeConditions(): bool
    {

        if ($this->getStatusFollCode() === 'auto') {
            return $this->debugger->getActiveType() === ErrorTypesInterface::E_BIG_WARNING || $this->debugger->getActiveType() === ErrorTypesInterface::E_BIG_TOLERABLE ? false : true;
        } else {
            return $this->getStatusFollCode() === 'true';
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method connects the template and passes information about the error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws IncorrectSettingException
     */
    private function callTemplate(): void
    {

        ob_start();
        extract($this->debugger->getData());

        require File::getRealPath('system/Components/BigSlab/Debugger/template.php');

        if ($this->follCodeConditions()) {
            exit;
        }

        ob_end_flush();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Start the debugger if everything has an error and the configuration is configured correctly
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws IncorrectSettingException
     */
    public function make(): void
    {

        if ($this->getDisplay() === true) {
            if ($this->handle() === true) {
                Response::setResponseCode(500)
                    ->getResponseCode();

                $this->callTemplate();
            }
        }

    }

}