<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>500 Server Error</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.4.1/styles/default.min.css">
    <link rel="stylesheet" href="https://highlightjs.org/static/demo/styles/a11y-dark.css">
</head>
<body>

<div class="container">
    <div class="title">
        <h3><?php echo $typeName; ?>:</h3>
        <br>
        <span><?php echo $message; ?></span>
    </div>
    <div class="coding">
        <span class="file"><b>File with errors:</b> <span class="path"><?php echo $file; ?></span></span>
        <hr>
        <span class="file"><b>Line with errors:</b> <span class="num"><?php echo $line + 1; ?></span></span>
        <hr>
        <pre>
            <code>
            <?php
            for ($i = $startCode; $i <= $endCode; $i++) {
                if ($line == $i) {
                    echo sprintf(
                        '<span line="%s" class="line-error"><div style="position: absolute;left: calc(55px + 10px * %s);">%s</div><span>%s</span></span>',
                        ($i + 1), $position, $position ? '^' : '', htmlspecialchars($code[$i])
                    );
                } else {
                    if (isset($code[$i])) {
                        echo sprintf(
                            '<span line="%s"><span>%s</span></span>',
                            ($i + 1), htmlspecialchars($code[$i])
                        );
                    }
                }
            }

            if (array_key_last($code) > $endCode) {
                echo '<span>...</span>';
            }
            ?>
            </code>
        </pre>
    </div>
</div>

<style>
    * {
        padding: 0;
        margin: 0;
    }

    body {
        background: rgba(68, 68, 68, .9);
    }

    hr {
        border: none;
        border-bottom: 1px solid #4e4c4c;
    }

    .container {
        max-width: 1200px;
        margin: auto;
    }

    .title {
        background: #b31717;
        font-family: sans-serif;
        padding: 10px 30px;
        color: #fff;
    }

    code {
        width: 100%;
        position: absolute;
        top: 72px;
        display: grid !important;
        white-space: pre-wrap;
    }

    code:hover span {
        transition: 0.2s;
        opacity: 1;
    }

    .num {
        color: olive;
    }

    .coding {
        border: 1px solid #3c3333;
        background: #2d2d2d;
        border-bottom-right-radius: 10px;
        border-bottom-left-radius: 10px;
        overflow-x: hidden;
        position: relative;
    }

    .path {
        color: cyan;
    }

    pre {
        font-size: 17px;
        width: 100%;
        padding: 14px;
        line-height: 25px;
        box-sizing: border-box;
        color: #717171;
        counter-reset: line;
        padding-top: 0px;
        background: transparent;
    }

    pre {
        counter-reset: line;
    }

    code > span {
        counter-increment: line;
        position: relative;
        transition: 0.2s;
        opacity: 0.3;
    }

    code > span:before {
        content: attr(line);
        display: inline-block;
        width: 1.5em; /* Fixed width */
        border-right: 1px solid #ddd;
        padding: 0 .5em;
        margin-right: .5em;
        color: #888;
        -webkit-user-select: none;
        border-right: 5px solid green;
    }

    span.line-error:before {
        border-right: 5px solid red;
        background: #2b2b2b;
        color: #fff;
    }

    .line-error {
        opacity: 1;
    }

    .line-error > span {
        position: relative;
        background: #ff0000a6;
        color: #fff;
        padding: 2px 0px;
        font-weight: 400;
    }

    .line-error > span * {
        padding: 0 6px;
    }

    span.line-error:after {
        content: "<- ERROR";
        position: absolute;
        transform: translateX(10px);
        font-weight: 400;
        color: #fff;
        font-size: 13px
    }

    .file {
        color: #fff;
        font-family: sans-serif;
        width: 100%;
        padding: 8px 18px;
        display: block;
        letter-spacing: 1px;
    }
</style>
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.4.1/highlight.min.js"></script>
<script>
    hljs.initHighlightingOnLoad();

    setTimeout(() => {
        let code = document.querySelectorAll('code > span');
        let heightCode = document.querySelector('code').offsetHeight;

        code.forEach((el) => {
            if (el.getAttribute('class') && el.getAttribute('class') !== null && el.getAttribute('class') !== 'line-error') {
                el.remove();
            }
        });
        document.querySelector('.coding').style.height = (heightCode + 80) + 'px';

    }, 100);
</script>
</body>
</html>