<?php


namespace System\Components\BigSlab\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class IncorrectSettingException
 * @package System\Components\BigSlab\Exceptions
 *
 * @author  Codememory
 */
class IncorrectSettingException extends ErrorException
{

    /**
     * IncorrectSettingException constructor.
     */
    #[Pure] public function __construct()
    {

        parent::__construct('The configuration of the big template engine is not configured correctly');

    }

}