<?php


namespace System\Components\BigSlab\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class InvalidErrorTypeException
 * @package System\Components\BigSlab\Exceptions
 *
 * @author  Codememory
 */
class InvalidErrorTypeException extends ErrorException
{

    /**
     * InvalidErrorTypeException constructor.
     *
     * @param string $const
     */
    #[Pure] public function __construct(string $const)
    {

        parent::__construct(sprintf(
            'Constants %s do not exist in ErrorTypesInterface',
            $const
        ));

    }

}