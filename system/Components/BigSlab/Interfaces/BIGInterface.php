<?php

namespace System\Components\BigSlab\Interfaces;

/**
 * Interface BIGInterface
 * @package System\Components\BigSlab\Interfaces
 *
 * @author Codememory
 */
interface BIGInterface
{

    const EXPANSION = '.big';

}