<?php

namespace System\Components\BigSlab\Interfaces\Debugger;

/**
 * Interface ErrorTypesInterface
 * @package System\Components\BigSlab\Interfaces\Debugger
 *
 * @author Codememory
 */
interface ErrorTypesInterface
{

    const E_BIG_TRAGIC = 5;

    const E_BIG_TOLERABLE = 3;

    const E_BIG_WARNING = 4;

    const E_BIG_ALL = 0;

}