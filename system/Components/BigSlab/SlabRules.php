<?php

namespace System\Components\BigSlab;

use System\Components\BigSlab\Debugger\Debugger;

/**
 * Class SlabRules
 * @package System\Components\BigSlab
 *
 * @author  Codememory
 */
class SlabRules
{

    /**
     * @var mixed
     */
    private $recedeCode;

    /**
     * @var int|null
     */
    protected ?int $line = null;

    /**
     * @var array
     */
    protected array $debug = [];

    /**
     * @var Debugger|null
     */
    protected ?Debugger $debugger = null;

    /**
     * & List of components for the template engine
     */
    private const RULES = [
        'clearPhp'     => '/\<\?(php|\=)?((\n)*?.*)*\?\>/U',
        'echoRawBr'    => '/\[\[\s+(.*)(\s+)?\|(\s+)?raw(\s+)?\.break\s+\]\]/',
        'echoRaw'      => '/\[\[\s+(.*)(\s+)?\|(\s+)?raw\s+\]\]/',
        'echoBr'       => '/\[\[\s+(.*)(\s+)?\|(\s+)?break\s+\]\]/',
        'echo'         => '/\[\[\s+(.*)\s+\]\]/',
        'if'           => '/\[\@if\s+(.*)\]/',
        'elseif'       => '/\[\@elseIf\s+(.*)\]/',
        'else'         => '/\[\@else\]/',
        'endif'        => '/\[\@endIf\]/',
        'for'          => '/\[\@for\s+(.*)\]/',
        'endfor'       => '/\[\@endFor\]/',
        'tagsPhp'      => '/\[@php\s+(.*)]/',
        'sectionStart' => '/\[@section(\s+\=\s+([a-zA-Z0-9_\-]+))?\]/',
        'sectionEnd'   => '/\[@endSection\]/',
        'array',
        // script - [@script/build = libs.mousemove]
        // css - [@css/build = libs.mousemove]
    ];

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method adds a message to the debugger
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $message
     *
     * @return object
     */
    protected function addMessageDebug(string $message): object
    {

        $this->debug['message'] = $message;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Sets the type of error. By default E_BIG_ALL
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $type
     *
     * @return object
     */
    protected function addTypeDebug(int $type): object
    {

        $this->debug['type'] = $type;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns a compiler method in which actions are described from scratch
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $method
     * @param string|null $code
     * @param array       $arrayCode
     *
     * @return string|null
     */
    private function fullFind(string $method, ?string $code, array $arrayCode): ?string
    {

        $compilerMethod = sprintf('compiler%s', ucfirst($method));

        return call_user_func_array([$this, $compilerMethod], ['code' => $code, 'templateArray' => $arrayCode]);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns the compiler method if regular expression matches are found in the pattern
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $rules
     * @param string      $method
     * @param string|null $code
     * @param array       $arrayCode
     *
     * @return string|null
     */
    private function findByRegex(string $rules, string $method, ?string $code, array $arrayCode): ?string
    {

        preg_match($rules, $code, $matches);

        $compilerMethod = 'compiler' . ucfirst($method);

        return call_user_func_array([$this, $compilerMethod], [$rules, $code, $arrayCode]);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Disassembling regular expressions and calling certain methods to process the
     * & template engine component
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param $code
     * @param $oldCode
     *
     * @return object
     */
    protected function disassemble($code, $oldCode): object
    {

        $this->debugger = new Debugger();

        $explodeOld = explode(PHP_EOL, $oldCode);

        foreach (self::RULES as $method => $rulesOrNameMethod) {
            if (is_int($method)) {
                $code = $this->fullFind($rulesOrNameMethod, $code, $explodeOld);
            } else {
                $code = $this->findByRegex($rulesOrNameMethod, $method, $code, $explodeOld);
            }
        }

        $this->recedeCode = $code;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method processes a debugger if an error occurs in the template and this error is handled
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws Exceptions\InvalidErrorTypeException
     */
    protected function debugSetup(): void
    {

        if ($this->debug !== []) {
            $this->debugger =
                $this->debugger
                    ->setMessage($this->debug['message'])
                    ->setType($this->debug['type'])
                    ->setFile($this->pathFormation())
                    ->setLine($this->line);
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns a line of ready code from the template engine to the program code
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return mixed
     * @throws Exceptions\InvalidErrorTypeException
     */
    protected function getReplacedCode(): mixed
    {
        $this->debugSetup();

        return $this->recedeCode;

    }

}