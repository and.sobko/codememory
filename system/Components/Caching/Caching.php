<?php

namespace System\Components\Caching;

use File;
use System\Components\Caching\Interfaces\CachingInterface;
use System\FileSystem\Different\Exceptions\IncorrectPathException;

/**
 * Class Caching
 * @package System\Components\Caching
 *
 * @author  Codememory
 */
class Caching extends Configuration implements CachingInterface
{

    /**
     * @var string|null
     */
    private ?string $type = null;

    /**
     * @var string|null
     */
    private ?string $content = null;

    /**
     * @var array
     */
    private array $lastHistoryData = [];

    /**
     * @var bool
     */
    private bool $createOnlyMeta = false;

    /**
     * @return History
     */
    private function history(): History
    {

        return new History(
            $this->pathSaveHistory(),
            $this->historyFilename(),
            $this->expansionHistoryFile(),
            $this->historyFormatDate(),
            $this->createNameCacheFile(),
            $this->normalPathCacheFile()
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the type, i.e. the name of the folder that will be created inside the main cache folder
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $type
     *
     * @return Caching
     */
    public function type(string $type): Caching
    {

        $this->type = $type;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Content that will be overwritten or created when the cache is updated
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $content
     *
     * @return Caching
     */
    public function content(string $content): Caching
    {

        $this->content = $content;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get information about the last cache by type including its full path to the cache file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $type
     *
     * @return array|null
     */
    private function getDataByType(string $type): array|null
    {

        $data = $this->history()->getLastCacheInfo($type);

        if([] !== $data) {
            return array_merge(
                $data,
                [
                    'path' => sprintf('%s/%s/%s.php', $this->pathCache(), $type, $data['hash'])
                ]
            );
        }

        return null;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get cache from main php file, there should be return in cache file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $type
     *
     * @return mixed
     */
    public function getCache(string $type): mixed
    {

        $data = $this->getDataByType($type);

        if(null !== $data) {
            return File::getImport($data['path']);
        }

        return null;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get cache text from meta file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $type
     *
     * @return mixed
     */
    public function getMetaCache(string $type): mixed
    {

        $data = $this->getDataByType($type);

        return File::read($data['path']);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Create only meta file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     */
    public function createOnlyMeta(): Caching
    {

        $this->createOnlyMeta = true;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Start the caching system which will create the necessary cache files
     * & or update the cache history
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     * @throws IncorrectPathException
     */
    public function make(): bool
    {

        $this->lastHistoryData = $this->history()->getLastCacheInfo($this->type);

        $this->createFileCache()
            ->createHistory($this->history());

        return true;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A method to clear the entire cache history and when the cache is updated again,
     * & it will be created again
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     */
    public function clearHistory(): Caching
    {

        $this->history()->clearHistory();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method generates a name for the file that will contain the cache content.
     * & If in the configuration caching the key: "createNewFile" is true, then the cache
     * & will be overwritten in the previously created file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    private function createNameCacheFile(): string
    {

        $generatedName = hash($this->hashFileCache(), sprintf(
            '%s/%s/%s', $this->pathCache(), $this->type, $this->content
        ));

        if (!$this->createNewFileContent()) {

            if ([] !== $this->lastHistoryData) {
                return $this->lastHistoryData['hash'];
            }

            return $generatedName;
        }

        return $generatedName;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method is similar to "createNameCacheFile" only this method does not hash but
     * & displays the full path
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    private function normalPathCacheFile(): string
    {

        $path = sprintf(
            '%s%s/%s.',
            $this->pathCache(),
            $this->type,
            $this->createNameCacheFile()
        );

        if(File::exists($path.'php')) {
            return $path.'php';
        } else {
            return $path.'meta';
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method creates files for caching and retrieves content in them.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return Caching
     * @throws IncorrectPathException
     */
    private function createFileCache(): Caching
    {

        $pathWithType = $this->pathCache() . $this->type;

        if (!File::is()->isDir($pathWithType)) {
            File::mkdir($pathWithType, 0777, true);
        }

        if(false === $this->createOnlyMeta) {
            File::editor()->put(
                sprintf('%s/%s.php', $pathWithType, $this->createNameCacheFile()),
                $this->content
            );
        }

        if ($this->createMetaFile() || true === $this->createOnlyMeta) {
            File::editor()->put(
                sprintf('%s/%s.meta', $pathWithType, $this->createNameCacheFile()),
                $this->metaSerialize() ? serialize($this->content) : $this->content
            );
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method is creating a story that collects the desired methods
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param History $history
     *
     * @throws IncorrectPathException
     */
    private function createHistory(History $history)
    {

        $history
            ->createHistoryFile()
            ->setType($this->type)
            ->recordHistory();

    }

}