<?php

namespace System\Components\Caching\Commands;

use File;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use System\FileSystem\Different\Exceptions\IncorrectPathException;
use Yaml;
use Symfony\Component\Console\Style\SymfonyStyle;
use System\Components\Caching\Caching;
use System\FileSystem\Config\Configuration\Config;

/**
 * Class ConfigCommand
 * @package System\Components\Caching\Commands
 *
 * @author  Codememory
 */
class ConfigCommand extends Command
{

    private const PACKAGE_PATH = 'configs/packages/';
    private const CONFIG_PATH = 'configs/';
    private const NAME_CONFIG = '/^[a-z0-9]+\.yaml$/';

    /**
     * @var array
     */
    private array $packageData = [];

    /**
     * @var array
     */
    private array $configData = [];

    /**
     * @var array
     */
    private array $scanningFile = [];

    protected function configure(): void
    {

        $this->setName('cache:config')
            ->addOption('all', null, InputOption::VALUE_NONE, 'Обновить весь кэш конфигурации')
            ->addOption('package', null, InputOption::VALUE_NONE, 'Обновить кэш конфигурации пакетов')
            ->setDescription('Обновление кэша конфигурации');

    }

    /**
     * @param Caching $cache
     *
     * @return ConfigCommand
     * @throws IncorrectPathException
     */
    private function all(Caching $cache): ConfigCommand
    {

        $this->config(self::CONFIG_PATH)
             ->package(self::PACKAGE_PATH)
             ->createConfigCache($cache)
             ->createPackageConfigCache($cache);

        return $this;

    }

    /**
     * @param string $path
     *
     * @return ConfigCommand
     */
    private function package(string $path): ConfigCommand
    {

        if (File::is()->isFile($path)) {
            $splitPath = explode('/', substr($path, 0, -1));

            if (preg_match(self::NAME_CONFIG, $splitPath[array_key_last($splitPath)])) {
                $this->packageData = array_merge($this->packageData, Yaml::open($path)->parse());

                $this->scanningFile[] = substr($path, 0, -1);
            }
        } else {
            $scan = File::scanning($path);

            if([] !== $scan) {
                foreach ($scan as $dirOrFile) {
                    $this->package($path.$dirOrFile.'/');
                }
            }
        }

        return $this;

    }

    /**
     * @param string $path
     *
     * @return ConfigCommand
     */
    private function config(string $path): ConfigCommand
    {

        $scan = File::scanning($path);

        if([] !== $scan) {
            foreach ($scan as $filename) {
                if (File::is()->isFile($path.$filename)) {
                    if (preg_match(self::NAME_CONFIG, $filename)) {
                        $this->configData = array_merge($this->configData, Yaml::open($path.$filename)->parse());

                        $this->scanningFile[] = $path.$filename;
                    }
                }
            }
        }

        return $this;

    }

    /**
     * @param Caching $cache
     * @param string  $type
     * @param string  $perkType
     * @param array   $data
     *
     * @return ConfigCommand
     * @throws IncorrectPathException
     */
    private function handlerCreateCache(Caching $cache, string $type, string $perkType, array $data): ConfigCommand
    {

        $cache
            ->type(sprintf($type, $perkType))
            ->content(json_encode($data))
            ->createOnlyMeta()
            ->make();

        return $this;

    }

    /**
     * @param Caching $cache
     *
     * @return ConfigCommand
     * @throws IncorrectPathException
     */
    private function createConfigCache(Caching $cache): ConfigCommand
    {

        $this->handlerCreateCache($cache, Config::CONFIG_CACHE_KEY, Config::TYPE_DEFAULT_CACHE_CONFIG, $this->configData);

        return $this;

    }

    /**
     * @param Caching $cache
     *
     * @return ConfigCommand
     * @throws IncorrectPathException
     */
    private function createPackageConfigCache(Caching $cache): ConfigCommand
    {

        $this->handlerCreateCache($cache, Config::CONFIG_PACKAGES_CACHE_KEY, Config::TYPE_PACKAGE_CACHE_CONFIG, $this->packageData);

        return $this;

    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws IncorrectPathException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);
        $cache = new Caching();

        if($input->getOption('package')) {
            $this->package(self::PACKAGE_PATH)
                ->createPackageConfigCache($cache);
        } else if($input->getOption('all')) {
            $this->all($cache);
        } else {
            $this->config(self::CONFIG_PATH)
                ->createConfigCache($cache);
        }

        $io
            ->success(
                [
                    sprintf('Кэш обновлен для %s файлов', count($this->scanningFile)),
                    sprintf(
                        'Файлы для которых обновлен кэш: %s%s',
                        PHP_EOL,
                        implode(PHP_EOL, $this->scanningFile)
                    )
                ]
            );

        return Command::FAILURE;

    }

}