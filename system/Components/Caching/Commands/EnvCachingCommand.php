<?php

namespace System\Components\Caching\Commands;

use Env;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use File;
use System\FileSystem\Different\Exceptions\IncorrectPathException;
use System\Components\Caching\Caching;

/**
 * Class EnvCachingCommand
 * @package System\Components\Caching\Commands
 *
 * @author  Codememory
 */
class EnvCachingCommand extends Command
{

    const PATH_CACHE = 'storage/cache/Environment';

    protected function configure(): void
    {

        $this->setName('cache:env')
            ->addOption('clear', null, InputOption::VALUE_NONE, 'Очистить кэш')
            ->setDescription('Обновление кэша для переменн окружений(<fg=yellow>.env</>)');

    }

    /**
     * @param SymfonyStyle    $io
     * @param OutputInterface $output
     *
     * @throws IncorrectPathException
     */
    private function clear(SymfonyStyle $io, OutputInterface $output): void
    {

        if(File::is()->isDir(self::PATH_CACHE)) {
            $size = File::info()->getSize(self::PATH_CACHE);

            File::remove(self::PATH_CACHE, true);
            File::remove(self::PATH_CACHE, false);

            $io->success(
                [
                    'Кэш Env был успешно удален',
                    sprintf(
                        'Размер кэша составил: %s byte', $size
                    )
                ]
            );
            exit;
        }

        $output->writeln("<fg=yellow>\nКэш Env был удален ранее.\n</>");

    }

    /**
     * @param SymfonyStyle    $io
     * @param OutputInterface $output
     * @param Caching         $caching
     *
     * @throws IncorrectPathException
     */
    private function update(SymfonyStyle $io, OutputInterface $output, Caching $caching): void
    {

        $output->writeln("<fg=red>\nОбновление кэша...\n</>");

        $cache = '<?php return [%s];';
        $env = Env::getAll();
        $envString = null;

        foreach ($env as $group => $vars) {
            $varsGroupString = null;

            foreach ($vars as $key => $value) {
                $varsGroupString .= sprintf("\"%s\" => \"%s\",", $key, $value);
            }
            $envString .= sprintf("\"%s\" => [%s],", $group, substr($varsGroupString, 0, -1));
        }

        $updated = $caching->type('Environment')
            ->content(sprintf($cache, $envString))
            ->make();

        if($updated) {
            $io->success('Кэш успешно обновлен');
        }

    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws IncorrectPathException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);

        if($input->getOption('clear')) {
            $this->clear($io, $output);
        }

        $this->update($io, $output, new Caching());

        return Command::FAILURE;

    }

}