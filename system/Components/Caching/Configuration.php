<?php

namespace System\Components\Caching;

use Yaml;

/**
 * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
 * & This is a getter class with which you can get information from the main caching
 * & configuration file
 * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
 *
 * Class Configuration
 * @package System\Components\Caching
 *
 * @author Codememory
 */
class Configuration
{

    private const PATH_SAVE_CACHE = 'storage.cache';
    private const HASH_FILE_CACHE = 'md5';
    private const CREATE_META_FILE = true;

    private const CONTENT_META_SERIALIZE = false;

    private const PATH_SAVE_HISTORY = 'storage.cache.history';
    private const HISTORY_FILENAME = 'cache';
    private const EXPANSION_HISTORY_FILE = 'yaml';
    private const HISTORY_FORMAT_DATE = 'Y-m-d H:i';
    private const CREATE_NEW_FILE_CONTENT = true;

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns an array of all caching configurations from configs/packages/cache/conf.yaml
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $keys
     *
     * @return mixed
     */
    public function conf(?string $keys = null): mixed
    {

        $data = Yaml::open('configs/packages/cache/conf.yaml')->parse()['cache'];

        if(null !== $keys) {
            foreach (explode('.', $keys) as $key) {
                if(array_key_exists($key, $data)) {
                    $data = $data[$key];
                } else {
                    $data = null;
                }
            }
        }

        return $data;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method changes the text of the path to the normal type, i.e. instead of
     * dots, it substitutes a slash
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return string
     */
    private function correctPath(string $path): string
    {

        $replacedPath = str_replace('.', '/', $path);

        return trim($replacedPath, '/').'/';

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the path where the cache will be saved
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    public function pathCache(): string
    {

        return $this->correctPath(
            $this->conf('path') ?? self::PATH_SAVE_CACHE
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the name of the algorithm by which the file name for the cache should be stitched
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    public function hashFileCache(): string
    {

        return $this->conf('hashName') ?? self::HASH_FILE_CACHE;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get a status that determines whether to create an additional cache file with the .meta extension
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function createMetaFile(): bool
    {

        return $this->conf('createMetaFile') ?? self::CREATE_META_FILE;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get a status that determines whether to write data to a .meta file in serialize
     * & format, this option will be triggered if the option to create meta files is set to true
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function metaSerialize(): bool
    {

        return $this->conf('content.meta.serialize') ?? self::CONTENT_META_SERIALIZE;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get a status that will determine whether to create new cache files if the cache content
     * & is updated in case of false the cache will be updated in the last created cache file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function createNewFileContent(): bool
    {
        
        return $this->conf('content.createNewFile') ?? self::CREATE_NEW_FILE_CONTENT;
        
    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the path where the cache history will be created, in the absence of
     * & this path, the path will be created automatically when updating any cache
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    public function pathSaveHistory(): string
    {

        return $this->correctPath(
            $this->conf('history.path') ?? self::PATH_SAVE_HISTORY
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the name of the file that will store the entire caching history
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    public function historyFilename(): string
    {

        return $this->conf('history.filename') ?? self::HISTORY_FILENAME;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get file extension for history
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    public function expansionHistoryFile(): string
    {

        return self::EXPANSION_HISTORY_FILE;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the date format that will be applied when creating/updating the cache
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    public function historyFormatDate(): string
    {

        return $this->conf('history.date.format') ?? self::HISTORY_FORMAT_DATE;

    }

}