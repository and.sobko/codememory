<?php

namespace System\Components\Caching\Interfaces;

use System\Components\Caching\Caching;

/**
 * Interface CachingInterface
 * @package System\Components\Caching\Interfaces
 *
 * @author  Codememory
 */
interface CachingInterface
{

    /**
     * @param string $type
     *
     * @return Caching
     */
    public function type(string $type): Caching;

    /**
     * @param string $content
     *
     * @return Caching
     */
    public function content(string $content): Caching;

    /**
     * @return bool
     */
    public function make(): bool;

}