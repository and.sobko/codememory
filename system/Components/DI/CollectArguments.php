<?php

namespace System\Components\DI;

use ReflectionClass;
use ReflectionException;

/**
 * Class CollectArguments
 * @package System\Components\DI
 */
class CollectArguments extends TypeSpecifier
{

    /**
     * @param array $parameters
     *
     * @return $this
     * @throws ReflectionException
     */
    public function setParameters(array $parameters = []): object
    {

        if ($parameters !== []) {
            foreach ($parameters as $index => $parameter) {
                $this->parameters[] = $this->assembly($parameter);
            }
        }

        return $this;

    }

    /**
     * @param $parameter
     *
     * @return array
     * @throws ReflectionException
     */
    private function assembly($parameter): array
    {

        $type = $parameter->getType();
        $parameters = [
            'name'        => $parameter->getName(),
            'package'     => null,
            'isBuiltin'   => false,
            'position'    => $parameter->getPosition(),
            'isInterface' => false
        ];

        if ($parameter->hasType() && ($type->isBuiltin()) === false) {
            $package = $type->getName();
            $reflection = new ReflectionClass($package);

            $parameters['isInterface'] = $reflection->isInterface();
            $parameters['package'] = $package;
        }

        if ($parameter->hasType() === false || $type->isBuiltin() === true) {
            $parameters['isBuiltin'] = true;
        }

        return $parameters;

    }

}