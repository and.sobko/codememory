<?php

namespace System\Components\DI\DIElements;

use ReflectionException;
use System\Components\DI\Container;
use System\Components\DI\Interfaces\ElementsInterface;

/**
 * @method setName(string $name)
 *
 * Class FunctionDependencies
 * @package System\Components\DI\DIElements
 *
 * @author  Codememory
 */
class FunctionDependencies extends Container implements ElementsInterface
{

    private const TYPE = 'callback';

    /**
     * @var array
     */
    private array $readyDep = [];

    /**
     * ObjectDependency constructor.
     */
    public function __construct()
    {

        $this->setType(self::TYPE);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add callback or function to DI trace
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param callable $callback
     *
     * @return $this
     */
    public function setCallback(callable $callback): object
    {

        $this->register($callback);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A handler method that collects all arguments and returns an array with data
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     * @throws ReflectionException
     */
    private function assembly(): array
    {
        $data = [];
        $dependencies = [
            'callback' => []
        ];

        if (array_key_exists('callback', $this->assembled) && $this->assembled['callback'] !== []) {
            foreach ($this->assembled['callback'] as $name => $object) {
                $this->parameters = [];
                $reflection = $this->getReflection($object['registered']);
                $dependencies['callback'] = $this->setParameters($reflection->getParameters())->parameters;

                $data[$name] = $this->assembled['callback'][$name];
                $data[$name]['parameters'] = $dependencies;
            }
        }

        return $data;

    }

    /**
     * Build
     *
     * @return $this
     * @throws ReflectionException
     */
    public function execute(): object
    {

        $this->readyDep = $this->getDependencies($this->assembly());

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns the finished result of the processed callback | function by name
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return mixed
     * @throws ReflectionException
     */
    public function get(string $name): mixed
    {

        $callback = $this->readyDep[$name]['registered'];
        $arguments = $this->readyDep[$name]['callback'];

        $reflection = $this->getReflection($callback);

        return $reflection->invokeArgs($arguments);

    }

}