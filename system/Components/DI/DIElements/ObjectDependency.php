<?php

namespace System\Components\DI\DIElements;

use ReflectionException;
use System\Components\DI\Container;
use System\Components\DI\Exceptions\MethodNotExistsException;
use System\Components\DI\Interfaces\ElementsInterface;

/**
 * @method setName(string $name)
 *
 * Class ObjectDependency
 * @package System\Components\DI\DIElements
 *
 * @author  Codememory
 */
class ObjectDependency extends Container implements ElementsInterface
{

    private const TYPE = 'object';
    private const WITH_CONSTRUCT = true;

    /**
     * @var array
     */
    private array $readyDep = [];

    /**
     * @var array
     */
    private array $withObjects = [];

    /**
     * ObjectDependency constructor.
     */
    public function __construct()
    {

        $this->setType(self::TYPE);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method controls the constructor of the class. Takes 1 argument boolean value
     * & True - the constructor will not be tracked
     * & false - otherwise
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param bool $with
     *
     * @return $this
     */
    public function withConstructor(bool $with): object
    {

        $this->additionalCollectKeys['withConstruct'] = $with;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method sets the namespace of the class to track.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $namespace
     *
     * @return $this
     */
    public function setObject(string $namespace): object
    {

        $this->register($namespace);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method sets the method of the class to be tracked. If you don't specify a method.
     * & The container will return a regular instance of the class
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $method
     *
     * @return $this
     */
    public function setMethod(string $method): object
    {

        $this->additionalCollectKeys['method'] = $method;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Collecting class constructor information
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param $reflection
     *
     * @return array
     * @throws ReflectionException
     */
    private function assemblyConstructor($reflection): array
    {

        $this->parameters = [];

        return $this->setParameters(
            $reflection
                ->getMethod('__construct')
                ->getParameters()
        )->parameters;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Collecting class method information
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param $reflection
     * @param $method
     *
     * @return array
     * @throws ReflectionException|MethodNotExistsException
     */
    private function assemblyMethod($reflection, $method): array
    {

        if (!method_exists($reflection->getName(), $method)) {
            throw new MethodNotExistsException($method, $reflection->getName());
        }
        $this->parameters = [];

        return $this->setParameters(
            $reflection
                ->getMethod($method)
                ->getParameters()
        )->parameters;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A handler method that collects all arguments and returns an array with data
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     * @throws ReflectionException|MethodNotExistsException
     */
    private function assembly(): array
    {
        $data = [];
        $dependencies = [
            'constructor' => [],
            'method'      => []
        ];

        if (array_key_exists('object', $this->assembled) && $this->assembled['object'] !== []) {
            foreach ($this->assembled['object'] as $name => $object) {
                $reflection = $this->getReflection($object['registered']);
                $method = $object['method'] ?? null;
                $constructor = array_key_exists('withConstruct', $object) ? $object['withConstruct'] : self::WITH_CONSTRUCT;

                if ($constructor === true && $reflection->hasMethod('__construct')) {
                    $dependencies['constructor'] = $this->assemblyConstructor($reflection);

                    $this->withObjects[$name]['constructor'] = true;
                }

                if ($method !== null) {
                    $dependencies['method'] = $this->assemblyMethod($reflection, $method);

                    $this->withObjects[$name]['method'] = true;
                }

                $data[$name] = $this->assembled['object'][$name];
                $data[$name]['parameters'] = $dependencies;
            }
        }

        return $data;

    }

    /**
     * Build
     *
     * @return object
     * @throws MethodNotExistsException
     * @throws ReflectionException
     */
    public function execute(): object
    {

        $this->readyDep = $this->getDependencies($this->assembly());

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The get method gets any object or function, by the name that was specified
     * & when creating the track
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return mixed
     * @throws ReflectionException
     */
    public function get(string $name): mixed
    {

        $object = $this->readyDep[$name]['registered'];

        if (is_string($object)) {
            $object = $this->getReflection($object);
        }

        if (isset($this->withObjects[$name]['method']) && isset($this->withObjects[$name]['constructor'])) {
            $reflection = $object->newInstanceArgs($this->readyDep[$name]['constructor']);

            return call_user_func_array([$reflection, $this->readyDep[$name]['methodName']], $this->readyDep[$name]['method']);
        } else if (isset($this->withObjects[$name]['method']) && isset($this->withObjects[$name]['constructor']) === false) {
            return $object
                ->getMethod($this->readyDep[$name]['methodName'])
                ->invokeArgs(
                    $object->newInstance(),
                    $this->readyDep[$name]['method']
                );
        } else {
            return $object->newInstanceArgs($this->readyDep[$name]['constructor']);
        }

    }

}