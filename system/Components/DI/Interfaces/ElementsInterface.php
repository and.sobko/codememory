<?php

namespace System\Components\DI\Interfaces;

/**
 * Interface ElementsInterface
 * @package System\Components\DI\Container\Interfaces
 *
 * @author  Codememory
 */
interface ElementsInterface
{

    /**
     * @return mixed
     */
    public function execute(): object;

}