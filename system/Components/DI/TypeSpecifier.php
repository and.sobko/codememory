<?php

namespace System\Components\DI;

use ReflectionClass;
use ReflectionException;
use System\Components\DI\DIElements\ObjectDependency;
use System\FileSystem\Different\Exceptions\IncorrectPathException;

/**
 * Class TypeSpecifier
 * @package System\Components\DI\Container
 *
 * @author  Codememory
 */
class TypeSpecifier
{

    const ARGUMENT_PROPERTY_NAME = [
        'method'      => 'arguments',
        'callback'    => 'arguments',
        'constructor' => 'argumentsConstructor'
    ];

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method gets a list of all classes in the project, except for the vendor / folder
     * & And from all these classes finds classes that implement the interface passed in the parameter
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $dependency
     *
     * @return mixed
     * @throws ReflectionException
     * @throws IncorrectPathException
     */
    private function implementedClass(array $dependency): mixed
    {

        $arguments = null;

        foreach (allClasses() as $namespace) {
            $reflection = new ReflectionClass($namespace);
            $implements = $reflection->implementsInterface($dependency['package']);

            if ($implements) {
                $arguments = new $namespace();
            }
        }

        return $arguments;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns an array with a class, if a class dependency is passed
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $parameter
     *
     * @return array
     * @throws ReflectionException
     */
    private function checkClass(array $parameter): array
    {

        $namePackage = sprintf('package-%s-%s', hash('sha256', $parameter['package']), hash('sha256', $parameter['package']));
        $objectDep = new ObjectDependency();

        $objectDep->setName($namePackage)
            ->setObject($parameter['package'])
            ->collect()
            ->execute();

        return [
            'package' => $objectDep->get($namePackage)
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns a class that implements the interface passed by type in parameters
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $parameter
     *
     * @return array
     * @throws ReflectionException|IncorrectPathException
     */
    private function checkInterface(array $parameter): array
    {

        return [
            'package' => $this->implementedClass($parameter)
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns an array with the argument value passed as a dependency. If there are no arguments
     * & for the given method and the argument in DI by default is null
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $type
     * @param string $name
     *
     * @param array  $dependency
     *
     * @return null[]
     */
    private function checkDefaultArgument(string $type, string $name, array $dependency): array
    {

        $keyArg = self::ARGUMENT_PROPERTY_NAME[$type];

        return [
            'default' => $dependency[$keyArg][$name] ?? null
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This conditional method for disassembling dependencies is called in the main method for disassembling
     * & the dependency array "disassemblyParameters"
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array  $argument
     * @param array  $dependency
     * @param string $type
     *
     * @return array
     * @throws ReflectionException
     * @throws IncorrectPathException
     */
    private function dependencyConditions(array $argument, array $dependency, string $type): array
    {

        if ($argument['isInterface'] === false && $argument['isBuiltin'] === false) {
            $parameters = $this->checkClass($argument);
        } else if ($argument['isInterface'] === true) {
            $parameters = $this->checkInterface($argument);
        } else {
            $parameters = $this->checkDefaultArgument($type, $argument['name'], $dependency);
        }

        return $parameters;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method that parses an array of arguments, and casts it into an array to feed the container
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array  $parameters
     * @param string $name
     * @param array  $dependencies
     *
     * @return array
     * @throws ReflectionException|IncorrectPathException
     */
    private function disassemblyDependencies(array $parameters, string $name, array $dependencies): array
    {

        $newDependencies = [];

        foreach ($parameters as $type => $arguments) {
            if ($arguments !== []) {
                foreach ($arguments as $index => $argument) {
                    $newDependencies[$type][] = $this->dependencyConditions($argument, $dependencies[$name], $type);
                }
            } else {
                $newDependencies[$type] = [];
            }
        }

        return $newDependencies;

    }

    /**
     * @param array $dependencies
     *
     * @return array
     * @throws ReflectionException|IncorrectPathException
     */
    private function disassembly(array $dependencies): array
    {

        $arguments = [];

        foreach ($dependencies as $name => $data) {
            $parameters = $data['parameters'];

            $arguments[$name] = $this->disassemblyDependencies($parameters, $name, $dependencies);
        }

        return $arguments;

    }

    /**
     * @param array $dependencies
     *
     * @return array
     */
    private function formatting(array $dependencies): array
    {

        $formattedDependencies = [];

        foreach ($dependencies as $name => $types) {
            foreach ($types as $type => $arguments) {
                if ($arguments === []) {
                    $formattedDependencies[$name][$type] = [];
                } else {
                    foreach ($arguments as $argument) {
                        if (array_key_exists('package', $argument)) {
                            $formattedDependencies[$name][$type][] = $argument['package'];
                        } else {
                            $formattedDependencies[$name][$type][] = $argument['default'];
                        }
                    }
                }
            }
        }

        return $formattedDependencies;

    }


    /**
     * @param array $dependencies
     *
     * @return array
     * @throws ReflectionException|IncorrectPathException
     */
    protected function getDependencies(array $dependencies): array
    {

        $format = $this->formatting(
            $this->disassembly($dependencies)
        );

        foreach ($dependencies as $data) {
            $format[$data['name']]['registered'] = $data['registered'];

            if (array_key_exists('method', $data)) {
                $format[$data['name']]['methodName'] = $data['method'];
            }

        }

        return $format;

    }


}