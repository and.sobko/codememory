<?php

namespace System\Components\Logging;

use File;
use JetBrains\PhpStorm\Pure;
use System\FileSystem\Different\Exceptions\IncorrectPathException;
use System\FileSystem\Different\Interfaces\UnitsInterface;

/**
 * Class Logging
 * @package System\Components\Logging
 *
 * @author  Codememory
 */
class Logging implements LoggingInterface
{

    private const PATH_LOG = 'storage/';

    /**
     * @var string
     */
    private string $type = 'info';

    /**
     * @var string|null
     */
    private ?string $message = null;

    /**
     * @param string $type
     * @param string $message
     */
    private function setLog(string $type, string $message): void
    {

        $this->type = $type;
        $this->message = $message;

    }

    /**
     * @param string $date
     *
     * @return string
     */
    #[Pure] private function viewLog(string $date): string
    {

        return sprintf(
            "[%s] %s #%s\n",
            $date, ucfirst($this->type), $this->message
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add a message to the log with type Error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $message
     *
     * @return $this
     */
    public function error(string $message): Logging
    {

        $this->setLog('error', $message);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add a message to the log with type Warning
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $message
     *
     * @return $this
     */
    public function warning(string $message): Logging
    {

        $this->setLog('warning', $message);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add a message to the log with type Info
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $message
     *
     * @return $this
     */
    public function info(string $message): Logging
    {

        $this->setLog('info', $message);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method creates the full path to the log file and returns it
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    private function pathLog(): string
    {

        return sprintf('%s%s', self::PATH_LOG, envi('LOGGING.file'));

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method automatically (when creating a new log) clears the log file if the file
     * & size exceeds the size specified in .env
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws IncorrectPathException
     */
    private function autoClear(): void
    {

        if (File::info()->getSize($this->pathLog(), UnitsInterface::UNIT_KB) >= (double) envi('logging.clean-up-after')) {
            File::remove($this->pathLog());
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>
     * & Write to log
     * <=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws IncorrectPathException
     */
    public function make(): void
    {

        $this->autoClear();

        File::editor()->append($this->pathLog(), $this->viewLog('2020-04-03 12:00:00'));
        File::setPermission($this->pathLog());

    }

}