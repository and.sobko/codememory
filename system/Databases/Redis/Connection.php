<?php

namespace System\Databases\Redis;

use Redis;
use RedisException;
use System\Databases\Redis\Exceptions\ConnectionErrorException;
use System\Databases\Redis\Exceptions\PasswordConnectionErrorException;

/**
 * Class Connection
 * @package System\Databases\Redis
 *
 * @author  Codememory
 */
class Connection
{

    private Redis $redis;

    /**
     * @var string|int
     */
    private string|int $host = '127.0.0.1';

    /**
     * @var int
     */
    private int $port = 6379;

    /**
     * @var int|float
     */
    private int|float $timeout = 0.0;

    /**
     * @var string|null
     */
    private ?string $persistentId = null;

    /**
     * @var int
     */
    private int $retryInterval = 0;

    /**
     * @var int
     */
    private int $readTimeout = 0;

    /**
     * @var string|null
     */
    private ?string $password = null;

    /**
     * @var Redis|null
     */
    private Redis|null $connected = null;

    /**
     * Connection constructor.
     */
    public function __construct()
    {

        $this->redis = new Redis();

    }

    /**
     * @param string|int $host
     *
     * @return $this
     */
    public function host(string|int $host): Connection
    {

        $this->host = $host;

        return $this;

    }

    /**
     * @param int $port
     *
     * @return $this
     */
    public function port(int $port): Connection
    {

        $this->port = $port;

        return $this;

    }

    /**
     * @param int|float $time
     *
     * @return $this
     */
    public function timeout(int|float $time): Connection
    {

        $this->timeout = $time;

        return $this;

    }

    /**
     * @param string $id
     *
     * @return Connection
     */
    public function persistentId(string $id): Connection
    {

        $this->persistentId = $id;

        return $this;

    }

    /**
     * @param int $interval
     *
     * @return $this
     */
    public function retryInterval(int $interval): Connection
    {

        $this->retryInterval = $interval;

        return $this;

    }

    /**
     * @param int $time
     *
     * @return $this
     */
    public function readTimeout(int $time): Connection
    {

        $this->readTimeout = $time;

        return $this;

    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function password(string $password): Connection
    {

        $this->password = $password;

        return $this;

    }

    /**
     * @return bool
     */
    public function isConnect(): bool
    {

        return null === $this->connected ? false : true;

    }

    /**
     * @return Redis
     * @throws ConnectionErrorException
     * @throws PasswordConnectionErrorException
     */
    public function connect(): Redis
    {

        return $this->handler();

    }

    /**
     * @param string|null $password
     *
     * @return bool
     */
    private function authWithPassword(?string $password = null): bool
    {

        return $password ? true : false;

    }

    /**
     * @throws ConnectionErrorException
     */
    private function createConnect(): void
    {

        try {
            $this->redis->connect(
                $this->host,
                $this->port,
                $this->timeout,
                $this->persistentId,
                $this->retryInterval,
                $this->readTimeout
            );
        } catch (RedisException) {
            throw new ConnectionErrorException($this->host, $this->port);
        }

    }

    /**
     * @return Connection
     * @throws ConnectionErrorException
     * @throws PasswordConnectionErrorException
     */
    private function connectByHostAndPort(): Connection
    {

        if ($this->authWithPassword($this->password)) {
            try {
                $this->createConnect();
                $this->redis->auth($this->password);
            } catch (RedisException) {
                throw new PasswordConnectionErrorException();
            }
        }

        return $this;

    }

    /**
     * @return Redis
     * @throws ConnectionErrorException
     * @throws PasswordConnectionErrorException
     */
    private function handler(): Redis
    {

        $this->connectByHostAndPort();

        $this->connected = $this->redis;

        return $this->connected;

    }

}