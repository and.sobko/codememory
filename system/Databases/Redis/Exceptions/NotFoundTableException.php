<?php

namespace System\Databases\Redis\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class NotFoundTableException
 * @package System\Databases\Redis\Exceptions
 *
 * @author  Codememory
 */
class NotFoundTableException extends ErrorException
{

    /**
     * @var string|null
     */
    private ?string $table;

    /**
     * @var int
     */
    private int $db;

    /**
     * NotFoundTableException constructor.
     *
     * @param string|int  $db
     * @param string|null $table
     */
    #[Pure] public function __construct(string|int $db, ?string $table = null)
    {

        $this->db = $db;
        $this->table = $table;

        parent::__construct(
            sprintf('Could not find table(key) <b>%s</b> in Redis in DB <b>%s</b>', $table ?? 'null', $db)
        );

    }

    /**
     * @return string|null
     */
    public function getTable(): ?string
    {

        return $this->table;

    }

    /**
     * @return int
     */
    public function getDb(): int
    {

        return $this->db;

    }

}