<?php

namespace System\Databases\Redis\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class NotTableSelectedException
 * @package System\Databases\Redis\Exceptions
 *
 * @author  Codememory
 */
class NotTableSelectedException extends ErrorException
{

    /**
     * NotTableSelectedException constructor.
     */
    #[Pure] public function __construct()
    {
        parent::__construct('To add a new record to the table, you need to specify the name of the table to which to add the record selectTable (<name>)');
    }

}