<?php

namespace System\Databases\Redis;

use System\Databases\Redis\Exceptions\NotFoundTableException;
use System\Databases\Redis\Exceptions\NotTableSelectedException;

/**
 * Class Handler
 * @package System\Databases\Redis
 *
 * @author  Codememory
 */
abstract class Handler
{

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method is to create a table(key) with the addition of a lifetime in the active database
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     */
    private function createTable(): void
    {

        $this->connected->sAdd($this->createdTable['name'], null);

        if (null !== $this->createdTable['life']) {
            $this->connected->expire($this->createdTable['name'], $this->createdTable['life']);
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method handler to create a table
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     */
    protected function createTableInDatabase(): void
    {

        if ($this->createdTable['ifMissing']) {
            if (false === $this->tableExist($this->createdTable['name'])) {
                $this->createTable();
            }
        } else {
            $this->createTable();
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A method that generates a full value with a column name to add a record to the table
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $column
     * @param mixed  $value
     *
     * @return string
     */
    private function generateRecordName(string $column, mixed $value): string
    {

        return $column . '.' . $value;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Check if the table is selected otherwise an exception will be thrown about the loss of the table
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return Handler
     * @throws NotTableSelectedException
     */
    private function checkSelectedTable(): Handler
    {

        if (null === $this->selectedTable) {
            throw new NotTableSelectedException();
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checking the table for existence in the active database will throw a NotFoundTableException
     * & if it does not exist
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return Handler
     * @throws NotFoundTableException
     */
    private function noTableException(): Handler
    {

        if (false === $this->tableExist($this->selectedTable)) {
            throw new NotFoundTableException($this->selectedDb, $this->selectedTable);
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main processing method is adding a new record to the selected table
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws NotTableSelectedException
     */
    protected function addRecordInTable(): void
    {

        $this->checkSelectedTable();

        if (false === $this->tableExist($this->selectedTable)) {
            $this->createdTable['name'] = $this->selectedTable;

            $this->createTableInDatabase();
        } else {
            foreach ($this->records as $record) {
                $this->connected->sAdd($this->selectedTable, json_encode([
                    'value' => $this->generateRecordName($record['column'], $record['value'])
                ]));
            }
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns an array of correct records from the table, in addition, this method
     * & is a handler that processes the callback
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param callable $callback
     *
     * @return array
     * @throws NotFoundTableException
     * @throws NotTableSelectedException
     */
    private function records(callable $callback): array
    {

        $this->checkSelectedTable()
            ->noTableException();

        $records = $this->connected->sMembers($this->selectedTable);
        $readyValue = [];

        if ([] !== $records) {
            foreach ($records as $record) {
                $recordObject = json_decode($record);

                $readyValue[] = call_user_func($callback, $recordObject);
            }
        }

        return array_filter($readyValue, function ($value) {
            return null !== $value;
        });

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method for getting a specific record from the active table
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $column
     *
     * @return bool|string|null
     * @throws NotFoundTableException
     * @throws NotTableSelectedException
     */
    protected function getRecordOfTable(string $column): null|bool|string
    {

        $records = $this->records(function ($record) use ($column) {
            if (is_object($record)) {
                if (str_starts_with($record->value, $column . '.')) {
                    return substr($record->value, strlen($column . '.'));
                }
            }

            return null;
        });

        if ([] !== $records) {
            return $records[array_key_last($records)];
        }

        return null;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method is similar to the getRecordOfTable method with one difference,
     * & it returns a complete list of all records from the active table.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     * @throws NotFoundTableException
     * @throws NotTableSelectedException
     */
    protected function getRecordsOfTable(): array
    {

        return $this->records(function ($record) {
            if (is_object($record)) {
                if (preg_match('/[^.]\.(?<value>.*)/', $record->value, $match)) {
                    return $match['value'];
                }
            }

            return $record;
        });

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A handler method that updates a specific record in the active table
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws NotFoundTableException
     * @throws NotTableSelectedException
     */
    protected function updateRecordInTable(): void
    {

        if (null !== $this->getRecordOfTable($this->updatedRecord['column'])) {
            $this->removeRecord($this->updatedRecord['column']);

            $this->records = [];

            $this->addRecord($this->updatedRecord['column'], $this->updatedRecord['value']);
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method is a handler that deletes records from the active table by the specified columns
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws NotFoundTableException
     * @throws NotTableSelectedException
     */
    protected function removeRecordOfTable(): void
    {

        $records = [];

        if ([] !== $this->removedRecord) {
            foreach ($this->removedRecord as $column) {
                $records = $this->records(function ($record) use ($column) {
                    if (is_object($record)) {
                        if (str_starts_with($record->value, $column . '.')) {
                            return json_encode($record);
                        }
                    }

                    return null;
                });
            }
        }

        if ([] !== $records) {
            foreach ($records as $record) {
                $this->connected->sRem($this->selectedTable, $record);
            }
        }

    }

}