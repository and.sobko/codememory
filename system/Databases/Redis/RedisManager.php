<?php

namespace System\Databases\Redis;

use Redis;
use System\Databases\Redis\Connection as RedisConnection;
use System\Databases\Redis\Handler as RedisHandler;

/**
 * Class Redis
 * @package System\Databases\Redis
 *
 * @author  Codememory
 */
class RedisManager extends RedisHandler implements RedisInterface
{

    /**
     * @var Redis
     */
    public Redis $connected;

    /**
     * @var int
     */
    protected int $selectedDb = 0;

    /**
     * @var array
     */
    protected array $createdTable = [];

    /**
     * @var string|null
     */
    protected ?string $selectedTable = null;

    /**
     * @var array
     */
    protected array $records = [];

    /**
     * @var array
     */
    protected array $updatedRecord = [];

    /**
     * @var array
     */
    protected array $removedRecord = [];

    /**
     * Redis constructor.
     * @throws Exceptions\ConnectionErrorException
     * @throws Exceptions\PasswordConnectionErrorException
     */
    public function __construct()
    {

        $this->connected = $this->connection();

    }

    /**
     * @return Redis
     * @throws Exceptions\ConnectionErrorException
     * @throws Exceptions\PasswordConnectionErrorException
     */
    private function connection(): Redis
    {

        $redisConnection = new RedisConnection();

        $redisConnection
            ->host(envi('redis.ip'))
            ->port((int) envi('redis.port'));

        if (null !== envi('redis.password')) {
            $redisConnection->password(envi('redis.password'));
        }

        return $redisConnection->connect();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checking a table for existence in the active database
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return bool
     */
    public function tableExist(string $name): bool
    {

        $tables = $this->connected->keys('*');

        return in_array($name, $tables);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Select the database for which all actions will be performed
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $db
     *
     * @return $this
     */
    public function selectDb(int $db): RedisManager
    {

        $this->selectedDb = $db;
        $this->connected->select($db);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Create table(key) in active database
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string   $name
     * @param bool     $ifMissing
     * @param int|null $life
     *
     * @return $this
     */
    public function createTable(string $name, bool $ifMissing = true, ?int $life = null): RedisManager
    {

        $this->createdTable = [
            'name'      => $name,
            'ifMissing' => $ifMissing,
            'life'      => $life
        ];
        $this->selectedTable = $name;

        $this->createTableInDatabase();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Select the table (key) for which the actions will be carried out: add, get, update, delete
     * & By default, the table that was created is selected
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return $this
     */
    public function selectTable(string $name): RedisManager
    {

        $this->selectedTable = $name;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add a new record to the table (key) records are stored in json format, all specified
     * & columns must be unique, otherwise, when deleting, all records will be deleted with the
     * & same column names
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $column
     * @param mixed  $value
     *
     * @return $this
     * @throws Exceptions\NotTableSelectedException
     */
    public function addRecord(string $column, mixed $value): RedisManager
    {

        $this->records[] = [
            'column' => $column,
            'value'  => $value
        ];

        $this->addRecordInTable();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get all records from the active database and from the active table
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     * @throws Exceptions\NotFoundTableException
     * @throws Exceptions\NotTableSelectedException
     */
    public function getRecords(): array
    {

        return $this->getRecordsOfTable();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get a specific record from the active database and from the active table
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $column
     *
     * @return bool|string|null
     * @throws Exceptions\NotFoundTableException
     * @throws Exceptions\NotTableSelectedException
     */
    public function getRecord(string $column): null|bool|string
    {

        return $this->getRecordOfTable($column);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Update a value in a record in the active table
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $column
     * @param mixed  $value
     *
     * @return bool
     * @throws Exceptions\NotFoundTableException
     * @throws Exceptions\NotTableSelectedException
     */
    public function updateRecord(string $column, mixed $value): bool
    {

        $this->updatedRecord = [
            'value'  => $value,
            'column' => $column
        ];

        $this->updateRecordInTable();

        return true;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add a value to the entry in the active tallies
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string   $column
     * @param callable $callback
     *
     * @return bool
     * @throws Exceptions\NotFoundTableException
     * @throws Exceptions\NotTableSelectedException
     */
    public function updateRecordWithAdd(string $column, callable $callback): bool
    {

        $call = call_user_func($callback, $this->getRecord($column));
        $this->updateRecord($column, $call);

        return true;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Remove all records from the active table that have the columns specified in the argument
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param mixed ...$columns
     *
     * @return bool
     * @throws Exceptions\NotFoundTableException
     * @throws Exceptions\NotTableSelectedException
     */
    public function removeRecord(...$columns): bool
    {

        $this->removedRecord = $columns;
        $this->removeRecordOfTable();

        return true;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Access to all Redis methods
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $method
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call(string $method, array $arguments): mixed
    {

        return call_user_func_array([$this->connected, $method], $arguments);

    }

}