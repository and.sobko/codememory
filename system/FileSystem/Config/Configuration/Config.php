<?php

namespace System\FileSystem\Config\Configuration;

use File;
use System\Components\Caching\Configuration;
use System\FileSystem\Config\Configuration\Repositories\CacheConfigRepository;
use System\FileSystem\Config\Exceptions\InvalidCacheConfigTypeException;
use System\FileSystem\Config\Exceptions\InvalidConfigKeyException;
use Yaml;

/**
 * Class Config
 * @package System\FileSystem\Config\Configuration
 *
 * @author  Codememory
 */
class Config
{

    public const CONFIG_CACHE_KEY = 'configs/%s';
    public const CONFIG_PACKAGES_CACHE_KEY = 'configs/%s';
    public const TYPE_DEFAULT_CACHE_CONFIG = 'conf';
    public const TYPE_PACKAGE_CACHE_CONFIG = 'packages';

    /**
     * @var string|null
     */
    private ?string $openedConfig = null;

    /**
     * @var Configuration
     */
    private Configuration $cacheConf;

    /**
     * @var CacheConfigRepository
     */
    private CacheConfigRepository $configRepository;

    /**
     * @var bool
     */
    private bool $ofPackage = false;

    /**
     * Config constructor.
     *
     * @param Configuration         $cacheConf
     * @param CacheConfigRepository $configRepository
     */
    public function __construct(Configuration $cacheConf, CacheConfigRepository $configRepository)
    {

        $this->cacheConf = $cacheConf;
        $this->configRepository = $configRepository;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Retrieving the entire cache history
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    private function cacheHistory(): array
    {

        $path = sprintf(
            '%s%s.%s',
            $this->cacheConf->pathSaveHistory(),
            $this->cacheConf->historyFilename(),
            $this->cacheConf->expansionHistoryFile()
        );

        return Yaml::open($path)->parse()['history'];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Open a configuration chat from the cache by type
     * & There are records like this in the caching history
     * & config/conf, configs/packages
     * & So conf, packages is the type of configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $config
     *
     * @return $this
     */
    public function open(string $config): Config
    {

        $this->openedConfig = $config;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set to true to get configuration data only from packages
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     */
    public function package(): Config
    {

        $this->ofPackage = true;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checking for the existence of a configuration type If an open configuration
     * & type exists, then the cache information of the open type is returned. Otherwise,
     * & InvalidCacheConfigTypeException will be thrown
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $halfKey
     *
     * @return bool|array|null
     * @throws InvalidCacheConfigTypeException
     */
    private function exists(string $halfKey): bool|array|null
    {

        return array_key_exists(sprintf($halfKey, $this->openedConfig), $this->cacheHistory()) ? $this->cacheHistory()[sprintf($halfKey, $this->openedConfig)] : throw new InvalidCacheConfigTypeException($this->openedConfig);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Loading package configuration from cache
     *
     * Path: configs/packages/{dir|file}.yaml
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool|array|null
     * @throws InvalidCacheConfigTypeException
     */
    private function fromPackage(): null|bool|array
    {

        return $this->exists(self::CONFIG_PACKAGES_CACHE_KEY) ?: null;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Loading the usual configuration from the cache
     *
     * Path: configs/*.yaml
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool|array|null
     * @throws InvalidCacheConfigTypeException
     */
    private function fromDefaultConfig(): null|bool|array
    {

        return $this->exists(self::CONFIG_PACKAGES_CACHE_KEY);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Generating the full path to the .meta configuration cache
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $type
     *
     * @return string
     * @throws InvalidCacheConfigTypeException
     */
    private function generatePathConfig(string $type): string
    {

        $path = $this->configRepository->getConfigCachePath();

        return $path . $type . '/' . $this->recipient()['hash'] . '.meta';

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method reads the cache file, converts data from serialize to json,
     * & and from json to php array and returns this array
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string|array
     * @throws InvalidCacheConfigTypeException
     */
    private function configurationCache(): string|array
    {

        $path = false === $this->ofPackage ? $this->generatePathConfig(self::TYPE_DEFAULT_CACHE_CONFIG) : $this->generatePathConfig(self::TYPE_PACKAGE_CACHE_CONFIG);

        return json_decode(
            unserialize(File::read($path)), true
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method determines from which type of configuration to load information
     * & from the cache
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return mixed
     * @throws InvalidCacheConfigTypeException
     */
    private function recipient(): mixed
    {

        if (false === $this->ofPackage) {
            return $this->fromDefaultConfig()['lastCaching'];
        }

        return $this->fromPackage()['lastCaching'];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method parses a string of keys and returns information from an array for
     * & these keys
     *
     * Example string: Test.info.name
     * TO: $array['Test']['info']['name']
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array       $data
     * @param string|null $keys
     *
     * @throws InvalidConfigKeyException
     */
    private function parsingKeys(array &$data, ?string $keys = null): void
    {

        $keys = explode('.', $keys);

        if ([] !== $keys && !empty($keys[0])) {
            foreach ($keys as $key) {
                if (false === array_key_exists($key, $data)) {
                    throw new InvalidConfigKeyException($key);
                }

                $data = $data[$key];
            }
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returning information from the cache by key
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $keys
     *
     * @return array|string
     * @throws InvalidConfigKeyException|InvalidCacheConfigTypeException
     */
    public function get(?string $keys = null): array|string
    {

        $data = $this->configurationCache();
        $this->parsingKeys($data, $keys);

        return $data;

    }

}