<?php

namespace System\FileSystem\Config\Configuration\Interfaces;

/**
 * Interface CacheConfigInterface
 * @package System\FileSystem\Config\Configuration\Interfaces
 *
 * @author Codememory
 */
interface CacheConfigInterface
{

    /**
     * @return string
     */
    public function getConfigCachePath(): string;


}