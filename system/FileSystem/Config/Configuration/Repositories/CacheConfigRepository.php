<?php


namespace System\FileSystem\Config\Configuration\Repositories;

use System\Components\Caching\Configuration;
use System\FileSystem\Config\Configuration\Interfaces\CacheConfigInterface;

/**
 * Class CacheConfig
 * @package System\FileSystem\Config\Configuration\Repositories
 *
 * @author  Codememory
 */
class CacheConfigRepository implements CacheConfigInterface
{

    private const CACHE_CONFIG_PATH = '*configs/';

    /**
     * @var Configuration
     */
    private Configuration $cacheConf;

    /**
     * CacheConfigRepository constructor.
     *
     * @param Configuration $conf
     */
    public function __construct(Configuration $conf)
    {

        $this->cacheConf = $conf;

    }

    /**
     * @return string
     */
    public function getConfigCachePath(): string
    {

        return str_replace('*', $this->cacheConf->pathCache(), self::CACHE_CONFIG_PATH);

    }


}