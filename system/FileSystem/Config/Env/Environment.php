<?php

namespace System\FileSystem\Config\Env;

use Json;
use System\Components\Caching\Caching;
use System\FileSystem\Config\Env\Exceptions\NotFoundKeyNameException;
use System\FileSystem\Config\Env\Exceptions\OldEnvCacheException;
use System\FileSystem\Different\File;
use System\FileSystem\Different\Yaml;

/**
 * Class Environment
 * @package System\FileSystem\Config\Env
 *
 * @author  Codememory
 */
class Environment extends Parser
{

    public const TYPE_ARRAY = 'array';
    public const TYPE_STRING = 'string';
    public const TYPE_JSON = 'json';

    /**
     * Environment constructor.
     *
     * @param File    $file
     * @param Yaml    $yaml
     * @param Caching $cache
     */
    public function __construct(
        protected File $file,
        protected Yaml $yaml,
        private Caching $cache
    )
    {

        $this->assembly();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add dynamic new environment
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string           $group
     * @param string           $varName
     * @param bool|string|null $value
     *
     * @return $this
     */
    public function set(string $group, string $varName, bool|null|string $value): Environment
    {

        $this->envi[$group][$varName] = $value;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Retrieving environment variable from cache
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string           $group
     * @param string           $varName
     * @param bool|string|null $default
     *
     * @return bool|string|null
     * @throws NotFoundKeyNameException|OldEnvCacheException
     */
    public function get(string $group, string $varName, bool|null|string $default = null): bool|string|null
    {

        if (!array_key_exists($group, $this->envi) || !array_key_exists($varName, $this->envi[$group])) {
            throw new NotFoundKeyNameException($group, $varName);
        }

        $cache = $this->cache->getCache('Environment');
        $data = null;

        if (null === $cache) {
            throw new OldEnvCacheException();
        } else {
            $data = $cache[$group][$varName];
        }

        return null === $data ? $default : $data;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get an array of all environment groups. For example, there are several environments
     * & APP_DEBUG, APP_IP
     * & the keyword "APP" is the group, and "IP" and "DEBUG" are the name of the variable
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    public function getGroups(): array
    {

        return $this->groups;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Getting all environments in different types JSON, STRING, ARRAY
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $type
     *
     * @return array|string|null
     */
    public function getAll(string $type = self::TYPE_ARRAY): array|string|null
    {

        if ($type === 'string') {
            $data = null;

            foreach ($this->envi as $group => $env) {
                foreach ($env as $var => $value) {
                    $data .= sprintf(
                        '%s=%s%s', $group . '_' . $var, $value, PHP_EOL
                    );
                }

                $data .= PHP_EOL;
            }

            return substr($data, 0, -1);
        } else if ($type === 'json') {
            return Json::setData($this->envi)->encode();
        }

        return $this->envi;

    }

}