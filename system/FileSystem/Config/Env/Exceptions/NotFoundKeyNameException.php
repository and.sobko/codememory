<?php

namespace System\FileSystem\Config\Env\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class NotFoundKeyNameException
 * @package System\FileSystem\Config\Env\Exceptions
 *
 * @author  Codememory
 */
class NotFoundKeyNameException extends ErrorException
{

    /**
     * NotFoundKeyNameException constructor.
     *
     * @param $group
     * @param $name
     */
    #[Pure] public function __construct($group, $name)
    {

        parent::__construct(sprintf('Group <b>%s</b> or key <b>%s</b> was not found in group <b>%s</b>.', $group, $name, $group));

    }

}