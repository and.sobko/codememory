<?php

namespace System\FileSystem\Config\Env\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class OldEnvCacheException
 * @package System\FileSystem\Config\Env\Exceptions
 *
 * @author  Codememory
 */
class OldEnvCacheException extends ErrorException
{

    /**
     * OldEnvCacheException constructor.
     */
    #[Pure] public function __construct()
    {

        parent::__construct('Cache for environment changes has not been updated. Refresh the bin/codememory cache:env');

    }

}