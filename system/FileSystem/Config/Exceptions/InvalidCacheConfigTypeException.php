<?php

namespace System\FileSystem\Config\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class InvalidCacheConfigTypeException
 * @package System\FileSystem\Config\Exceptions
 *
 * @author Codememory
 */
class InvalidCacheConfigTypeException extends ErrorException
{

    /**
     * InvalidCacheConfigTypeException constructor.
     *
     * @param string|null $type
     */
    #[Pure] public function __construct(?string $type = null)
    {

        parent::__construct(
            sprintf(
                'Invalid configuration type from cache. There is no <b>%s</b> type for the configuration cache', $type
            )
        );

    }

}