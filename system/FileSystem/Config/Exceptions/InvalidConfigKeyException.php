<?php

namespace System\FileSystem\Config\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class InvalidConfigKeyException
 * @package System\FileSystem\Config\Exceptions
 *
 * @author  Codememory
 */
class InvalidConfigKeyException extends ErrorException
{

    /**
     * InvalidConfigKeyException constructor.
     *
     * @param string $key
     */
    #[Pure] public function __construct(string $key)
    {
        parent::__construct(
            sprintf('<b>%s</b> key not found in configuration', $key)
        );
    }

}