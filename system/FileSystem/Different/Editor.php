<?php

namespace System\FileSystem\Different;

/**
 * Class Editor
 * @package System\FileSystem\Different
 *
 * @author  Codememory
 */
class Editor
{

    /**
     * @var File
     */
    private File $file;

    /**
     * Editor constructor.
     *
     * @param File $file
     */
    public function __construct(File $file)
    {

        $this->file = $file;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Write data to file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string   $path
     * @param mixed    $contents
     * @param int      $permission
     * @param int|null $length
     *
     * @return bool
     * @throws Exceptions\IncorrectPathException
     */
    public function put(string $path, mixed $contents, int $permission = 0777, int $length = null): bool
    {

        $recourse = fopen($this->file->getRealPath($path), 'w+');
        $put = fwrite($recourse, $contents, $length);
        fclose($recourse);

        if (false !== $put) {
            $this->file->setPermission($path, $permission);
        }

        return $put;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add data to the end of the file, namely at the position where the sentence ended
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param mixed  $data
     *
     * @return $this
     */
    public function append(string $path, mixed $data): Editor
    {

        file_put_contents($this->file->getRealPath($path), $data, FILE_APPEND);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add data to the beginning of the file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param mixed  $data
     *
     * @return $this
     */
    public function prepend(string $path, mixed $data): Editor
    {

        $getData = $this->file->read($path);

        file_put_contents($this->file->getRealPath($path), $getData . $data);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Adds data to a file on a delimited string. By specifying the arguments line number
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param mixed  $data
     * @param int    $line
     *
     * @return bool
     * @throws Exceptions\IncorrectPathException
     */
    public function appendToLine(string $path, mixed $data, int $line): bool
    {

        $arrayData = file($this->file->getRealPath($path));

        if (array_key_exists($line, $arrayData)) $arrayData[$line] .= $data;
        else {
            $arrayData[$line] = $data;
        }

        return $this->put($path, implode(PHP_EOL, $arrayData));

    }

}