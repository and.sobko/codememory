<?php

namespace System\FileSystem\Different;

use JetBrains\PhpStorm\Pure;
use System\FileSystem\Different\Exceptions\IncorrectPathException;
use System\FileSystem\Different\Traits\FileInformationHandlerTrait;

/**
 * Class Information
 * @package System\FileSystem\Different
 *
 * @author  Codememory
 */
class Information
{

    use FileInformationHandlerTrait;

    /**
     * @var File
     */
    private File $file;

    /**
     * @var int
     */
    private int $size = 0;

    /**
     * Information constructor.
     *
     * @param File $file
     */
    public function __construct(File $file)
    {

        $this->file = $file;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the last divisible part of the element [/]
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $path
     * @param string|null $suffix
     *
     * @return string
     */
    #[Pure] public function getBasename(string $path, ?string $suffix = null): string
    {

        return basename($this->file->getRealPath($path), $suffix);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the file extension
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return string
     */
    #[Pure] public function getExtension(string $path): string
    {

        return pathinfo($this->file->getRealPath($path), PATHINFO_EXTENSION);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns before the last part of the element of the string that is split into a symbol [/]
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return string
     */
    #[Pure] public function getDirname(string $path): string
    {

        return pathinfo($this->file->getRealPath($path), PATHINFO_DIRNAME);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the type. Which will define a file or a directory or something else
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return string
     */
    #[Pure] public function getType(string $path): string
    {

        return filetype($this->file->getRealPath($path));

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Obtaining the file size in different units by default in bytes To retrieve on another system,
     * & use the UnitsInterface
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param array  $unit
     * @param int    $scale
     * @param bool   $recursion
     *
     * @return string|null
     * @throws IncorrectPathException
     */
    public function getSize(string $path, $unit = ['%byte%', 1], int $scale = 0, bool $recursion = false): ?string
    {

        $unit = !is_array($unit) ? ['%byte%', 1] : $unit;

        if ($this->file->exists($path)) {
            if ($recursion === false) {
                $size = filesize($this->file->getRealPath($path));

                return str_replace('%byte%', $size, $unit[0]) / $unit[1];
            } else {
                $this->sizeCalculation($path, $unit, $scale);

                return str_replace('%byte%', $this->size, $unit[0]) / $unit[1];
            }
        } else {
            return null;
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the time the file was last modified
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $path
     * @param string|null $format
     *
     * @return string
     */
    #[Pure] public function lastModified(string $path, ?string $format = 'Y-m-d H:i'): string
    {

        return date($format, filectime($this->file->getRealPath($path)));

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns full information about a file or directory
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param $path
     *
     * @return array
     */
    public function getAllInfo($path): array
    {

        $info = [];

        if (is_string($path)) {
            $info = $this->getArrAllInfo($path);

            if ($this->file->getIs()->isFile($path)) {
                $info['extension'] = $this->getExtension($path);
                $info['size'] = $this->getArrayReadyMeasurementsUnits($path);
            } else if ($this->file->getIs()->isDir($path)) {
                $info['size'] = $this->getArrayReadyMeasurementsUnits($path, true);
            }
        }

        return $info;

    }

}