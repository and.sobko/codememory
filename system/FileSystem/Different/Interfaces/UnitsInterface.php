<?php

/**
 * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
 * & The interface is intended for working with the file system, namely with dimensions,
 * & the user interface allows  you to get bytes into any unit of measurement.
 * & You can also create your own constant by changing only 2 parameters in the array
 * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
 */

namespace System\FileSystem\Different\Interfaces;

/**
 * Interface UnitsInterface
 * @package System\FileSystem\Different\Interfaces
 *
 * @author  Codememory
 */
interface UnitsInterface
{

    const UNIT_BYTE = ['%byte%', 1];

    const UNIT_KB = ['%byte%', 1000];

    const UNIT_MB = ['%byte%', 1000000];

    const UNIT_GB = ['%byte%', 1000000000];

}