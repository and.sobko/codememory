<?php

namespace System\FileSystem\Different\Interfaces;

/**
 * Interface YamlFlagsInterface
 * @package System\FileSystem\Different\Interfaces
 *
 * @author  Codememory
 */
interface YamlFlagsInterface
{

    const DUMP_OBJECT = 1;
    const PARSE_EXCEPTION_ON_INVALID_TYPE = 2;
    const PARSE_OBJECT = 4;
    const PARSE_OBJECT_FOR_MAP = 8;
    const DUMP_EXCEPTION_ON_INVALID_TYPE = 16;
    const PARSE_DATETIME = 32;
    const DUMP_OBJECT_AS_MAP = 64;
    const DUMP_MULTI_LINE_LITERAL_BLOCK = 128;
    const PARSE_CONSTANT = 256;
    const PARSE_CUSTOM_TAGS = 512;
    const DUMP_EMPTY_ARRAY_AS_SEQUENCE = 1024;
    const DUMP_NULL_AS_TILDE = 2048;

}