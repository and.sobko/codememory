<?php

namespace System\FileSystem\Different;

use System\FileSystem\Different\Traits\FileIsHandlerTrait;

/**
 * Class Is
 * @package System\FileSystem\Different
 *
 * @author  Codememory
 */
class Is
{

    use FileIsHandlerTrait;

    /**
     * @var File
     */
    private File $file;

    /**
     * Editor constructor.
     *
     * @param File $file
     */
    public function __construct(File $file)
    {

        $this->file = $file;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method checks if the path is a file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|array $filename
     *
     * @return bool
     */
    public function isFile(string|array $filename): bool
    {

        return $this->handlerIs($filename, 'is_file');

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method checks if the path is a directory
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|array $dirname
     *
     * @return bool
     */
    public function isDir(string|array $dirname): bool
    {

        return $this->handlerIs($dirname, 'is_dir');

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method checks if the file is writable
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|array $filename
     *
     * @return bool
     */
    public function isWrite(string|array $filename): bool
    {

        return $this->handlerIs($filename, 'is_writable');

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method checks if the file is readable
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|array $filename
     *
     * @return bool
     */
    public function isRead(string|array $filename): bool
    {

        return $this->handlerIs($filename, 'is_readable');

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method checks if the path is a symbolic link
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|array $filename
     *
     * @return bool
     */
    public function isLink(string|array $filename): bool
    {

        return $this->handlerIs($filename, 'is_link');

    }

}