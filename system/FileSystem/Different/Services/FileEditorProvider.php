<?php

namespace System\FileSystem\Different\Services;

use System\FileSystem\Different\Editor;

/**
 * @method put(string $path, mixed $contents, int $permission = 0777, int $length = null): bool
 * @method append(string $path, mixed $data): Editor
 * @method prepend(string $path, mixed $data): Editor
 * @method appendToLine(string $path, mixed $data, int $line): bool
 *
 * Class FileEditorProvider
 * @package System\FileSystem\Different\Services
 *
 * @author  Codememory
 */
class FileEditorProvider
{

    public function __construct()
    {
    }

    /**
     * @param Editor $editor
     *
     * @return Editor
     */
    public function executor(Editor $editor): Editor
    {

        return $editor;

    }
}