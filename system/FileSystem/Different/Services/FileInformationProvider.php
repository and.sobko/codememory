<?php

namespace System\FileSystem\Different\Services;

use System\FileSystem\Different\Information;

/**
 * @method isFile(string|array $filename): bool
 * @method isDir(string|array $dirname): bool
 * @method isWrite(string|array $filename): bool
 * @method isRead(string|array $filename): bool
 * @method isLink(string|array $filename): bool
 *
 * Class FileInformationProvider
 * @package System\FileSystem\Different\Services
 *
 * @author  Codememory
 */
class FileInformationProvider
{

    public function __construct()
    {
    }

    /**
     * @param Information $file
     *
     * @return Information
     */
    public function executor(Information $file): Information
    {

        return $file;

    }

}