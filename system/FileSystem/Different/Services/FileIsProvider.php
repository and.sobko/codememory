<?php

namespace System\FileSystem\Different\Services;

use System\FileSystem\Different\Is;

/**
 * @method isFile(string|array $filename): bool
 * @method isDir(string|array $dirname): bool
 * @method isWrite(string|array $filename): bool
 * @method isRead(string|array $filename): bool
 * @method isLink(string|array $filename): bool
 *
 * Class FileIsProvider
 * @package System\FileSystem\Different\Services
 *
 * @author  Codememory
 */
class FileIsProvider
{

    public function __construct()
    {
    }

    /**
     * @param Is $fileIs
     *
     * @return Is
     */
    public function executor(Is $fileIs): Is
    {

        return $fileIs;

    }

}