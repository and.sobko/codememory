<?php

namespace System\FileSystem\Different\Services;

use JetBrains\PhpStorm\Pure;
use System\FileSystem\Different\Editor as FileEditor;
use System\FileSystem\Different\File;
use System\FileSystem\Different\Find;
use System\FileSystem\Different\Information as FileInformation;
use System\FileSystem\Different\Is as FileIs;

/**
 * @method exists(string $path): bool
 * @method setRealPath(string $path): File
 * @method getRealPath(?string $join = null): string
 * @method scanning($path, ?array $ignoring = []): array
 * @method mkdir($dirname, ?int $permission = 0777, bool $recursion = false): void
 * @method setPermission(string $path, int $permission = 0777, bool $recursion = false): bool
 * @method setOwner(string $path, string $ownerName, bool $recursion = false): bool
 * @method rename(string $path, string $newName): bool
 * @method remove($path, bool $recursion = false, bool $removeCurrentDir = false): bool
 * @method move(string $path, string $moveTo): bool
 * @method setGroup(string $path, $group): bool
 * @method read(string $path): string
 * @method getImport($path): mixed
 * @method oneImport(string $path): void
 *
 * Class FileProvider
 * @package System\FileSystem\Different\Services
 *
 * @author  Codememory
 */
class FileProvider
{

    public function __construct()
    {
    }

    /**
     * @param File $file
     *
     * @return FileIs
     */
    #[Pure] public function is(File $file): FileIs
    {

        return $file->getIs();

    }

    /**
     * @param File $file
     *
     * @return FileEditor
     */
    #[Pure] public function editor(File $file): FileEditor
    {

        return $file->getEditor();

    }

    /**
     * @param FileInformation $fileInformation
     *
     * @return FileInformation
     */
    public function info(FileInformation $fileInformation): FileInformation
    {

        return $fileInformation;

    }

    /**
     * @param Find $findFile
     *
     * @return Find
     */
    public function find(Find $findFile): Find
    {

        return $findFile;

    }

    /**
     * @param File $file
     *
     * @return File
     */
    public function executor(File $file): File
    {

        return $file;

    }

}