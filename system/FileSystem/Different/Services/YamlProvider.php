<?php

namespace System\FileSystem\Different\Services;

use System\FileSystem\Different\Yaml;

/**
 * @method open(string $file): object
 * @method parse(): array
 * @method fromString(bool $status): object
 * @method addStringYaml(string $data): object
 * @method setFlags(int $flags): object
 * @method put(array $data, int $inline = 5, int $indent = 2): bool
 * @method change(callable $callback, int $inline = 5, int $indent = 2): bool
 *
 * Class YamlProvider
 * @package System\FileSystem\Different\Services
 *
 * @author  Codememory
 */
class YamlProvider
{

    public function __construct()
    {
    }

    /**
     * @param Yaml $yaml
     *
     * @return Yaml
     */
    public function executor(Yaml $yaml): Yaml
    {

        return $yaml;

    }

}