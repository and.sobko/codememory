<?php

namespace System\FileSystem\Different\Traits;

use System\FileSystem\Different\Exceptions\IncorrectPathException;
use System\FileSystem\Different\Interfaces\UnitsInterface;

/**
 * Trait FileInformationHandlerTrait
 * @package System\FileSystem\Different\Traits
 *
 * @author  Codememory
 */
trait FileInformationHandlerTrait
{

    /**
     * @param string $dir
     * @param array  $unit
     * @param int    $scale
     *
     * @throws IncorrectPathException
     */
    private function sizeCalculation(string $dir, array $unit, int $scale): void
    {

        $dir = rtrim($dir, '/') . '/';
        $attached = $this->file->scanning($dir);

        if (is_array($attached) && count($attached) > 0) {
            foreach ($attached as $att) {
                $path = $dir . $att;

                if ($this->file->isDir($path)) {
                    $this->size += $this->getSize($path, $unit, $scale);

                    $this->sizeCalculation($path, $unit, $scale);
                } else {
                    $this->size += $this->getSize($path, $unit, $scale);
                }
            }
        }

    }

    /**
     * @param $path
     *
     * @return array
     */
    private function getArrAllInfo($path): array
    {

        $realPath = $this->file->getRealPath($path);

        return [
            'type'         => $this->getType($path),
            'lastModified' => $this->lastModified($path),
            'path'         => $this->getDirname($path),
            'group'        => posix_getpwuid(filegroup($realPath))['name'],
            'owner'        => posix_getpwuid(fileowner($realPath))['name'],
            'permissions'  => substr(sprintf('%o', fileperms($realPath)), -4)
        ];

    }

    /**
     * @param      $path
     * @param bool $recursion
     *
     * @return array
     */
    private function getArrayReadyMeasurementsUnits($path, bool $recursion = false): array
    {

        return [
            'b'  => $this->getSize($path, UnitsInterface::UNIT_BYTE, 0, $recursion),
            'kb' => $this->getSize($path, UnitsInterface::UNIT_KB, 3, $recursion),
            'mb' => $this->getSize($path, UnitsInterface::UNIT_MB, 3, $recursion),
            'gb' => $this->getSize($path, UnitsInterface::UNIT_GB, 3, $recursion),
        ];

    }

}