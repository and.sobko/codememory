<?php

namespace System\FileSystem\Different\Traits;

/**
 * Trait FileIsHandlerTrait
 * @package System\FileSystem\Different\Traits
 *
 * @author  Codememory
 */
trait FileIsHandlerTrait
{

    /**
     * @param $path
     * @param $function
     *
     * @return mixed
     */
    private function handlerIs($path, $function): mixed
    {

        $is = false;

        if (is_string($path)) {
            $is = $function($this->file->getRealPath($path));
        } else if (is_array($path)) {
            foreach ($path as $name) {
                $is = $this->handlerIs($name, $function);
            }
        }

        return $is;

    }

}