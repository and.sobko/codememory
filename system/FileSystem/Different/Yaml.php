<?php

namespace System\FileSystem\Different;

use Symfony\Component\Yaml\Yaml as SymfonyYaml;

/**
 * Class Yaml
 * @package System\FileSystem\Different
 *
 * @author  Codememory
 */
class Yaml
{

    /**
     * @var string|null
     */
    private ?string $openFile = null;

    /**
     * @var File
     */
    private File $file;

    /**
     * @var int
     */
    private int $flags = 0;

    /**
     * @var bool
     */
    private bool $parseOfString = false;

    /**
     * @var string|null
     */
    private ?string $data = null;

    /**
     * Yaml constructor.
     *
     * @param File $file
     */
    public function __construct(File $file)
    {

        $this->file = $file;

    }

    /**
     * @param bool $status
     *
     * @return object
     */
    public function fromString(bool $status): object
    {

        $this->parseOfString = $status;

        return $this;

    }

    /**
     * @param string $data
     *
     * @return object
     */
    public function addStringYaml(string $data): object
    {

        $this->data = $data;

        return $this;

    }

    /**
     * @param string $file
     *
     * @return $this
     */
    public function open(string $file): object
    {

        $this->openFile = $file;

        return $this;

    }

    /**
     * @param int $flags
     *
     * @return object
     */
    public function setFlags(int $flags): object
    {

        $this->flags = $flags;

        return $this;

    }

    /**
     * @return array|null
     */
    public function parse(): array|null
    {

        if ($this->parseOfString === true) {
            return SymfonyYaml::parse($this->data, $this->flags);
        }

        return SymfonyYaml::parseFile($this->file->getRealPath($this->openFile), $this->flags);

    }

    /**
     * @param array $data
     * @param int   $inline
     * @param int   $indent
     *
     * @return bool
     * @throws Exceptions\IncorrectPathException
     */
    public function put(array $data, int $inline = 5, int $indent = 2): bool
    {

        $data = SymfonyYaml::dump($data, $inline, $indent, $this->flags);

        $this->file->getEditor()->put($this->openFile, $data);

        return true;

    }

    /**
     * @param callable $callback
     * @param int      $inline
     * @param int      $indent
     *
     * @return bool
     * @throws Exceptions\IncorrectPathException
     */
    public function change(callable $callback, int $inline = 5, int $indent = 2): bool
    {

        $data = call_user_func($callback, $this->parse());

        return $this->put($data, $inline, $indent);

    }

}