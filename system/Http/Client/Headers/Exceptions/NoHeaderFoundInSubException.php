<?php

namespace System\Http\Response\Headers\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class NoHeaderFoundInSubException
 * @package System\Http\Response\Headers\Exceptions
 *
 * @author  Codememory
 */
class NoHeaderFoundInSubException extends ErrorException
{

    /**
     * NoHeaderFoundInSubException constructor.
     *
     * @param string $header
     */
    #[Pure] public function __construct(string $header)
    {

        parent::__construct(
            sprintf(
                'Unable to add value to dispatched header <b>%1$s</b>. First you need to create a <b>%1$s</b> header', $header
            )
        );

    }

}