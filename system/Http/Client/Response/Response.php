<?php

namespace System\Http\Client\Response;

use JetBrains\PhpStorm\Pure;
use System\Http\Client\Headers\Header;
use System\Http\Response\Headers\Exceptions\NoHeaderFoundInSubException;
use System\Support\Exceptions\JsonParser\JsonErrorException;
use System\Support\JsonParser;

/**
 * Class Response
 * @package System\Http\Client\Response
 *
 * @author  Codememory
 */
class Response
{

    /**
     * @var Download
     */
    public Download $download;

    /**
     * @var Header
     */
    public Header $headers;

    /**
     * @var JsonParser
     */
    public JsonParser $json;

    /**
     * @var string|int|null
     */
    private string|int|null $content = null;

    /**
     * @var array
     */
    private array $listHeaders = [];

    /**
     * @var int
     */
    private int $responseCode = 200;

    /**
     * Response constructor.
     *
     * @param Header     $header
     * @param JsonParser $json
     * @param Download   $download
     */
    #[Pure] public function __construct(Header $header, JsonParser $json, Download $download)
    {

        $this->headers = $header;
        $this->json = $json;
        $this->download = $download;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Create a response with specific content, status and headers
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $content
     * @param int         $statusCode
     * @param array       $headers
     *
     * @return $this
     */
    public function create(?string $content = null, int $statusCode = 200, array $headers = []): Response
    {

        $this->listHeaders = $headers;
        $this
            ->setContent($content)
            ->responseStatus($statusCode);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the content that will be displayed on the screen when receiving a response
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|int|null $content
     *
     * @return $this
     */
    public function setContent(string|int|null $content): Response
    {

        $this->content = $content;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the added content
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string|int|null
     */
    public function getContent(): string|int|null
    {

        return $this->content;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Send content to be displayed on the page
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     */
    public function sendContent(): Response
    {

        echo $this->getContent();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set response status. Default 200 OK
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $code
     *
     * @return $this
     */
    public function responseStatus(int $code): Response
    {

        $this->responseCode = $code;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get HTTP response status
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     */
    public function getResponseStatus(): int
    {

        return $this->headers->getHttpStatus();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Main method that sends headers, response status
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     */
    public function sendHeaders(): Response
    {

        $this->headers
            ->set($this->listHeaders)
            ->setResponseCode($this->responseCode)
            ->send();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method is almost identical to the previous "sendHeaders", but with one
     * & difference, this method not only sends all headers and response status, but
     * & also displays the content that is added to "setContent()"
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return Response
     */
    public function concateSendHeaders(): Response
    {

        $this->sendContent();
        $this->headers
            ->set($this->listHeaders)
            ->setResponseCode($this->responseCode)
            ->send();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Send a json response with the required headers and status, including the
     * & json text itself
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array|string|int|object $data
     * @param int                     $status
     * @param array                   $headers
     *
     * @return string
     * @throws JsonErrorException
     */
    public function json(array|string|int|object $data, int $status = 200, array $headers = []): string
    {

        $jsonData = $this->json->setData($data)->encode();

        return $this->create($jsonData, $status, array_merge(
            $headers,
            ['Content-Type' => 'text/json']
        ))->concateSendHeaders();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the title to the type of content received. Default text/html
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $type
     *
     * @return $this
     */
    public function setContentType(string $type): Response
    {

        $this->headers
            ->setContentType($type)
            ->send();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set header content encoding
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $charset
     *
     * @return $this
     * @throws NoHeaderFoundInSubException
     */
    public function setCharset(string $charset): Response
    {

        $this->headers
            ->setContentType($this->headers->getHeader('Content-Type'))
            ->setCharset($charset)
            ->send();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method checks if the response code is 100(Continue)
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function isContinue(): bool
    {

        return 100 === $this->getResponseStatus();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method checks if the response code is 200(OK)
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function isOk(): bool
    {

        return 200 === $this->getResponseStatus();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method checks if the response code is 301,302(Redirect-Location)
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function isRedirect(): bool
    {

        return in_array($this->getResponseStatus(), [301, 302]);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method checks if the response code is 403(Access is denied)
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function isForbidden(): bool
    {

        return 403 === $this->getResponseStatus();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method checks if the response code is 404(Not Found)
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function isNotFound(): bool
    {

        return 404 === $this->getResponseStatus();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method checks if the received response code is informational
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function isInformational(): bool
    {

        return $this->getResponseStatus() >= 100 && $this->getResponseStatus() < 200;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method checks if the received response code is successful
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function isSuccess(): bool
    {

        return $this->getResponseStatus() >= 200 && $this->getResponseStatus() < 300;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method checks if the received response code is redirected
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function isRedirection(): bool
    {

        return $this->getResponseStatus() >= 300 && $this->getResponseStatus() < 400;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method checks if the received response code is a client error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function isClientError(): bool
    {

        return $this->getResponseStatus() >= 400 && $this->getResponseStatus() < 500;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method checks if the received response code is a northern error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function isServerError(): bool
    {

        return $this->getResponseStatus() >= 500;

    }

}