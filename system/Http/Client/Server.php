<?php

namespace System\Http\Client;

use JetBrains\PhpStorm\Pure;

/**
 * Class Server
 * @package System\Http\Client
 *
 * @author  Codememory
 */
class Server
{

    /**
     * @var array
     */
    private array $server;

    /**
     * Server constructor.
     */
    public function __construct()
    {

        $this->server = $_SERVER;

    }

    /**
     * @return array
     */
    public function getAll(): array
    {

        return $this->server;

    }

    /**
     * @param string $key
     *
     * @return string|null
     */
    public function get(string $key): ?string
    {

        return $this->server[$key] ?? null;

    }

    /**
     * @param mixed ...$keys
     *
     * @return array
     */
    public function getMultiple(...$keys): array
    {

        $datas = [];

        foreach ($keys as $keyName) {
            if (array_key_exists($keyName, $this->server)) {
                $datas[$keyName] = $this->get($keyName);
            }
        }

        return $datas;

    }

    /**
     * @param      $key
     * @param null $value
     *
     * @return $this
     */
    public function set($key, $value = null): object
    {

        if (is_string($key)) {
            $this->server[$key] = $value;
        } else if (is_array($key)) {
            foreach ($key as $keyName => $value) {
                $this->server[$keyName] = $value;
            }
        }

        return $this;

    }

    /**
     * @param string $key
     *
     * @return bool
     */
    #[Pure] public function has(string $key): bool
    {

        return array_key_exists($key, $this->server);

    }

}