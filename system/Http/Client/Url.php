<?php

namespace System\Http\Client;

/**
 * Class Url
 * @package System\Http\Client
 *
 * @author  Codememory
 */
class Url
{

    /**
     * @var Server
     */
    private Server $server;

    /**
     * Url constructor.
     *
     * @param Server $server
     */
    public function __construct(Server $server)
    {

        $this->server = $server;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method gets the current url of the page
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    public function current(): string
    {

        $fullUrl = $this->server->get('REQUEST_URI');

        if (preg_match('/(?<url>.*)\?.*/', $fullUrl, $match)) {
            $fullUrl = $match['url'];
        }

        if ($fullUrl !== '/' && $fullUrl !== '') {
            $fullUrl = sprintf('%s%s%s', '/', trim($fullUrl, '/'), '/');
        }

        return $fullUrl;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the request method in uppercase.
     * & But if you pass the argument $ case = down, the
     * & request method will be returned in lower case
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param mixed $case
     *
     * @return string
     */
    public function getMethod(?string $case = 'up'): string
    {

        $method = $this->server->get('REQUEST_METHOD');

        if ($case === 'up') {
            $method = strtoupper($this->server->get('REQUEST_METHOD'));
        } else if ($case === 'down') {
            $method = strtolower($this->server->get('REQUEST_METHOD'));
        }

        return $method;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns an array as a parameter in url with a key and
     * & value. You can get a specific parameter by specifying
     * & the $key argument
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param mixed $key
     *
     * @return array
     */
    public function getQuery(?string $key = null): array
    {

        $arrayQuery = [];
        $query = $this->server->get('QUERY_STRING');
        $quires = explode('&', $query);

        if ($quires !== []) {
            foreach ($quires as $query) {
                $queryValues = explode('=', $query);

                $arrayQuery[$queryValues[0]] = $queryValues[1];
            }
        }

        if ($key !== null) {
            return $arrayQuery[$key] ?? $arrayQuery;
        } else {
            return $arrayQuery;
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method returns the url of the previous page, if
     * & there is no previous page, it will return the current url
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string|null
     */
    public function getPrevUrl(): ?string
    {

        if ($this->server->has('HTTP_REFERER')) {
            return $this->server->get('HTTP_REFERER');
        }

        return $this->current();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method merges urls. The first argument should be the url you want to add to the current
     * & one. The second argument is an array of parameters key => value. The third argument
     * & boolean => true means do not delete the current query parameters; false => delete
     * & the current parameters
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param mixed $url
     * @param mixed $params
     * @param mixed $saveCurrentQuery
     *
     * @return string|null
     */
    public function mergeUrl(string $url, array $params = [], bool $saveCurrentQuery = true): ?string
    {

        $url = trim($url, '/') . '/';
        $fullUrl = sprintf('%s%s', $this->current(), $url);
        $queryParams = $this->getQuery();

        if ($saveCurrentQuery === true) {
            if ($params !== []) {
                foreach ($params as $key => $value) {
                    $queryParams[$key] = $value;
                }
            }
        }

        $query = http_build_query($queryParams);

        if ($query !== null && !empty($query)) {
            return sprintf('%s%s%s', $fullUrl, '?', $query);
        }

        return null;

    }


}