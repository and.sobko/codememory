<?php


namespace System\Http\Exceptions;

use ErrorException;

/**
 * Class DownloadableResourceNotFoundException
 * @package System\Http\Exceptions
 *
 * @author Codememory
 */
class DownloadableResourceNotFoundException extends ErrorException
{

    /**
     * DownloadableResourceNotFoundException constructor.
     *
     * @param string $resourceName
     */
    public function __construct(string $resourceName)
    {
        parent::__construct(
            sprintf(
                'Unable to download file <b>%s</b> this file does not exist, or the name is incorrect',
                $resourceName
            )
        );
    }

}