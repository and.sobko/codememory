<?php

namespace System\Http\Request;

use JetBrains\PhpStorm\Pure;
use PHPHtmlParser\Dom;
use System\Http\Request\Upload\Uploader;
use Url;

/**
 * Class Request
 * @package System\Http
 *
 * @author  Codememory
 */
class Request
{

    public const REQUEST_DATA_POST = 1;
    public const REQUEST_DATA_QUERY = 2;
    public const REQUEST_DATA_ALL = 3;

    /**
     * @var Dom
     */
    public Dom $dom;

    /**
     * @var Parser
     */
    public Parser $parser;

    /**
     * @var Uploader
     */
    public Uploader $upload;

    /**
     * @var int
     */
    private int $selectedType = self::REQUEST_DATA_ALL;

    /**
     * Request constructor.
     */
    public function __construct()
    {

        $this->initializer();

    }

    private function initializer(): void
    {

        $this->dom = new Dom();
        $this->parser = new Parser($this->dom);
        $this->upload = new Uploader($this);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the type of selection given when calling the `all ()` method
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $type
     *
     * @return $this
     */
    public function selectType(int $type): Request
    {

        $this->selectedType = $type;

        return $this;

    }

    /**
     * @param array      $data
     * @param string     $key
     * @param mixed|null $default
     *
     * @return mixed
     */
    #[Pure] private function retrieval(array $data, string $key, mixed $default = null): mixed
    {

        if (array_key_exists($key, $data)) {
            return $data[$key];
        }

        return $default;

    }

    /**
     * @param array             $data
     * @param string|array|null $keys
     * @param mixed|null        $default
     *
     * @return mixed
     */
    private function dataRetrieval(array $data, string|null|array $keys = null, mixed $default = null): mixed
    {

        if (null === $keys) {
            return $data;
        } else if (is_array($keys)) {
            $updatedData = [];

            foreach ($keys as $key => $keyName) {
                $updatedData[$keyName] = $data[$keyName];
            }

            return $updatedData;
        } else {
            return $this->keyCall($keys, $data);
        }

    }

    /**
     * @param string $keys
     * @param array  $data
     *
     * @return mixed
     */
    private function keyCall(string $keys, array $data): mixed
    {

        $splitKeyString = explode('.', $keys);

        foreach ($splitKeyString as $key) {
            if (array_key_exists($key, $data)) {
                $data = $data[$key];
            } else {
                return null;
            }
        }

        return $data;

    }

    /**
     * @param array $arrayData
     * @param mixed ...$args
     *
     * @return array|string|null
     */
    #[Pure] private function getBodyByKeys(array $arrayData, ...$args): array|string|null
    {

        $argsData = [];

        foreach ($args as $key) {
            if (array_key_exists($key, $arrayData)) {
                $argsData[$key] = $arrayData[$key];
            }
        }

        return count($args) === 1 ? $arrayData[$args[0]] : $argsData;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get Body response from php://input
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param mixed ...$args
     *
     * @return array|string
     */
    public function getBody(...$args): array|string
    {

        $input = file_get_contents('php://input');
        parse_str($input, $arrayData);

        return $this->getBodyByKeys(array_merge($arrayData, $_POST), ...$args);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get POST data that is sent by the form
     * --------------------------------------------------------------------------------------------
     * 1 argument key to get
     * 2 - the argument is the default value if value is obtained through the key === null
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $key
     * @param mixed|null  $default
     *
     * @return mixed
     */
    public function post(?string $key = null, mixed $default = null): mixed
    {

        return $this->dataRetrieval($this->getBody(), $key, $default);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get GET parameters that go to the URL via `?`
     * --------------------------------------------------------------------------------------------
     * 1 argument key to get
     * 2 - the argument is the default value if value is obtained through the key === null
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $key
     * @param mixed|null  $default
     *
     * @return mixed
     */
    public function query(?string $key = null, mixed $default = null): mixed
    {

        return $this->dataRetrieval($_GET, $key, $default);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns an array of multiple superglobal data POST, GET, php://input
     * --------------------------------------------------------------------------------------------
     * 1 argument key to get
     * 2 - the argument is the default value if value is obtained through the key === null
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|array|null $key
     * @param mixed|null        $default
     *
     * @return mixed
     */
    public function all(string|null|array $key = null, mixed $default = null): mixed
    {

        $certainData = [];
        $data = match ($this->selectedType) {
            1 => $this->post(),
            2 => $this->query(),
            3 => array_merge($this->post(), $this->query())
        };

        if (is_array($key)) {
            foreach ($key as $keyName) {
                $certainData[$keyName] = $this->dataRetrieval($data, $keyName, $default);
            }
        } else {
            $certainData = $this->dataRetrieval($data, $key, $default);
        }

        return $certainData;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get an object of class Files in which information about the superglobal array $_FILES
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $input
     *
     * @return Files
     */
    #[Pure] public function file(string $input): Files
    {

        return new Files($input);

    }


    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>
     * & ast to boolean
     * <=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $key
     *
     * @return bool
     */
    public function inBoolean(string $key): bool
    {

        return match ($this->all($key)) {
            '1', 1, true, 'true', 'yes', 'on', 'enabled' => true,
            default => false
        };

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Check a value from an array for filling
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $key
     *
     * @return bool
     */
    public function isFilled(string $key): bool
    {

        return null === $this->all($key) ||
        empty($this->all($key)) ? false : true;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Check any value from the specified array for filling
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $keys
     *
     * @return bool
     */
    public function anyFilled(array $keys): bool
    {

        foreach ($keys as $key) {
            if ($this->isFilled($key)) {
                return true;
            }
        }

        return false;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Check Any value from an array for empty
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $keys
     *
     * @return bool
     */
    public function anyNotFilled(array $keys): bool
    {

        foreach ($keys as $key) {
            if (!$this->isFilled($key)) {
                return true;
            }
        }

        return false;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Execute active if value is filled
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string   $key
     * @param callable $callback
     *
     * @return mixed
     * @noinspection PhpMixedReturnTypeCanBeReducedInspection
     */
    public function whenFiled(string $key, callable $callback): mixed
    {

        if ($this->isFilled($key)) {
            return call_user_func($callback);
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Check the key for existence
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $key
     *
     * @return bool
     */
    public function missing(string $key): bool
    {

        return array_key_exists($key, $this->all());

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns true if at least 1 of the specified keys in the array exists
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $keys
     *
     * @return bool
     */
    public function anyMissing(array $keys): bool
    {

        foreach ($keys as $key) {
            if (!$this->missing($key)) {
                return true;
            }
        }

        return false;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Callback if the specified key is missing
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string   $key
     * @param callable $callback
     *
     * @return $this
     * @noinspection PhpMixedReturnTypeCanBeReducedInspection
     */
    public function whenMissing(string $key, callable $callback): mixed
    {

        if (!$this->missing($key)) {
            return call_user_func($callback);
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Return current URL-path
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    public function path(): string
    {

        return Url::current();

    }

    /**
     * @param string $path
     *
     * @return bool
     */
    private function isPathString(string $path): bool
    {

        $path = str_replace(['/'], ['\/'], $path);

        if (preg_match(sprintf('/^%s$/', $path), $this->path())) {
            return true;
        }

        return false;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Check the current URL-path for the one specified in the argument
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|array $path
     *
     * @return bool
     */
    public function isPath(string|array $path): bool
    {

        if (is_string($path)) {
            return $this->isPathString($path);
        } else {
            foreach ($path as $item) {
                if ($this->isPathString($item)) {
                    return true;
                }
            }

            return false;
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Return the current request method
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    public function method(): string
    {

        return Url::getMethod();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Check the current request method for the method specified in the argument
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $method
     *
     * @return bool
     */
    public function isMethod(string $method): bool
    {

        return strtoupper($method) === $this->method();

    }

    /**
     * @param string $property
     *
     * @return mixed
     */
    public function __get(string $property): mixed
    {

        return $this->all($property);

    }

}