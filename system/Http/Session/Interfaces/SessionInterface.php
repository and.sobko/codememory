<?php

namespace System\Http\Session\Interfaces;

use System\Http\Session\Session;

/**
 * Interface SessionInterface
 * @package System\Http\Session\Interfaces
 *
 * @author  Codememory
 */
interface SessionInterface
{

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return Session
     */
    public function create(string $key, mixed $value): Session;

    /**
     * @return Session
     */
    public function temp(): Session;

    /**
     * @param int $time
     *
     * @return Session
     */
    public function life(int $time): Session;

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key): mixed;

    /**
     * @return array
     */
    public function all(): array;

    /**
     * @param string $key
     *
     * @return bool
     */
    public function destroy(string $key): bool;

    /**
     * @return bool
     */
    public function destroyAll(): bool;

}