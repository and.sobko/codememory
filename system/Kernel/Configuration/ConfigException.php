<?php

namespace Kernel\Configuration;

use \ErrorException;

/**
 * Class ConfigException
 * @package Kernel\Configuration
 *
 * @author Codememory
 */
abstract class ConfigException extends ErrorException
{

    /**
     * ConfigException constructor.
     *
     * @param $message
     */
    public function __construct($message)
    {

        parent::__construct($message);

    }

    /**
     * @return array
     */
    public function getVariables(): array
    {

        return [];

    }

}