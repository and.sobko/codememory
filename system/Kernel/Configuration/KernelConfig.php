<?php

namespace Kernel\Configuration;

/**
 * Class KernelConfig
 * @package Kernel
 *
 * @author  Codememory
 */
class KernelConfig
{

    use ConfigurationMethods;

    private const STARTUP_SEQUENCE = [
        'versionCheck'
    ];

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method collects all methods and includes the template in case of a configuration error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return KernelConfig
     */
    private function assemblingConfigurationMethods(): KernelConfig
    {

        try {
            $executors = $this;

            foreach (self::STARTUP_SEQUENCE as $method) {
                $executors = $executors->$method();
            }

            return $executors;
        } catch (ConfigException $e) {
            $vars = [ 'message' => $e->getMessage() ];

            foreach ($e->getVariables() as $name => $value) {
                $vars[$name] = $value;
            }

            extract($vars);

            require_once sprintf('%s/view/template.php', __DIR__);
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method runs all configuration methods
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return KernelConfig
     */
    public function exec(): KernelConfig
    {

        return $this->assemblingConfigurationMethods();

    }

}