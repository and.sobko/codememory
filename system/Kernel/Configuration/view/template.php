<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>404 HTML Template by Colorlib</title>

    <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
</head>
<body>
<div class="notfound-404">
    <h1 class="logo">
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" width="153px" height="100%" preserveAspectRatio="xMidYMid meet" viewBox="0 5.27189342847123e-7 227.9774169921875 147" overflow="visible"><defs id="SvgjsDefs114165"></defs><g id="SvgjsG114166" transform="scale(0.700391449076773)" opacity="1"><g id="SvgjsG114167" class="uVPoPB6Kx" transform="translate(91.47843046101305, -1.8068300399445572) scale(1.3813685320677045)" light-content="false" non-strokable="false" fill="#ba031e"><path d="M93.384 8.29l-.162-1.5-1.476.312c-.998.212-17.142 3.784-30.67 14.86-.426-5.235-2.74-14.761-9.23-19.845l-1.031-.809-.85.999c-.525.62-12.882 15.455-12.41 38.601l.031.255c.013.064 1.318 6.396-1.517 10.042-1.23 1.583-3.057 2.409-5.57 2.535a23.423 23.423 0 0 0-5.171-.598c-7.879 0-13.739 3.872-15.383 10.129-2.249 2.834-2.346 6.207-.253 9.303l1.262 1.86 1.077-1.974c.016-.029 1.678-2.951 3.941-3.042.119.009.364.021.662.026.222.018.481.016.674-.002 1.037-.029 2.191-.188 4.191-.771v.002l.093-.073c.006-.002.038-.057.292-.057.673 0 2.449.424 4.475 4.336.19.471 1.261 3.002 3.299 5.867-.948 2.246-2 5.308-1.844 7.513a4.679 4.679 0 0 0-1.405-.177l.072 2.038c-.296-.509-.636-.991-1.062-1.394a4.565 4.565 0 0 0-3.371-1.268l.098 2.725c.574-.021 1 .146 1.399.521.156.147.297.322.427.513a3.21 3.21 0 0 0-.233.097c-.943-.605-1.971-1.027-2.985-.996-.816.019-1.981.285-2.898 1.633l2.254 1.317c.32-.47.58-.771.705-.771h.027c1.192 0 3.222 2.178 4.415 3.679l.002.155 1.924 3.007 2.288-1.462-1.324-2.039.089-.211c.635.605 1.222 1.27 1.649 1.838l.13.17 1.53 2.362 2.288-1.48-1.324-2.046 1.805-4.686c.128.02.252-.287.386-.287h.012c.086 0 .172.33.259.322.132-.012.266.128.399.1.017-.004.032.078.049.074.216-.049.435-.081.654-.173.033-.015.066-.01.101-.024.182-.081.363-.164.546-.271.037-.021.073-.037.11-.06.941-.574 1.883-1.462 2.752-2.525.897.344 1.802.606 2.723.788a9.783 9.783 0 0 1-.351 7.518l-.715 1.45 1.468.44c.105.028 2.479.734 6.704.771l1.09.012V96.09l1.481 1.763 1.083-.07c6.443-.564 11.89-2.866 16.187-6.842l2.1-1.944-2.834-.406c-.689-.099-16.521-2.529-16.638-16.523 1.385-.431 4.104-1.36 7.54-2.971l1.104-.517-.838-2.479 3.363 1.167.532-.294a66.995 66.995 0 0 0 7.57-4.868l1.537-1.143-.023-.016.363-.291c3.836-3.086 7.21-6.559 10.03-10.322l1.481-1.98-3.039-.242 3.86-1.11.277-.444a52.686 52.686 0 0 0 5.156-10.91l.704-2.075-5.627.74 6.536-4.434.113-.538c1.379-6.518 1.669-13.599.865-21.051zM38.221 52.879c3.382-4.349 2.264-11.075 2.059-12.146-.342-18.565 8.181-31.797 10.927-35.568 6.027 5.741 7.494 16.678 7.211 19.094-5.485 5.063-10.306 11.491-13.154 19.593l-.025.078c-.029.104-2.891 9.896-9.019 10.813a8.752 8.752 0 0 0 2.001-1.864zM11.064 69.197c-.349-1.28-.125-2.521.654-3.714.7.255 1.729.728 2.56 1.513-1.343.453-2.43 1.35-3.214 2.201zm9.906-1.491l-.079-.341.162.552-.083-.211zm11.822 14.748c-.285.89-.531 1.804-.681 2.677-.024.136-.044.264-.063.391-.815.509-1.311.691-1.542.742-.148-.777.323-2.834 1.12-5.044.37.416.755.829 1.166 1.234zm-1.824 6.496c.34-.063.679-.166 1.004-.298l-.539 1.299a9.066 9.066 0 0 0-.465-1.001zm-3.732-.037l-.169.405c-.065-.166-.144-.328-.219-.492.136.017.266.044.388.087zm62.727-60.672L74.821 38.516l12.633-1.662a50.024 50.024 0 0 1-3.857 7.819l-17.278 4.965 13.002 1.035a55.568 55.568 0 0 1-7.522 7.469L60.79 56.157l7.173 4.886a64.787 64.787 0 0 1-5.173 3.25l-7.467-2.594 1.835 5.435c-4.105 1.83-6.934 2.578-6.963 2.586l-.971.255-.033 1.008c-.559 13.27 10.756 18.107 16.171 19.658-3.289 2.378-7.174 3.836-11.667 4.347L48.5 87.215v7.931c-2-.065-2.546-.202-3.346-.331 1.519-4.833-.535-8.756-.631-8.938l-.361-.615-.688-.1a13.4 13.4 0 0 1-2.264-.542c.42-.696.801-1.413 1.127-2.125l-2.479-1.136a17.992 17.992 0 0 1-1.975 3.331 13.61 13.61 0 0 1-.958 1.164c-.084.091-.162.163-.243.245a8.575 8.575 0 0 1-.663.613l-.115.091c-.537.42-.949.61-1.165.634a.01.01 0 0 0-.002-.005c-.006-.012-.01-.03-.016-.043-.06-.187-.18-.846.231-2.527.044-.176.091-.356.143-.543l.009-.029a28.647 28.647 0 0 1 .559-1.756l.253-.716c.328-.879.707-1.787 1.121-2.67L34.57 77.99c-.208.442-.459 1-.722 1.625-3.322-3.655-4.96-7.752-4.978-7.797l-.057-.127c-2.495-4.854-5.146-5.871-6.93-5.871a3.9 3.9 0 0 0-1.387.239l-.156.051c-1.018.399-2.059.54-2.865.587-.617-1.08-1.469-1.897-2.344-2.506l3.693-1.3.26-4.257s-3.342 3.997-4.293 5.33a11.01 11.01 0 0 0-1.896-.953c1.939-5.21 7.562-7.086 12.43-7.086 2.238 0 4.523.436 6.607 1.152 1.137.391 2.268.703 3.361.703h.001c8.635 0 12.274-11.995 12.552-12.945 8.462-23.994 36.229-32.889 42.979-34.695.559 6.39.27 12.469-.862 18.101z"></path></g><g id="SvgjsG114168" class="text" transform="translate(161.54, 199.4626312164602) scale(1)" light-content="false" fill="#ba031e"><path d="M-158.1 -3.56C-155.81 -0.69 -151.93 0.74 -146.47 0.74C-141.23 0.74 -137.51 -0.57 -135.31 -3.19C-133.12 -5.82 -132.03 -9.49 -132.03 -14.2L-132.03 -18.54L-142.88 -18.54L-142.88 -13.27C-142.88 -11.28 -143.1 -9.76 -143.53 -8.71C-143.96 -7.66 -144.95 -7.13 -146.47 -7.13C-148 -7.13 -149.03 -7.66 -149.54 -8.71C-150.06 -9.76 -150.32 -11.28 -150.32 -13.27L-150.32 -36.89C-150.32 -38.83 -150.07 -40.35 -149.58 -41.45C-149.08 -42.54 -148.05 -43.09 -146.47 -43.09C-144.9 -43.09 -143.91 -42.57 -143.5 -41.54C-143.09 -40.51 -142.88 -39.02 -142.88 -37.08L-142.88 -32.05L-132.03 -32.05L-132.03 -36.39C-132.03 -41.02 -133.12 -44.6 -135.31 -47.12C-137.51 -49.64 -141.23 -50.9 -146.47 -50.9C-151.97 -50.9 -155.86 -49.49 -158.13 -46.65C-160.4 -43.82 -161.54 -39.64 -161.54 -34.1L-161.54 -16C-161.54 -10.58 -160.39 -6.44 -158.1 -3.56Z M-123.94 -2.36C-121.73 -0.37 -118.66 0.62 -114.73 0.62C-110.8 0.62 -107.72 -0.38 -105.49 -2.39C-103.26 -4.39 -102.15 -7.3 -102.15 -11.1L-102.15 -24.74C-102.15 -28.54 -103.26 -31.44 -105.49 -33.45C-107.72 -35.45 -110.8 -36.46 -114.73 -36.46C-118.66 -36.46 -121.73 -35.46 -123.94 -33.48C-126.15 -31.5 -127.25 -28.58 -127.25 -24.74L-127.25 -11.1C-127.25 -7.25 -126.15 -4.34 -123.94 -2.36ZM-112.68 -7.16C-113.06 -6.44 -113.72 -6.08 -114.67 -6.08C-115.62 -6.08 -116.29 -6.44 -116.68 -7.16C-117.08 -7.88 -117.27 -8.97 -117.27 -10.42L-117.27 -25.42C-117.27 -26.83 -117.08 -27.9 -116.68 -28.64C-116.29 -29.39 -115.62 -29.76 -114.67 -29.76C-113.72 -29.76 -113.06 -29.4 -112.68 -28.67C-112.31 -27.95 -112.13 -26.87 -112.13 -25.42L-112.13 -10.42C-112.13 -8.97 -112.31 -7.88 -112.68 -7.16Z M-94.98 -2.17C-93.56 -0.31 -91.5 0.62 -88.81 0.62C-86.46 0.62 -84.1 -0.52 -81.75 -2.79L-81.75 0L-71.64 0L-71.64 -50.22L-81.75 -50.22L-81.75 -33.6C-83.94 -35.51 -86.29 -36.46 -88.81 -36.46C-91.5 -36.46 -93.56 -35.44 -94.98 -33.42C-96.41 -31.39 -97.12 -28.66 -97.12 -25.23L-97.12 -10.42C-97.12 -6.78 -96.41 -4.03 -94.98 -2.17ZM-81.75 -6.76C-82.74 -6.14 -83.63 -5.83 -84.41 -5.83C-85.28 -5.83 -85.92 -6.19 -86.33 -6.91C-86.75 -7.64 -86.95 -8.6 -86.95 -9.8L-86.95 -26.29C-86.95 -27.45 -86.74 -28.39 -86.3 -29.11C-85.87 -29.83 -85.22 -30.19 -84.35 -30.19C-83.61 -30.19 -82.74 -29.93 -81.75 -29.39Z M-62.84 -2.51C-60.65 -0.42 -57.59 0.62 -53.66 0.62C-49.9 0.62 -46.98 -0.31 -44.89 -2.17C-42.8 -4.03 -41.76 -6.76 -41.76 -10.35L-41.76 -13.83L-51.06 -13.83L-51.06 -10.04C-51.06 -7.73 -51.92 -6.57 -53.66 -6.57C-55.31 -6.57 -56.14 -7.85 -56.14 -10.42L-56.14 -17.3L-41.76 -17.3L-41.76 -24.49C-41.76 -28.42 -42.75 -31.39 -44.73 -33.42C-46.72 -35.44 -49.69 -36.46 -53.66 -36.46C-57.59 -36.46 -60.65 -35.41 -62.84 -33.33C-65.03 -31.24 -66.12 -28.29 -66.12 -24.49L-66.12 -11.35C-66.12 -7.54 -65.03 -4.6 -62.84 -2.51ZM-51.06 -22.01L-56.14 -22.01L-56.14 -25.05C-56.14 -26.62 -55.93 -27.71 -55.52 -28.33C-55.11 -28.95 -54.51 -29.26 -53.72 -29.26C-52.9 -29.26 -52.25 -28.98 -51.77 -28.43C-51.29 -27.87 -51.06 -26.93 -51.06 -25.61Z M-36.24 -35.84L-36.24 0L-26.32 0L-26.32 -28.27C-25.04 -29.14 -23.92 -29.57 -22.97 -29.57C-21.52 -29.57 -20.8 -28.64 -20.8 -26.78L-20.8 0L-11 0L-11 -27.84L-11 -28.27C-10.47 -28.69 -9.89 -29.01 -9.27 -29.23C-8.65 -29.46 -8.11 -29.57 -7.66 -29.57C-6.95 -29.57 -6.43 -29.36 -6.08 -28.92C-5.72 -28.49 -5.55 -27.78 -5.55 -26.78L-5.55 0L4.37 0L4.37 -27.84C4.37 -30.52 3.74 -32.64 2.48 -34.19C1.22 -35.74 -0.46 -36.52 -2.57 -36.52C-6.13 -36.52 -9.17 -35.09 -11.69 -32.24C-12.22 -33.65 -13.02 -34.71 -14.07 -35.43C-15.13 -36.16 -16.38 -36.52 -17.82 -36.52C-21.09 -36.52 -23.92 -35.34 -26.32 -32.98L-26.32 -35.84Z M12.99 -2.51C15.18 -0.42 18.24 0.62 22.17 0.62C25.93 0.62 28.85 -0.31 30.94 -2.17C33.03 -4.03 34.07 -6.76 34.07 -10.35L34.07 -13.83L24.77 -13.83L24.77 -10.04C24.77 -7.73 23.9 -6.57 22.17 -6.57C20.51 -6.57 19.69 -7.85 19.69 -10.42L19.69 -17.3L34.07 -17.3L34.07 -24.49C34.07 -28.42 33.08 -31.39 31.09 -33.42C29.11 -35.44 26.13 -36.46 22.17 -36.46C18.24 -36.46 15.18 -35.41 12.99 -33.33C10.8 -31.24 9.7 -28.29 9.7 -24.49L9.7 -11.35C9.7 -7.54 10.8 -4.6 12.99 -2.51ZM24.77 -22.01L19.69 -22.01L19.69 -25.05C19.69 -26.62 19.89 -27.71 20.31 -28.33C20.72 -28.95 21.32 -29.26 22.1 -29.26C22.93 -29.26 23.58 -28.98 24.06 -28.43C24.53 -27.87 24.77 -26.93 24.77 -25.61Z M39.59 -35.84L39.59 0L49.51 0L49.51 -28.27C50.79 -29.14 51.9 -29.57 52.86 -29.57C54.3 -29.57 55.03 -28.64 55.03 -26.78L55.03 0L64.82 0L64.82 -27.84L64.82 -28.27C65.36 -28.69 65.94 -29.01 66.56 -29.23C67.18 -29.46 67.71 -29.57 68.17 -29.57C68.87 -29.57 69.4 -29.36 69.75 -28.92C70.1 -28.49 70.28 -27.78 70.28 -26.78L70.28 0L80.2 0L80.2 -27.84C80.2 -30.52 79.57 -32.64 78.31 -34.19C77.05 -35.74 75.36 -36.52 73.25 -36.52C69.7 -36.52 66.66 -35.09 64.14 -32.24C63.6 -33.65 62.81 -34.71 61.75 -35.43C60.7 -36.16 59.45 -36.52 58 -36.52C54.74 -36.52 51.9 -35.34 49.51 -32.98L49.51 -35.84Z M88.85 -2.36C91.06 -0.37 94.13 0.62 98.05 0.62C101.98 0.62 105.06 -0.38 107.29 -2.39C109.52 -4.39 110.64 -7.3 110.64 -11.1L110.64 -24.74C110.64 -28.54 109.52 -31.44 107.29 -33.45C105.06 -35.45 101.98 -36.46 98.05 -36.46C94.13 -36.46 91.06 -35.46 88.85 -33.48C86.63 -31.5 85.53 -28.58 85.53 -24.74L85.53 -11.1C85.53 -7.25 86.63 -4.34 88.85 -2.36ZM100.1 -7.16C99.73 -6.44 99.07 -6.08 98.12 -6.08C97.16 -6.08 96.49 -6.44 96.1 -7.16C95.71 -7.88 95.51 -8.97 95.51 -10.42L95.51 -25.42C95.51 -26.83 95.71 -27.9 96.1 -28.64C96.49 -29.39 97.16 -29.76 98.12 -29.76C99.07 -29.76 99.73 -29.4 100.1 -28.67C100.47 -27.95 100.66 -26.87 100.66 -25.42L100.66 -10.42C100.66 -8.97 100.47 -7.88 100.1 -7.16Z M116.34 -35.84L116.34 0L126.45 0L126.45 -23.31C128.23 -25.63 130.21 -26.78 132.4 -26.78C133.35 -26.78 134.45 -26.54 135.69 -26.04L135.69 -36.08C135.23 -36.21 134.72 -36.27 134.14 -36.27C132.57 -36.27 131.2 -35.75 130.04 -34.72C128.89 -33.69 127.69 -32.07 126.45 -29.88L126.45 -35.84Z M140.96 3.41L140.96 10.42L143.25 10.42C146.64 10.42 149.37 9.7 151.44 8.28C153.5 6.85 154.93 4.38 155.71 0.87L163.96 -35.84L154.41 -35.84L151 -15.13L146.48 -35.84L136.99 -35.84L145.42 -5.64C146.29 -2.67 146.72 -0.66 146.72 0.37C146.72 1.53 146.26 2.33 145.33 2.76C144.4 3.19 142.94 3.41 140.96 3.41Z"></path></g></g></svg>
    </h1>
</div>
<div id="notfound">
    <div class="notfound">
        <div class="notfound-404">
            <h3>Ой! Произошла ошибка сервера</h3>
            <h1><span>5</span><span>0</span><span>0</span></h1>
        </div>
        <div class="content">
            <h2><?=$message;?></h2>
        </div>
    </div>
</div>
<style>
    .logo {
        font-family: 'Raleway', sans-serif;
        font-size: 23px;
        color: #262626;
        margin: 25px;
        letter-spacing: 2px;
        margin-bottom: 0px;
    }

    * {
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }

    .content {
        background: #FEFEFE;
        width: 100%;
        height: auto;
        padding: 1rem;
        border: 1px solid #DCDCDC;
        border-radius: 0.25rem;
    }

    .content h2 {
        font-size: 1.25em;
        line-height: 1.3;
        color: #e30528!important;
    }

    body {
        padding: 0;
        margin: 0;
    }

    #notfound {
        position: relative;
        height: 100vh;
    }

    #notfound .notfound {
        position: absolute;
        left: 50%;
        top: 40%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
    }

    .notfound {
        max-width: 520px;
        width: 100%;
        line-height: 1.4;
        text-align: center;
    }

    .notfound .notfound-404 {
        position: relative;
        height: 240px;
    }

    .notfound .notfound-404 h1 {
        font-family: 'Montserrat', sans-serif;
        position: absolute;
        left: 50%;
        top: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        font-size: 252px;
        font-weight: 900;
        margin: 0px;
        color: #262626;
        text-transform: uppercase;
        letter-spacing: -40px;
        margin-left: -20px;
    }

    .notfound .notfound-404 h1>span {
        text-shadow: -8px 0px 0px #fff;
    }

    .notfound .notfound-404 h3 {
        font-family: 'Cabin', sans-serif;
        position: relative;
        font-size: 16px;
        font-weight: 700;
        text-transform: uppercase;
        color: #262626;
        margin: 0px;
        letter-spacing: 3px;
        padding-left: 6px;
    }

    .notfound h2 {
        font-family: 'Cabin', sans-serif;
        font-size: 20px;
        font-weight: 400;
        text-transform: uppercase;
        color: #000;
        margin-top: 0px;
        margin-bottom: 25px;
    }

    @media only screen and (max-width: 767px) {
        .notfound .notfound-404 {
            height: 200px;
        }
        .notfound .notfound-404 h1 {
            font-size: 200px;
        }
    }

    @media only screen and (max-width: 480px) {
        .notfound .notfound-404 {
            height: 162px;
        }
        .notfound .notfound-404 h1 {
            font-size: 162px;
            height: 150px;
            line-height: 162px;
        }
        .notfound h2 {
            font-size: 16px;
        }
    }
</style>
</body>
</html>
