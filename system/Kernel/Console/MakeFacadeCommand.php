<?php

namespace Kernel\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use System\FileSystem\Different\Exceptions\IncorrectPathException;
use Yaml;
use File;

/**
 * Class MakeServiceCommand
 * @package Kernel\Console
 *
 * @author  Codememory
 */
class MakeFacadeCommand extends Command
{

    const CORE_PATH = 'system/Kernel/%s';

    protected function configure()
    {

        $this
            ->setName('make:facade')
            ->addArgument('name', InputArgument::REQUIRED, 'Имя фасада')
            ->addOption('service', 's', InputOption::VALUE_NONE, 'Фасад для сервис провайдеа')
            ->addOption('re-create', 'r', InputOption::VALUE_NONE, 'Пересоздать фасад, если он существует')
            ->addOption('update-aliases', '-u', InputOption::VALUE_NONE, 'Обновить все алиасы фасадов')
            ->setDescription('Создание фасада');

    }

    /**
     * @return array
     */
    private function getServices(): array
    {

        $services = Yaml::open('configs/providers.yaml');

        return $services->parse();

    }

    /**
     * @return array
     */
    private function getFacades(): array
    {

        $services = Yaml::open('configs/facades.yaml');

        return $services->parse();

    }

    /**
     * @param string       $serviceName
     * @param SymfonyStyle $style
     *
     * @return bool
     */
    private function checkServiceExist(string $serviceName, SymfonyStyle $style): bool
    {

        if (!array_key_exists($serviceName, $this->getServices()['services'])) {
            $style
                ->error([
                    'Провайдер не существует',
                    sprintf('Сервис провайдер %s не существует', $serviceName)
                ]);

            return false;
        }

        return true;

    }

    /**
     * @param string       $facadeName
     * @param SymfonyStyle $style
     *
     * @return bool
     */
    private function checkFacadeExist(string $facadeName, SymfonyStyle $style): bool
    {

        if (array_key_exists($facadeName, $this->getFacades()['facades'])) {
            $style
                ->error([
                    sprintf('Фасад %s уже существует', $facadeName)
                ]);

            return false;
        }

        return true;

    }

    /**
     * @param InputInterface $input
     * @param SymfonyStyle   $style
     * @param                $stubFacade
     *
     * @return bool
     */
    private function withService(InputInterface $input, SymfonyStyle $style, &$stubFacade): bool
    {

        $serviceName = null;

        $style->ask('Введите название сервис провайдера', null, function (string $serviceName) use ($input, $style, &$stubFacade) {

            if($this->checkServiceExist($serviceName, $style)) {
                $stubFacade = str_replace(
                    ['{type}', '{return}'],
                    ['string', sprintf("return self::\$service = '%s';", $serviceName)],
                    $stubFacade
                );

                return true;
            } else {
                return $this->withService($input, $style, $stubFacade);
            }

        });

        return true;

    }

    /**
     * @param string $facadeName
     * @param        $stubFacade
     *
     * @return string|string[]
     */
    private function sameCreate(string $facadeName, $stubFacade): ?string
    {

        return str_replace(
            ['{facadeName}'], [$facadeName],
            $stubFacade
        );

    }

    /**
     * @param string         $facadeName
     * @param SymfonyStyle   $style
     * @param InputInterface $input
     * @param                $stubFacade
     *
     * @return bool
     */
    private function same(string $facadeName, SymfonyStyle $style, InputInterface $input, &$stubFacade): bool
    {

        if($input->getOption('re-create')) {

            $stubFacade = $this->sameCreate($facadeName, $stubFacade);

            return true;
        } else {
            if($this->checkFacadeExist($facadeName, $style)) {
                $stubFacade = $this->sameCreate($facadeName, $stubFacade);

                return true;
            }
        }

        return false;

    }

    /**
     * @param $stubFacade
     *
     * @return bool
     */
    private function neat(&$stubFacade): bool
    {

        $stubFacade = str_replace(
            ['{type}', '{return}'],
            ['mixed', '//TODO:'],
            $stubFacade
        );

        return true;

    }

    /**
     * @param InputInterface $input
     * @param SymfonyStyle   $style
     * @param                $stubFacade
     *
     * @return bool
     */
    private function assembly(InputInterface $input, SymfonyStyle $style, &$stubFacade): bool
    {

        $facadeName = $input->getArgument('name');

        if($this->same($facadeName, $style, $input, $stubFacade)) {
            if($input->getOption('service')) {
                return $this->withService($input, $style, $stubFacade);
            } else {
                return $this->neat($stubFacade);
            }
        }

        return false;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws IncorrectPathException
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $style = new SymfonyStyle($input, $output);
        $pathConfig = 'configs/facades.yaml';
        $pathFacades = 'system/Kernel/Facades/';
        $stubPath = '/system/Kernel/Console/Stubs/FacadeStub.stub';
        $stubFacade = File::read($stubPath);
        $facadeName = $input->getArgument('name');
        $namespace = sprintf('Kernel\Facades\%s', $facadeName);

        if($this->assembly($input, $style, $stubFacade)) {
            File::editor()->put(sprintf('%s%s.php', $pathFacades, $facadeName), $stubFacade);

            Yaml::open($pathConfig)->change(function ($data) use ($facadeName, $namespace) {
                $data['facades'][$facadeName]['namespace'] = $namespace;

                return $data;
            });

            $style->success([
                'Фасад успешно создан',
                sprintf('path: %s%s.php', $pathFacades, $facadeName),
                sprintf('namespace: %s', $namespace)
            ]);

            if($input->getOption('update-aliases')) {

                $input = new ArrayInput([
                    'command' => 'facade:updateAliases'
                ]);
                $this->getApplication()->find('facade:updateAliases')->run($input, $output);
            }
        }

        return Command::FAILURE;

    }

}