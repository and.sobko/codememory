<?php

namespace Kernel\Console;

use File;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use System\FileSystem\Different\Exceptions\IncorrectPathException;
use Yaml;

/**
 * Class MakeServiceCommand
 * @package Kernel\Console
 *
 * @author  Codememory
 */
class MakeServiceCommand extends Command
{

    /**
     * @var array
     */
    private array $dependencies = [];

    protected function configure()
    {

        $this
            ->setName('make:provider')
            ->addArgument('name', InputArgument::REQUIRED, 'Имя сервиса')
            ->addArgument('namespace', InputArgument::REQUIRED, 'Пространство имен для которого создается провайдер')
            ->setDescription('Создание сервис провайдера');

    }

    /**
     * @return array
     */
    private function getServices(): array
    {

        $services = Yaml::open('configs/providers.yaml');

        return $services->parse();

    }

    /**
     * @param string       $serviceName
     * @param SymfonyStyle $style
     *
     * @return bool
     */
    private function checkServiceExist(string $serviceName, SymfonyStyle $style): bool
    {

        if (array_key_exists($serviceName, $this->getServices()['services'])) {
            $style
                ->error([
                    'Провайдер существует',
                    sprintf('Сервис провайдер %s уже существует', $serviceName)
                ]);

            return false;
        }

        return true;

    }

    /**
     * @param string       $namespace
     * @param SymfonyStyle $style
     *
     * @return bool
     */
    private function checkServiceNamespace(string $namespace, SymfonyStyle $style): bool
    {

        if (!class_exists($namespace)) {
            $style
                ->error([
                    'Неверное пространства имен',
                    sprintf('Класс %s не найден для реализации сервис провайдера', $namespace)
                ]);

            return false;
        }

        return true;

    }

    /**
     * @param SymfonyStyle $style
     * @param              $injectedNamespace
     *
     * @return bool
     */
    private function addingDependencies(SymfonyStyle $style, $injectedNamespace): bool
    {

        if ($injectedNamespace === false || empty($injectedNamespace)) {
            return true;
        } else {
            $style->ask('Введите namespace объекта', false, function ($namespace) use ($style) {

                if ($namespace !== false && !empty($namespace)) {
                    $statusExpand = $style->confirm('Отслеживать __construct этого обьекта?', true);

                    $this->dependencies[] = [
                        'namespace'      => $namespace,
                        'checkConstruct' => $statusExpand
                    ];

                    $this->addingDependencies($style, $namespace);
                }
            });

            return false;
        }

    }

    /**
     * @param SymfonyStyle    $style
     * @param OutputInterface $output
     * @param string          $serviceName
     * @param string          $namespace
     *
     * @return string[][]
     */
    private function polls(SymfonyStyle $style, OutputInterface $output, string $serviceName, string $namespace): array
    {

        $dep = $style->confirm('Хотите передать какий-то зависимости?', false);
        $table = new Table($output);

        if ($dep === true) {
            $this->addingDependencies($style, true);

            $dependencies = [];

            if ($this->dependencies !== []) {
                foreach ($this->dependencies as $dependence) {
                    $dependencies[] = [
                        $dependence['namespace'],
                        $dependence['checkConstruct'] === true ? 'Yes' : 'No'
                    ];
                }
            }

            $style->text('        Список зависимостей');
            $table
                ->setHeaders(['namespace', 'constructor tracking'])
                ->setRows($dependencies);
            $table->render();

            if ($dependencies === []) {
                $style->text('              Пусто');
            }
        }

        $expand = $style->confirm('Хотите расширить сервис провайдер?', false);

        $expands = [];
        if ($expand === true) {
            $expands = $style->ask('Укажите имена расширителей, или нажмите ENTER в случае отмены', false, function ($expands) {
                if ($expands !== false && $expands !== [] && !empty($expands[0])) {
                    return array_map(function ($expandName) {
                        return [trim($expandName)];
                    }, explode(' ', $expands));
                }

                return [];
            });

            $style->text('  Расширители');
            if ($expands !== false) {
                $table
                    ->setHeaders(['expander name'])
                    ->setRows($expands);
                $table->render();
            }
        }

        $contextThis = $style->confirm('Разрешить доступ к контексту $this?', true);

        $method = $style->ask('Укажите имя метода, который будет запущен при старте провайдера', 'executor', function ($method) {
            if (!preg_match('/^[a-z_]+$/i', $method)) {
                throw new \RuntimeException('Имя метода должно совподать с regex /^[a-z_]$/i');
            }

            return $method;
        });

        return $this->serviceAssembly($serviceName, $namespace, $method, $this->dependencies, $expands, $contextThis !== true ? false : true);

    }

    /**
     * @param string $serviceName
     * @param string $serviceNamespace
     * @param string $method
     * @param array  $dependencies
     * @param array  $expands
     * @param bool   $contextThis
     *
     * @return string[][]
     */
    private function serviceAssembly(string $serviceName, string $serviceNamespace, string $method, array $dependencies = [], array $expands = [], bool $contextThis = true): array
    {

        $services = [
            $serviceName => [
                'namespace' => $serviceNamespace
            ]
        ];

        if ($dependencies !== []) {
            foreach ($dependencies as $dep) {
                $services[$serviceName]['dependencies']['objects'][] = [
                    $dep['namespace'], $dep['checkConstruct']
                ];
            }
        }

        if ($expands !== []) {
            $services[$serviceName]['expand'] = [];

            foreach ($expands as $expand) {
                $services[$serviceName]['expand'] += $expand;
            }
        }

        if ($contextThis === false) {
            $services[$serviceName]['executed']['as'] = 'default';
        }

        if ($method !== 'executor') {
            $services[$serviceName]['executed']['method'] = $method;
        }

        $services[$serviceName]['launch_method'] = $method;

        return $services;

    }

    /**
     * @return string[]
     */
    private function replaced(): array
    {

        return [
            '{handler_namespace}',
            '{serviceName}',
            '{dependencies}',
            '{name_handler}',
            '{name_handler_var}',
            '{expands}',
            '{launch_method}'
        ];

    }

    /**
     * @param string $serviceName
     * @param string $namespace
     * @param array  $polls
     *
     * @return array
     */
    private function replaceBy(string $serviceName, string $namespace, array $polls): array
    {

        $explodeNamespace = explode('\\', $namespace);
        $handlerName = array_pop($explodeNamespace);
        $dependencies = null;
        $expands = null;
        $replaces = [
            sprintf('use %s;', $namespace), ucfirst($serviceName) . 'Provider'
        ];

        if (isset($polls[$serviceName]['dependencies'])) {
            foreach ($polls[$serviceName]['dependencies']['objects'] as $dependency) {
                $varExp = explode('\\', $dependency[0]);
                $var = sprintf('$%s', strtolower(array_pop($varExp)));
                $dependencies .= sprintf('\%s %s, ', $dependency[0], $var);
            }
        }

        if (isset($polls[$serviceName]['expand'])) {
            foreach ($polls[$serviceName]['expand'] as $expand) {
                $expands .= sprintf(
                    '
    /**
     * Expander %1$s
     */
    public function %1$s()
    {
    
        // TODO:
    
    }
                    ', $expand);
            }

            $expands = substr($expands, 0, -1);
        }

        array_push($replaces, $dependencies, $handlerName, sprintf('$%s', strtolower($handlerName)), $expands, $polls[$serviceName]['launch_method']);

        return $replaces;

    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws IncorrectPathException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $style = new SymfonyStyle($input, $output);

        $serviceName = sprintf('%s', lcfirst($input->getArgument('name')));
        $serviceNamespace = str_replace('.', '\\', $input->getArgument('namespace'));
        $checkService = $this->checkServiceExist($serviceName, $style);
        $checkNamespace = $this->checkServiceNamespace($serviceNamespace, $style);
        $stubService = File::read('system/Kernel/Console/Stubs/ServiceStub.stub');

        if ($checkService && $checkNamespace) {
            $polls = $this->polls($style, $output, $serviceName, $serviceNamespace);

            $replaced = $this->replaced();
            $replaceBy = $this->replaceBy($serviceName, $serviceNamespace, $polls);

            $stubService = str_replace($replaced, $replaceBy, $stubService);
            $fileName = ucfirst(sprintf('%sProvider', $serviceName));
            $polls[$serviceName]['namespace'] = sprintf('Kernel\\Service\\Providers\\%s', $fileName);

            unset($polls[$serviceName]['launch_method']);

            $path = sprintf('system/Kernel/Service/Providers/%s.php', $fileName);
            File::editor()->put($path, $stubService);

            Yaml::open('configs/providers.yaml')
                ->change(function ($data) use ($polls) {
                    $data['services'] += $polls;

                    return $data;
                });

            $style->success([
                'Сервис успешно создан',
                sprintf('namespace: %s', $polls[$serviceName]['namespace']),
                sprintf('path: %s', $path)
            ]);
        }

        return Command::SUCCESS;

    }

}