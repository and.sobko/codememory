<?php

namespace Kernel\Console;

use File;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use System\FileSystem\Different\Exceptions\IncorrectPathException;
use Yaml;

/**
 * Class UpdateFacadeAliasesCommand
 * @package Kernel\Console
 *
 * @author  Codememory
 */
class UpdateFacadeAliasesCommand extends Command
{

    protected function configure(): void
    {

        $this
            ->setName('facade:updateAliases')
            ->setDescription('Обновление псевдонимов для фасадов');

    }

    /**
     * @param array $oldMatches
     * @param       $classmapCode
     *
     * @return array
     */
    private function findRemoved(array $oldMatches, $classmapCode): array
    {

        $removedMatch = [];
        $removed = [];

        foreach ($oldMatches[0] as $removingAlias) {
            if (!preg_match(sprintf('/%s/', preg_quote($removingAlias) . '\\n'), $classmapCode, $matchRemoving)) {
                $removedMatch[] = $removingAlias;
            }
        }

        if ($removedMatch !== []) {
            foreach ($removedMatch as $alias) {
                preg_match('/class_alias\((?<namespace>.*),(\s+)(?<alias_name>.*)?\);/', $alias, $match);

                $removed[] = sprintf('- %s => %s', substr($match['namespace'], 1, -1), substr($match['alias_name'], 1, -1));
            }
        }

        return $removed;

    }

    /**
     * @param array $facades
     * @param array $oldMatches
     * @param       $classmapCode
     *
     * @return array
     */
    private function findAdding(array $facades, array $oldMatches, &$classmapCode): array
    {

        $added = [];

        foreach ($facades as $facadeName => $facadeInfo) {
            $classmapCode .= sprintf("class_alias('%s', '%s');%s", $facadeInfo['namespace'], $facadeName, PHP_EOL);

        }

        $newLine = str_replace($oldMatches[0], '', $classmapCode);
        preg_match_all('/class_alias\((?<namespace>.*),(\s+)(?<alias_name>.*)?\);/', $newLine, $matches);

        foreach ($matches['namespace'] as $key => $namespace) {
            $added[] = sprintf('+ %s => %s', substr($namespace, 1, -1), substr($matches['alias_name'][$key], 1, -1));
        }

        return $added;

    }

    /**
     * @param array $facades
     * @param array $oldMatches
     * @param       $classmapCode
     *
     * @return array
     */
    private function findHandler(array $facades, array $oldMatches, &$classmapCode): array
    {

        /**
         * New alias fetch handler
         */
        $added = $this->findAdding($facades, $oldMatches, $classmapCode);
        array_unshift($added, 'Псевдонимы фасадов обновлены', sprintf('Добавлено: %s', count($added)));

        /**
         * Remote alias fetch handler
         */
        $removed = $this->findRemoved($oldMatches, $classmapCode);
        array_push($added, sprintf('Удалено: %s', count($removed)));

        return array_merge($added, $removed);

    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws IncorrectPathException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $pathClassmap = 'system/Kernel/Facades/Handler/classmap.php';
        $classmapCode = null;
        $facades = Yaml::open('configs/facades.yaml')->parse()['facades'];
        $style = new SymfonyStyle($input, $output);
        $oldClassmap = File::read($pathClassmap);
        preg_match_all('/class_alias\(.*\);/', $oldClassmap, $oldMatches);

        $response = $this->findHandler($facades, $oldMatches, $classmapCode);

        /**
         * Updating the file with aliases
         */
        $classmap = File::editor()->put($pathClassmap, sprintf('<?php%1$s%1$s%2$s', PHP_EOL, $classmapCode));

        if ($classmap) {
            $style->success($response);
        }

        return Command::SUCCESS;

    }

}