<?php

namespace Kernel\Exceptions;

use Kernel\Configuration\ConfigException;

/**
 * Class PHPVersionIsIncorrectException
 * @package Kernel\Exceptions
 *
 * @author  Codememory
 */
class PHPVersionIsIncorrectException extends ConfigException
{

    /**
     * PHPVersionIsIncorrectException constructor.
     */
    public function __construct()
    {

        parent::__construct('To work with the framework, PHP version must be ≥ 7.4');

    }

    /**
     * @return array
     */
    public function getVariables(): array
    {
        return [];
    }

}