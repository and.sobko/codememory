<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;

/**
 * @method static open(string $config): Config
 * @method static package(): Config
 * @method static get(?string $keys = null): array|string
 *
 * Class Config
 * @package Kernel\Facades
 */
final class Config extends Facade
{

    public const TYPE_DEFAULT_CACHE_CONFIG = 'conf';
    public const TYPE_PACKAGE_CACHE_CONFIG = 'packages';

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'config';        

    }

}
