<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;

/**
 * @method static create(string $name, string|int|float|null $value = null, ?string $domain = null, ?string $path = null, int $expires = 0, bool $httpOnly = false, bool $secure = false, ?string $sameSite = \System\Http\Client\Cookie::SAME_SITE_LAX): Cookie): Cookie
 * @method static all(): array
 * @method static missing(string $name): bool
 * @method static whenMissing(string $name, callable $callback): Cookie
 * @method static get(string $name): null|int|string
 * @method static remove(string $name): bool
 * @method static send(): Cookie
 * @method static getName(): ?string
 * @method static setName(?string $name): Cookie
 * @method static getValue(): float|int|string|null
 * @method static setValue(float|int|string|null $value): Cookie
 * @method static getDomain(): ?string
 * @method static setDomain(?string $domain): Cookie
 * @method static getPath(): ?string
 * @method static setPath(?string $path): Cookie
 * @method static getExpires(): int
 * @method static setExpires(int $expires): Cookie
 * @method static isHttpOnly(): bool
 * @method static setHttpOnly(bool $httpOnly): Cookie
 * @method static isSecure(): bool
 * @method static setSecure(bool $secure): Cookie
 * @method static getSameSite(): ?string
 * @method static setSameSite(?string $sameSite): Cookie
 *
 * Class Cookie
 * @package Kernel\Facades
 */
final class Cookie extends Facade
{

    public const SAME_SITE_LAX = 'Lax';
    public const SAME_SITE_STRICT = 'Strict';
    public const SAME_SITE_NONE = 'None';

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'cookie';        

    }

}
