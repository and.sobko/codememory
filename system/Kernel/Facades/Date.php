<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;

/**
 * @method static setDate(?string $date = null, int $day = 1, int $month = 0, int $year = 0): DateTime
 * @method static setTime(int $hour = 0, int $minute = 0, int $second = 0, bool $timestamp = false): DateTime
 * @method static modify(string $modify): DateTime
 * @method static addYears(int $number): DateTime
 * @method static addMonths(int $number): DateTime
 * @method static addWeeks(int $number): DateTime
 * @method static addDays(int $number): DateTime
 * @method static addHours(int $number): DateTime
 * @method static addMinutes(int $number): DateTime
 * @method static addSeconds(int $number): DateTime
 * @method static now(): int|string
 * @method static sub(): DateTime
 * @method static splitByFormat(string $format, string $date): DateTime
 * @method static diff(string $compare, string $target): DateTime
 * @method static format(string $format): string
 *
 * Class Date
 * @package Kernel\Facades
 */
final class Date extends Facade
{

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'date';

    }

}
