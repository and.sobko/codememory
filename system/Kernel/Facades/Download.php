<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\Http\Client\Response\Download as ResponseDownload;

/**
 * @method static accept(string $file): object
 * @method static rename(string $newName): object
 * @method static cacheControl(string $value): object
 * @method static make(): bool
 *
 * Class Download
 * @package Kernel\Facades
 *
 * @author Codememory
 */
final class Download extends Facade
{

    /**
     * @return ResponseDownload
     */
    public static function init(): ResponseDownload
    {

        return new ResponseDownload();

    }

}
