<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;

/**
 * @method static set(string $group, string $varName, bool|null|string $value): Environment
 * @method static get(string $group, string $varName, bool|null|string $default = null): bool|string|null
 * @method static getGroups(): array
 * @method static getAll(string $type = self::TYPE_ARRAY): array|string|null
 *
 * Class Env
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class Env extends Facade
{

    public const TYPE_ARRAY = 'array';
    public const TYPE_STRING = 'string';
    public const TYPE_JSON = 'json';

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'env';

    }

}
