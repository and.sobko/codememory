<?php

namespace Kernel\Facades\Exceptions;

use ErrorException;

/**
 * Class InitException
 * @package Kernel\Facades\Exceptions
 *
 * @author  Codememory
 */
class InitException extends ErrorException
{

    /**
     * InitException constructor.
     *
     * @param string $message
     */
    public function __construct(string $message)
    {

        parent::__construct($message);

    }

}