<?php

namespace Kernel\Facades;

use JetBrains\PhpStorm\Pure;
use Kernel\Facades\Handler\Facade;
use System\FileSystem\Different\Editor;
use System\FileSystem\Different\Find;
use System\FileSystem\Different\Information;
use System\FileSystem\Different\Is;

/**
 * @method static exists(string $path): bool
 * @method static setRealPath(string $path): File
 * @method static getRealPath(?string $join = null): string
 * @method static scanning($path, ?array $ignoring = []): array
 * @method static mkdir($dirname, ?int $permission = 0777, bool $recursion = false): void
 * @method static setPermission(string $path, int $permission = 0777, bool $recursion = false): bool
 * @method static setOwner(string $path, string $ownerName, bool $recursion = false): bool
 * @method static rename(string $path, string $newName): bool
 * @method static remove($path, bool $recursion = false, bool $removeCurrentDir = false): bool
 * @method static move(string $path, string $moveTo): bool
 * @method static setGroup(string $path, $group): bool
 * @method static read(string $path): string
 * @method static getImport($path): mixed
 * @method static oneImport(string $path): void
 *
 * Class File
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class File extends Facade
{

    /**
     * @return Is
     */
    #[Pure] public static function is(): Is
    {

        return self::getService('file_is');

    }

    /**
     * @return Editor
     */
    #[Pure] public static function editor(): Editor
    {

        return self::getService('file_editor');

    }

    /**
     * @return Find
     */
    #[Pure] public static function find(): Find
    {

        return self::getService('file_find');

    }

    /**
     * @return Information
     */
    #[Pure] public static function info(): Information
    {

        return self::getService('file_info');

    }

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'file';

    }

}