<?php

class_alias('Kernel\Facades\Yaml', 'Yaml');
class_alias('Kernel\Facades\File', 'File');
class_alias('Kernel\Facades\Header', 'Header');
class_alias('Kernel\Facades\Response', 'Response');
class_alias('Kernel\Facades\Url', 'Url');
class_alias('Kernel\Facades\Download', 'Download');
class_alias('Kernel\Facades\Env', 'Env');
class_alias('Kernel\Facades\Request', 'Request');
class_alias('Kernel\Facades\Config', 'Config');
class_alias('Kernel\Facades\Json', 'Json');
class_alias('Kernel\Facades\Cookie', 'Cookie');
class_alias('Kernel\Facades\Date', 'Date');
class_alias('Kernel\Facades\Time', 'Time');
