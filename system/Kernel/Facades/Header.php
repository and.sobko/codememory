<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;

/**
 * @method static getParser(): Parser
 * @method static setProtocolVersion(int|float $version): Header
 * @method static replaceHeaders(bool $replace): Header
 * @method static set(array $headers, bool $replace = true): Header
 * @method static setContentType(string $type): Header
 * @method static setCharset(string $charset): Header
 * @method static setResponseCode(int $code): Header
 * @method static addEnum(array ...$data): ?string
 * @method static getHttpStatus(): int
 * @method static getHeader(string ...$headers): string|array|null
 * @method static getAll(): array
 * @method static hasHeader(string $key): bool
 * @method static removeHeaders(string ...$headers): bool
 * @method static send(): Header
 *
 * Class Header
 * @package Kernel\Facades
 *
 * @author Codememory
 */
class Header extends Facade
{

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'header';

    }

}