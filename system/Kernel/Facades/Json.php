<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\Support\JsonParser;

/**
 * @method static setData(mixed $data): JsonParser
 * @method static removeRecursions(mixed $replacement = null): JsonParser
 * @method static removeNanOrInf(): JsonParser
 * @method static decodeAmp(): DecoderTrait
 * @method static decodeQuotes(): DecoderTrait
 * @method static decodeTag(): DecoderTrait
 * @method static slashAdaptation(): DecoderTrait
 * @method static ofJson(int $inType = JsonParser::JSON_TO_ARRAY): JsonParser
 * @method static encode(int $flags = 0): mixed
 * @method static decode(int $flags = 0): mixed
 *
 * Class Json
 * @package Kernel\Facades
 */
final class Json extends Facade
{

    public const JSON_TO_ARRAY = 4;
    public const JSON_TO_OBJECT = 6;
    public const JSON_TO_SERIALIZE = 9;

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'json';

    }

}
