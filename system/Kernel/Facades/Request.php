<?php

namespace Kernel\Facades;

use JetBrains\PhpStorm\Pure;
use Kernel\Facades\Handler\Facade;
use PHPHtmlParser\Dom;
use System\Http\Request\Parser;
use System\Http\Request\Upload\Uploader;

/**
 * @method static getBody(...$args): array|string
 * @method static post(?string $key = null, mixed $default = null): mixed
 * @method static query(?string $key = null, mixed $default = null): mixed
 * @method static all(string|null|array $key = null, mixed $default = null): mixed
 * @method static file(string $input): Files
 * @method static inBoolean(string $key): bool
 * @method static isFilled(string $key): bool
 * @method static anyFilled(array $keys): bool
 * @method static anyNotFilled(array $keys): bool
 * @method static whenFiled(string $key, callable $callback): mixed
 * @method static missing(string $key): bool
 * @method static anyMissing(array $keys): bool
 * @method static whenMissing(string $key, callable $callback): mixed
 * @method static path(): string
 * @method static isPathString(string $path): bool
 * @method static isPath(string|array $path): bool
 * @method static method(): string
 * @method static isMethod(string $method): bool
 *
 * Class Request
 * @package Kernel\Facades
 */
final class Request extends Facade
{

    /**
     * @return Dom
     */
    #[Pure] public static function dom(): Dom
    {

        return self::getService('request')->dom;

    }

    /**
     * @return Uploader
     */
    #[Pure] public static function upload(): Uploader
    {

        return self::getService('request')->upload;

    }

    /**
     * @return Parser
     */
    #[Pure] public static function parser(): Parser
    {

        return self::getService('request')->parser;

    }

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'request';

    }

}
