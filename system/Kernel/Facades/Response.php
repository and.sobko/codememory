<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;

/**
 * @method static create(?string $content = null, int $statusCode = 200, array $headers = []): Response
 * @method static setContent(string|int|null $content): Response
 * @method static getContent(): string|int|null
 * @method static sendContent(): Response
 * @method static responseStatus(int $code): Response
 * @method static getResponseStatus(): int
 * @method static sendHeaders(): Response
 * @method static concateSendHeaders(): Response
 * @method static json(array|string|int|object $data, int $status = 200, array $headers = []): string
 * @method static setContentType(string $type): Response
 * @method static setCharset(string $charset): Response
 * @method static isContinue(): bool
 * @method static isOk(): bool
 * @method static isRedirect(): bool
 * @method static isForbidden(): bool
 * @method static isNotFound(): bool
 * @method static isInformational(): bool
 * @method static isSuccess(): bool
 * @method static isRedirection(): bool
 * @method static isClientError(): bool
 * @method static isServerError(): bool
 *
 * Class Response
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
class Response extends Facade
{

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'response';

    }

}