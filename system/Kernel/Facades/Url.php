<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;

/**
 * @method static current(): ?string
 * @method static getMethod(?string $case = 'up'): string
 * @method static getQuery(?string $key = null): array
 * @method static getPrevUrl(): ?string
 * @method static mergeUrl(string $url, array $params = [], bool $saveCurrentQuery = true): ?string
 *
 * Class Url
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
class Url extends Facade
{

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'url';

    }

}