<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;

/**
 * @method static open(string $file): object
 * @method static parse(): array
 * @method static fromString(bool $status): object
 * @method static addStringYaml(string $data): object
 * @method static setFlags(int $flags): object
 * @method static put(array $data, int $inline = 5, int $indent = 2): bool
 * @method static change(callable $callback, int $inline = 5, int $indent = 2): bool
 *
 * Class Yaml
 * @package Kernel\Facades
 *
 * @author Codememory
 */
final class Yaml extends Facade
{

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'yaml';

    }

}