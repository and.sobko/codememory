<?php

namespace System\Kernel;

use Kernel\Configuration\KernelConfig;
use Kernel\Facades\Handler\Facade;
use Kernel\Service\Exceptions\ExpanderNotImplementedException;
use Kernel\Service\Exceptions\ServiceNotCreatedException;
use Kernel\Service\ServiceProvider;
use Kernel\Traits\RouterCallTrait;

/**
 * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
 * & The main framework class that runs the entire chain
 * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
 *
 * Class KernelLaunch
 * @package System\CodememoryKernel
 *
 * @author  Codememory
 */
class KernelLaunch
{

    use RouterCallTrait;

    /**
     * @var ServiceProvider
     */
    public ServiceProvider $service;

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method collects all route files and connects. This was done to make it easier
     * & for developers to create their Routes
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     */
    private function connectingRoutes(): static
    {

        return $this->scanningFolderRoutes();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Creation of a service provider object for further work with models and Controllers.
     * & This object is required to call the facade
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     * @throws ServiceNotCreatedException
     * @throws \ReflectionException|ExpanderNotImplementedException
     */
    private function serviceInitialization(): static
    {

        $service = new ServiceProvider();
        $service->collect();

        $this->service = $service;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Instantiation of an object of the main class for working with facades. Facades are referring
     * & to any classes and their methods as a statically implementing service provider.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     */
    private function facadeInitialization(): static
    {

        Facade::setProviders($this->service);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method. This method collects all components and launches the framework.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws ServiceNotCreatedException
     * @throws \ReflectionException|ExpanderNotImplementedException
     */
    public function assembly(): void
    {

        $config = new KernelConfig();
        $config->exec();

        $this->serviceInitialization()
            ->facadeInitialization()
            ->connectingRoutes();

    }

}