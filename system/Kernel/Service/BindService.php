<?php

namespace Kernel\Service;

use ReflectionFunction;

/**
 * Class BindService
 * @package System\Kernel\Service
 *
 * @author  Codememory
 */
class BindService
{

    private const BINDS_METHODS = [
        [
            'regex' => '\%\{(?<func>[a-zA-Z0-9_]+)*(\.(?<name>[a-zA-Z0-9_]+)\=\"(?<value>[^\"]+)\")*\}\%',
            'method' => 'bindFunction'
        ]
    ];

    /**
     * @var array
     */
    private array $binds = [];

    /**
     * @var array
     */
    private array $unpacked = [];

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add bind (argument) to service constructor
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     * @param string $bind
     *
     * @return $this
     */
    public function setBind(string $name, string $bind): object
    {

        $this->binds[$name] = $bind;

        return $this;

    }

    /**
     * @param array  $match
     * @param string $bind
     *
     * @return array
     * @throws \ReflectionException
     */
    private function bindFunction(array $match, string $bind): array
    {

        $reflection = new ReflectionFunction($match['func'][0]);
        $funcParameters = $reflection->getParameters();
        $parameters = [];
        $assembledParameters = [];

        if ($match['name'] !== []) {
            foreach ($match['name'] as $index => $name) {
                $parameters[$name] = $match['value'][$index] ?? null;
            }
        }

        foreach ($funcParameters as $key => $parameter) {
            if (array_key_exists($parameter->getName(), $parameters)) {
                $assembledParameters[] = $parameters[$parameter->getName()];
            }
        }

        return $assembledParameters;

    }

    /**
     * @param string $bind
     * @param string $name
     */
    private function determinant(string $bind, string $name): void
    {

        foreach (self::BINDS_METHODS as $key => $binds) {
            if (preg_match_all(sprintf('/^%s$/', $binds['regex']), $bind, $match)) {
                foreach ($match as $index => $value) {
                    if (is_integer($index)) {
                        unset($match[$index]);
                    }
                }
                $method = self::BINDS_METHODS[$key]['method'];

                $this->unpacked[$name] = $this->$method($match, $bind);
            }
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A method that will unpack all binds and give ready information
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     */
    public function unpacking(): object
    {

        foreach ($this->binds as $name => $bind) {
            $this->determinant($bind, $name);
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns a list of all binds
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return mixed
     */
    public function getBind(string $name)
    {

        return $this->unpacked[$name];

    }

}