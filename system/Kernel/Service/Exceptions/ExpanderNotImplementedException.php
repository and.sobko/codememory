<?php

namespace Kernel\Service\Exceptions;

use ErrorException;

/**
 * Class ExpanderNotImplementedException
 * @package Kernel\Service\Exceptions
 *
 * @author  Codememory
 */
class ExpanderNotImplementedException extends ErrorException
{

    /**
     * ExpanderNotImplementedException constructor.
     *
     * @param string $serviceName
     * @param string $methods
     */
    public function __construct(string $serviceName, string $methods)
    {
        parent::__construct(
            sprintf('Expander [<b>%2$s</b>] for the <b>%1$s</b> provider is not implemented. To do this, add the [<b>%2$s</b>] method to the <b>%1$s</b> provider', $methods, $serviceName)
        );
    }

}