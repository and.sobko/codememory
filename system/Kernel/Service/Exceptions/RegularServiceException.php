<?php

namespace Kernel\Service\Exceptions;

use ErrorException;

/**
 * Class RegularServiceException
 * @package System\Kernel\Service\Exceptions
 *
 * @author  Codememory
 */
class RegularServiceException extends ErrorException
{

    /**
     * @var string|null
     */
    private ?string $serviceName;

    /**
     * ServiceNotImplemented constructor.
     *
     * @param string $serviceName
     */
    public function __construct(string $serviceName)
    {

        $this->serviceName = $serviceName;

        parent::__construct(
            sprintf(
                'The <b>%s</b> service cannot be called through the $ this context because it is a regular service. Specify "as: service" in the service configuration',
                $serviceName
            )
        );

    }

    /**
     * @return string|null
     */
    public function getServiceName(): ?string
    {

        return $this->serviceName;

    }

}