<?php

namespace Kernel\Service\Exceptions;

use ErrorException;

/**
 * Class ServiceNotCreatedException
 * @package System\Kernel\Service\Exceptions
 *
 * @author  Codememory
 */
class ServiceNotCreatedException extends ErrorException
{

    /**
     * @var string|null
     */
    private ?string $serviceName;

    /**
     * @var string|null
     */
    private ?string $serviceNamespace;

    /**
     * ServiceNotCreatedException constructor.
     *
     * @param string $serviceName
     * @param string $namespace
     */
    public function __construct(string $serviceName, string $namespace)
    {

        $this->serviceName = $serviceName;
        $this->serviceNamespace = $namespace;

        parent::__construct(
            sprintf(
                'The <b>%s</b> service cannot find its <b>%s</b> class. Perhaps the namespace is incorrect or the provider has not been created',
                $serviceName,
                $namespace
            )
        );

    }

    /**
     * @return string|null
     */
    public function getServiceName(): ?string
    {

        return $this->serviceName;

    }

    /**
     * @return string|null
     */
    public function getNamespace(): ?string
    {

        return $this->serviceNamespace;

    }

}