<?php

namespace Kernel\Service\Providers;

use System\FileSystem\Config\Configuration\Config;

/**
 * @method open(string $config): Config
 * @method package(): Config
 * @method get(?string $keys = null): array|string
 *
 * Class ConfigProvider
 * @package Kernel\Service\Providers
 *
 * @author  Codememory
 */
class ConfigProvider
{

    public function __construct()
    {
    }

    /**
     * @param Config $config
     *
     * @return Config
     */
    public function executor(Config $config): Config
    {

        return $config;

    }

}
