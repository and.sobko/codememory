<?php

namespace Kernel\Service\Providers;

use System\Http\Client\Cookie;

/**
 * @method create(string $name, string|int|float|null $value = null, ?string $domain = null, ?string $path = null, int $expires = 0, bool $httpOnly = false, bool $secure = false, ?string $sameSite = Cookie::SAME_SITE_LAX): Cookie): Cookie
 * @method all(): array
 * @method missing(string $name): bool
 * @method whenMissing(string $name, callable $callback): Cookie
 * @method get(string $name): null|int|string
 * @method remove(string $name): bool
 * @method send(): Cookie
 * @method getName(): ?string
 * @method setName(?string $name): Cookie
 * @method getValue(): float|int|string|null
 * @method setValue(float|int|string|null $value): Cookie
 * @method getDomain(): ?string
 * @method setDomain(?string $domain): Cookie
 * @method getPath(): ?string
 * @method setPath(?string $path): Cookie
 * @method getExpires(): int
 * @method setExpires(int $expires): Cookie
 * @method isHttpOnly(): bool
 * @method setHttpOnly(bool $httpOnly): Cookie
 * @method isSecure(): bool
 * @method setSecure(bool $secure): Cookie
 * @method getSameSite(): ?string
 * @method setSameSite(?string $sameSite): Cookie
 *
 * Class CookieProvider
 * @package Kernel\Service\Providers
 *
 * @author  Codememory
 */
class CookieProvider
{

    public function __construct()
    {
    }

    /**
     * @param Cookie $cookie
     *
     * @return Cookie
     */
    public function executor(Cookie $cookie): Cookie
    {

        return $cookie;

    }

}
