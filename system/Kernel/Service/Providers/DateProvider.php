<?php

namespace Kernel\Service\Providers;

use System\Support\DateTime\DateTime;

/**
 * @method setDate(?string $date = null, int $day = 1, int $month = 0, int $year = 0): DateTime
 * @method setTime(int $hour = 0, int $minute = 0, int $second = 0, bool $timestamp = false): DateTime
 * @method modify(string $modify): DateTime
 * @method addYears(int $number): DateTime
 * @method addMonths(int $number): DateTime
 * @method addWeeks(int $number): DateTime
 * @method addDays(int $number): DateTime
 * @method addHours(int $number): DateTime
 * @method addMinutes(int $number): DateTime
 * @method addSeconds(int $number): DateTime
 * @method now(): int|string
 * @method sub(): DateTime
 * @method splitByFormat(string $format, string $date): DateTime
 * @method diff(string $compare, string $target): DateTime
 * @method format(string $format): string
 *
 * Class DateProvider
 * @package Kernel\Service\Providers
 *
 * @author  Codememory
 */
class DateProvider
{

    public function __construct()
    {
    }

    /**
     * @param DateTime $datetime
     *
     * @return DateTime
     */
    public function executor(DateTime $datetime): DateTime
    {

        return $datetime;

    }

}
