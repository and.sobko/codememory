<?php

namespace Kernel\Service\Providers;

use System\FileSystem\Config\Env\Environment;

/**
 * @method set(string $group, string $varName, bool|null|string $value): Environment
 * @method get(string $group, string $varName, bool|null|string $default = null): bool|string|null
 * @method getGroups(): array
 * @method getAll(string $type = Environment::TYPE_ARRAY): array|string|null
 *
 * Class EnvProvider
 * @package Kernel\Service\Providers
 *
 * @author  Codememory
 */
class EnvProvider
{

    public function __construct()
    {
    }

    
    public function executor(Environment $environment): Environment
    {

        return $environment;

    }

}
