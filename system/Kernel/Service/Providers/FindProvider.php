<?php

namespace Kernel\Service\Providers;

use System\FileSystem\Different\Find;

/**
 * @method setIgnore(string ...$names): object
 * @method find(array $names, ?string $specificWay = null): array
 * @method findByRegex(string $regex, ?string $specificWay = null): array
 *
 * Class findProvider
 * @package Kernel\Service\Providers
 *
 * @author  Codememory
 */
class FindProvider
{

    public function __construct()
    {
    }

    /**
     * @param Find $find
     *
     * @return Find
     */
    public function executor(Find $find): Find
    {

        return $find;

    }

}
