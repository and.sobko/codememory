<?php

namespace Kernel\Service\Providers;

use JetBrains\PhpStorm\Pure;
use System\Http\Client\Headers\Header;
use System\Http\Client\Headers\Parser;

/**
 * @property Parser $parser
 *
 * @method getParser(): Parser
 * @method setProtocolVersion(int|float $version): Header
 * @method replaceHeaders(bool $replace): Header
 * @method set(array $headers, bool $replace = true): Header
 * @method setContentType(string $type): Header
 * @method setCharset(string $charset): Header
 * @method setResponseCode(int $code): Header
 * @method addEnum(array ...$data): ?string
 * @method getHttpStatus(): int
 * @method getHeader(string ...$headers): string|array|null
 * @method getAll(): array
 * @method hasHeader(string $key): bool
 * @method removeHeaders(string ...$headers): bool
 * @method send(): Header
 *
 * Class HeaderProvider
 * @package Kernel\Service\Providers
 *
 * @author  Codememory
 */
class HeaderProvider
{

    public function __construct()
    {
    }

    /**
     * @return Header
     */
    #[Pure] public function executor(): Header
    {

        return new Header();

    }

}