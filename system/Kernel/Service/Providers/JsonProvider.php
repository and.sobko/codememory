<?php

namespace Kernel\Service\Providers;

use System\Support\JsonParser;

/**
 * @method setData(mixed $data): JsonParser
 * @method removeRecursions(mixed $replacement = null): JsonParser
 * @method removeNanOrInf(): JsonParser
 * @method decodeAmp(): DecoderTrait
 * @method decodeQuotes(): DecoderTrait
 * @method decodeTag(): DecoderTrait
 * @method slashAdaptation(): DecoderTrait
 * @method ofJson(int $inType = JsonParser::JSON_TO_ARRAY): JsonParser
 * @method encode(int $flags = 0): mixed
 * @method decode(int $flags = 0): mixed
 *
 * Class JsonProvider
 * @package Kernel\Service\Providers
 *
 * @author  Codememory
 */
class JsonProvider
{

    public function __construct()
    {
    }

    /**
     * @param JsonParser $jsonParser
     *
     * @return JsonParser
     */
    public function executor(JsonParser $jsonParser): JsonParser
    {

        return $jsonParser;

    }

}
