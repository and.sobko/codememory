<?php

namespace Kernel\Service\Providers;

use System\Http\Request\Request;

/**
 * @method getBody(...$args): array|string
 * @method post(?string $key = null, mixed $default = null): mixed
 * @method query(?string $key = null, mixed $default = null): mixed
 * @method all(string|null|array $key = null, mixed $default = null): mixed
 * @method file(string $input): Files
 * @method inBoolean(string $key): bool
 * @method isFilled(string $key): bool
 * @method anyFilled(array $keys): bool
 * @method anyNotFilled(array $keys): bool
 * @method whenFiled(string $key, callable $callback): mixed
 * @method missing(string $key): bool
 * @method anyMissing(array $keys): bool
 * @method whenMissing(string $key, callable $callback): mixed
 * @method path(): string
 * @method isPathString(string $path): bool
 * @method isPath(string|array $path): bool
 * @method method(): string
 * @method isMethod(string $method): bool
 *
 * Class requestProvider
 * @package Kernel\Service\Providers
 *
 * @author  Codememory
 */
class RequestProvider
{

    public function __construct()
    {
    }

    /**
     * @param Request $request
     *
     * @return Request
     */
    public function executor(Request $request): Request
    {

        return $request;

    }

}
