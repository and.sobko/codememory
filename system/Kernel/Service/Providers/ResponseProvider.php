<?php

namespace Kernel\Service\Providers;

use System\Http\Client\Headers\Header;
use System\Http\Client\Response\Download;
use System\Http\Client\Response\Response;
use System\Support\JsonParser;

/**
 * @property Download $download
 * @property Header $header
 * @property JsonParser $json
 *
 * @method create(?string $content = null, int $statusCode = 200, array $headers = []): Response
 * @method setContent(string|int|null $content): Response
 * @method getContent(): string|int|null
 * @method sendContent(): Response
 * @method responseStatus(int $code): Response
 * @method getResponseStatus(): int
 * @method sendHeaders(): Response
 * @method concateSendHeaders(): Response
 * @method json(array|string|int|object $data, int $status = 200, array $headers = []): string
 * @method setContentType(string $type): Response
 * @method setCharset(string $charset): Response
 * @method isContinue(): bool
 * @method isOk(): bool
 * @method isRedirect(): bool
 * @method isForbidden(): bool
 * @method isNotFound(): bool
 * @method isInformational(): bool
 * @method isSuccess(): bool
 * @method isRedirection(): bool
 * @method isClientError(): bool
 * @method isServerError(): bool
 *
 * Class ResponseProvider
 * @package Kernel\Service\Providers
 *
 * @author  Codememory
 */
class ResponseProvider
{

    public function __construct()
    {
    }

    /**
     * @param Response $response
     *
     * @return Response
     */
    public function executor(Response $response): Response
    {

        return $response;

    }

}