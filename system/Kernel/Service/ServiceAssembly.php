<?php

namespace Kernel\Service;

use Kernel\Service\Exceptions\ExpanderNotImplementedException;
use Kernel\Service\Exceptions\ServiceNotCreatedException;
use ReflectionClass;
use ReflectionException;
use System\Components\DI\DIElements\ObjectDependency;

/**
 * Class ServiceAssembly
 * @package System\Kernel\Service
 *
 * @author  Codememory
 * @method getServices()
 * @method getServiceNamespace(int|string $name)
 * @method getDependencies(int|string $name)
 * @method getServiceExecuted(int|string $name, string $string)
 * @method getServiceExtender(string $serviceName)
 */
class ServiceAssembly extends ServiceProcess
{

    /**
     * @return ObjectDependency
     */
    private function DI(): ObjectDependency
    {

        return new ObjectDependency();

    }

    /**
     * @param string $service
     * @param string $namespace
     *
     * @return bool
     * @throws ServiceNotCreatedException
     */
    private function serviceNotCreated(string $service, string $namespace): bool
    {

        if (!class_exists($namespace)) {
            throw new ServiceNotCreatedException($service, $namespace);
        }

        return true;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Build all dependencies in the configuration and create a container with a service provider
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $dependencies
     *
     * @return array
     * @throws ReflectionException
     */
    private function collectArguments(array $dependencies): array
    {

        $DIObjects = $this->DI();
        $dependenciesObjects = [];
        $namesDependenciesObjects = [];

        if ($dependencies !== []) {
            foreach ($dependencies as $dependency) {
                $withConstruct = $dependency[1] ?? true;
                $namespace = $dependency[0];
                $nameDI = sprintf('service-object-%s', hash('sha256', $namespace));
                $namesDependenciesObjects[] = $nameDI;
                $DIObjects
                    ->setName($nameDI)
                    ->setObject($namespace)
                    ->withConstructor($withConstruct)
                    ->collect();
            }

            $DIObjects = $DIObjects->execute();

            foreach ($namesDependenciesObjects as $name) {
                $dependenciesObjects[] = $DIObjects->get($name);
            }
        }

        return $dependenciesObjects;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Build service providers extenders. Adding to Dependency Injection Container and
     * & checking the extension implementation
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string           $service
     * @param array            $expands
     * @param ObjectDependency $di
     *
     * @return ObjectDependency
     * @throws ExpanderNotImplementedException
     * @throws ReflectionException
     */
    private function collectProviderExtenders(string $service, array $expands, ObjectDependency $di): ObjectDependency
    {

        foreach ($expands as $expanderName) {
            $namespace = $this->getServiceNamespace($service);
            $reflection = new ReflectionClass($namespace);

            if ($reflection->hasMethod($expanderName) === false) {
                throw new ExpanderNotImplementedException($expanderName, $namespace);
            }

            $di = $di->setName(sprintf('service-extender-%s-%s', $service, $expanderName))
                ->setObject($namespace)
                ->setMethod($expanderName)
                ->collect();

        }

        return $di;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Initializing Service Provider Extenders
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param ObjectDependency $di
     * @param string           $executed
     * @param string           $serviceName
     *
     * @throws ReflectionException
     */
    private function expanderInitialization(ObjectDependency $di, string $executed, string $serviceName): void
    {

        if ($this->getServiceExtender($serviceName) !== []) {
            foreach ($this->getServiceExtender($serviceName) as $extender) {
                $extenderName = sprintf('%s_%s', $serviceName, $extender);

                $this->assembledServices[$executed][$extenderName] = $di->get(sprintf('service-extender-%s-%s', $serviceName, $extender));
            }
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Adding providers and their extenders to the Dependency Injection Container so that
     * & dependencies can be automatically transferred to the service provider
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param ObjectDependency $di
     * @param string           $serviceName
     * @param string           $namespace
     *
     * @return ObjectDependency
     * @throws ExpanderNotImplementedException
     * @throws ReflectionException
     */
    private function addProviderToContainer(ObjectDependency $di, string $serviceName, string $namespace): ObjectDependency
    {

        $container = $di->setName(sprintf('service-%s', $serviceName))
            ->setObject($namespace)
            ->setMethod($this->getServiceExecuted($serviceName, 'method'))
            ->collect();
        $extender = $this->getServiceExtender($serviceName);

        if ($extender !== []) {
            $container = $this->collectProviderExtenders($serviceName, $extender, $container);
        }

        return $container;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method creates ready-made services or their extenders and imports them to receive
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param ObjectDependency $di
     *
     * @throws ReflectionException
     */
    private function addReadyProviders(ObjectDependency $di): void
    {

        foreach ($this->getServices() as $name => $service) {
            $executed = $this->getServiceExecuted($name, 'as');

            $this->assembledServices[$executed][$name] = $di->get(sprintf('service-%s', $name));
            $this->expanderInitialization($di, $executed, $name);
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Full service assembly method
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws ReflectionException
     * @throws ServiceNotCreatedException|ExpanderNotImplementedException
     */
    public function collect(): void
    {

        $DIContainer = $this->DI();

        foreach ($this->getServices() as $name => $service) {
            $namespace = $this->getServiceNamespace($name);
            $this->serviceNotCreated($name, $namespace);

            $DIContainer = $this->addProviderToContainer($DIContainer, $name, $namespace);
        }
        $executedDi = $DIContainer->execute();

        $this->addReadyProviders($executedDi);

    }

}