<?php

namespace Kernel\Service;

use JetBrains\PhpStorm\Pure;
use Kernel\Service\Exceptions\RegularServiceException;
use Kernel\Service\Exceptions\ServiceNotImplementedException;

/**
 * Class ServiceProcess
 * @package System\Kernel\Service
 *
 * @author  Codememory
 */
class ServiceProcess
{

    /**
     * @param string $serviceName
     * @param string $key
     *
     * @return bool
     * @throws ServiceNotImplementedException
     */
    private function serviceNotImplemented(string $serviceName, string $key = 'service'): bool
    {

        if ($this->getReady()[$key][$serviceName] === null) {
            throw new ServiceNotImplementedException($serviceName);
        }

        return true;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns a list of services that do not have access to the $ this context
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $serviceName
     *
     * @return bool
     * @throws RegularServiceException
     */
    private function notService(string $serviceName): bool
    {

        if (array_key_exists($serviceName, $this->getReady()['default'] ?? [])) {
            throw new RegularServiceException($serviceName);
        }

        return true;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the finished service by name
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $serviceName
     *
     * @return mixed
     * @throws RegularServiceException
     * @throws ServiceNotImplementedException
     */
    public function getService(string $serviceName): mixed
    {

        if ($this->serviceNotImplemented($serviceName) && $this->notService($serviceName)) {
            return $this->getReady()['service'][$serviceName];
        }

        return false;

    }

    /**
     * @param string $serviceName
     *
     * @return mixed
     */
    #[Pure] public function getServiceAll(string $serviceName): mixed
    {

        $allServices = [];

        foreach ($this->getReady() as $arrServices) {
            foreach ($arrServices as $name => $service) {
                $allServices[$name] = $service;
            }
        }

        return $allServices[$serviceName];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns a list of all ready-made services, or if you specify an
     * & argument, it will return a service by name
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $name
     *
     * @return mixed
     * @throws RegularServiceException
     * @throws ServiceNotImplementedException
     */
    public function getAllServices(?string $name = null): mixed
    {

        if (null !== $name) {
            return $this->getService($name);
        }

        return $this->getReady()['service'];

    }

}