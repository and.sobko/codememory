<?php

namespace Kernel\Service;

use Kernel\Service\Traits\ServiceInfoGetterTrait;
use System\FileSystem\Different\File;
use System\FileSystem\Different\Yaml;

/**
 * Class ServiceProvider
 * @package Kernel\Service
 *
 * @author  Codememory
 */
class ServiceProvider extends ServiceAssembly
{

    use ServiceInfoGetterTrait;

    const PATH_SERVICES = 'configs/providers.yaml';

    protected const DEFAULT = [
        'executed' => [
            'method' => 'executor',
            'as' => 'service'
        ]
    ];

    /**
     * @var array
     */
    protected array $assembledServices = [];

    /**
     * @var array
     */
    private array $services;

    /**
     * @var Yaml
     */
    private Yaml $yaml;

    /**
     * @var File
     */
    private File $file;

    /**
     * ServiceProvider constructor.
     */
    public function __construct()
    {

        $this->file = new File();
        $this->yaml = new Yaml($this->file);

        $this->services = $this->yaml->open(self::PATH_SERVICES)->parse();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Getting array lists ready services
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    protected function getReady(): array
    {

        return $this->assembledServices;

    }

}