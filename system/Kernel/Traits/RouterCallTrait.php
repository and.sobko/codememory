<?php

namespace Kernel\Traits;

use File;
use Kernel\Interfaces\KernelConstantsInterface;

/**
 * Trait RouterCallTrait
 * @package Kernel\Traits
 *
 * @author  Codememory
 */
trait RouterCallTrait
{

    /**
     * @return $this
     */
    public function scanningFolderRoutes(): static
    {

        $routeFiles = File::scanning(KernelConstantsInterface::PATH_TO_ROUTES);

        foreach ($routeFiles as $fileName) {
            if (preg_match(sprintf('/^%s$/i', KernelConstantsInterface::REGEX_ROUTE_FILES), $fileName)) {
                File::oneImport(
                    sprintf('%s%s', KernelConstantsInterface::PATH_TO_ROUTES, $fileName)
                );
            }
        }

        return new static();

    }

}