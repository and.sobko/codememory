<?php


use System\FileSystem\Different\Exceptions\IncorrectPathException;
use System\Routing\Router;

if (!function_exists('allClasses')) {
    /**
     * @return array
     * @throws IncorrectPathException
     */
    function allClasses(): array
    {

        $fileLists = File::find()->setIgnore('vendor')->findByRegex('.*\.php');
        $classes = [];

        if ($fileLists !== []) {
            foreach ($fileLists as $fileArr) {
                $content = File::read($fileArr['path']);

                if (preg_match('/namespace\s+(?<namespace>[a-z_0-9\\\]+)/i', $content, $matchNamespace)) {
                    if (preg_match('/class\s+(?<className>[a-z0-9_\\\]+)(\s+)?({)?/i', $content, $matchClass)) {
                        $className = sprintf('%s\%s', $matchNamespace['namespace'], $matchClass['className']);

                        if (class_exists($className)) $classes[] = $className;
                    }
                }
            }
        }

        return $classes;
    }
}

if (!function_exists('envi')) {
    /**
     * @param string|null $key
     *
     * @return string|Env|null
     */
    function envi(?string $key = null): string|null|Env
    {

        if(null !== $key) {
            [$group, $key] = explode('.', $key);

            return Env::get(strtoupper($group), strtoupper($key));
        }

        return new Env();

    }
}

if (!function_exists('route')) {
    /**
     * @param string $routeName
     * @param array  $params
     *
     * @return string|null
     */
    function route(string $routeName, array $params = []): ?string
    {

        $query = null;
        $route = Router::getRoute($routeName);
        preg_match_all('/@(?<params>[^\/]+)/', $route, $match);

        if(array_key_exists('params', $match) && ([] !== $match['params'])) {
            $routeParams = $match['params'];

            if([] !== $params) {
                foreach ($params as $name => $value) {
                    if(!in_array($name, $routeParams)) {
                        $query .= sprintf('%s=%s&', $name, $value);
                    }
                }
            }
        }

        if(null !== $query) {
            $query = sprintf('?%s', substr($query, 0, -1));
        }

        return Router::getRoute($routeName, $params).$query;

    }
}