<?php

namespace System\Routing\Action;

use JetBrains\PhpStorm\Pure;
use System\Components\DI\DIElements\ObjectDependency;
use System\Routing\Action\Exceptions\InvalidArgumentName;

/**
 * Class ActionProcessing
 * @package System\Routing\Action
 *
 * @author  Codememory
 */
class ActionProcessing extends AutoGeneration
{

    public function __construct(private ObjectDependency $di) {}

    /**
     * @param array $arguments
     *
     * @throws InvalidArgumentName
     */
    private function isValidateArguments(array $arguments): void
    {

        if ($arguments !== []) {
            foreach ($arguments as $name => $value) {
                if (!preg_match('/^[a-zA-Z_]+$/', $name)) {
                    throw new InvalidArgumentName($name);
                }
            }
        }

    }

    /**
     * @param array $data
     *
     * @return string
     */
    #[Pure] private function determinant(array $data): string
    {

        return
            array_key_exists('controller', $data)
            && array_key_exists('method', $data) ? 'controller' : 'view';

    }

    private function loadingView(string $path)
    {

        // TODO:

    }

    /**
     * @param string $controller
     * @param string $method
     * @param array  $arguments
     *
     * @return mixed
     * @throws \ReflectionException
     */
    private function loadingController(string $controller, string $method, array $arguments = []): mixed
    {

        $name = sprintf('controller-action-%s', md5($controller . $method));

        $this->di
            ->setName($name)
            ->setObject($controller)
            ->setMethod($method)
            ->addArguments($arguments)
            ->collect()
            ->execute();

        return $this->di->get($name);

    }

    /**
     * @param int $httpCode
     *
     * @return mixed
     * @throws InvalidArgumentName|\ReflectionException
     */
    public function make(int $httpCode): mixed
    {

        $this->isValidateArguments($this->getControllerOrView($httpCode)['arguments']);
        $data = $this->getControllerOrView($httpCode);
        $type = $this->determinant($data);

        if ($type === 'controller') {
            return $this->loadingController($data['controller'], $data['method'], $data['arguments']);
        }

        return null;

    }

}