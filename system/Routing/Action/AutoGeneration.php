<?php

namespace System\Routing\Action;

/**
 * Class AutoGeneration
 * @package System\Routing\Action
 *
 * @author  Codememory
 */
abstract class AutoGeneration extends RouteAction
{

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method automatically determines to send a template for display,
     * & or a controller with a method
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $httpCode
     *
     * @return array
     */
    protected function getControllerOrView(int $httpCode): array
    {

        $controller = $this->getController($httpCode);
        $method = $this->getMethod($httpCode);
        $view = $this->getViewWithArguments($httpCode);
        $arguments = $this->getArguments($httpCode);

        return $this->handler($controller, $method, $arguments, $view);

    }

    /**
     * @param bool|string $controller
     * @param bool|string $method
     * @param array       $arguments
     * @param array       $views
     *
     * @return array
     */
    private function handler(string|bool $controller, string|bool $method, array $arguments = [], array $views = []): array
    {

        if ($controller === false || $method === false) {
            $views['arguments'] += $arguments;

            return $views;
        } else {
            return [
                'controller' => $controller,
                'method'     => $method,
                'arguments'  => $arguments
            ];
        }

    }

}