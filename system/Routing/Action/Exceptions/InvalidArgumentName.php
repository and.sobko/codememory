<?php

namespace System\Routing\Action\Exceptions;

use JetBrains\PhpStorm\Pure;

/**
 * Class InvalidArgumentName
 * @package System\Routing\Action\Exceptions
 *
 * @author  Codememory
 */
class InvalidArgumentName extends \ErrorException
{

    /**
     * InvalidArgumentName constructor.
     *
     * @param string $name
     */
    #[Pure] public function __construct(string $name)
    {
        parent::__construct(
            sprintf(
                'The argument name %s passed to action Route does not match the regular expression /^[a-zA-Z _]$/',
                $name
            )
        );
    }

}