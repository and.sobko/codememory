<?php

namespace System\Routing\Action;

use Yaml;

/**
 * Class RouteAction
 * @package System\Routing
 *
 * @author  Codememory
 */
class RouteAction
{

    /**
     * @var array
     */
    private array $actions = [];

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns all actions for Router(Response) that are written in
     * & configs/actions.yaml
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    protected function getActions(): array
    {

        $actions = Yaml::open('configs/actions.yaml')->parse()['Router'] ?? [];
        $this->reassembly($actions);

        return $this->actions;

    }

    /**
     * @param array $actions
     */
    private function reassembly(array $actions): void
    {

        if ($actions !== []) {
            foreach ($actions as $name => $action) {
                $this->actions[$action['httpResponseCode']] = $actions[$name];
            }
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the namespace of the controller if specified
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $httpCode
     *
     * @return bool|string
     */
    protected function getController(int $httpCode): string|bool
    {

        return $this->getActions()[$httpCode]['controller'] ?? false;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method returns the name of the method to be called
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $httpCode
     *
     * @return bool|string
     */
    protected function getMethod(int $httpCode): string|bool
    {

        return $this->getActions()[$httpCode]['method'] ?? false;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns an array of arguments for the controller or action template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $httpCode
     *
     * @return array
     */
    protected function getArguments(int $httpCode): array
    {

        return $this->getActions()[$httpCode]['arguments'] ?? [];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method automatically determines to send a template for display,
     * & or a controller with a method
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $httpCode
     *
     * @return array
     */
    protected function getViewWithArguments(int $httpCode): array
    {

        $action = $this->getActions()[$httpCode];

        if (array_key_exists('OR', $action)) {
            return [
                'view' => $action['OR']['view'],
                'arguments' => $action['OR']['arguments'] ?? []
            ];
        }
        return [];

    }

}