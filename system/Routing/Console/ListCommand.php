<?php

namespace System\Routing\Console;

use Kernel\Traits\RouterCallTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use System\Routing\Router;

/**
 * Class ListCommand
 * @package System\Routing\Console
 *
 * @author  Codememory
 */
class ListCommand extends Command
{

    use RouterCallTrait;

    const NUMBER_BY_PAGE = 7;

    /**
     * @var array
     */
    private array $routes = [];

    protected function configure()
    {

        $this
            ->setName('route:list')
            ->addOption(
                'method',
                'm',
                InputOption::VALUE_REQUIRED,
                'Выборка маршрутов по определенному методу'
            )
            ->addOption(
                'number',
                null,
                InputOption::VALUE_REQUIRED,
                'Выбрать определеное кол-во маршрутов'
            )
            ->addOption(
                'open-page',
                'o',
                InputOption::VALUE_REQUIRED,
                'Открыть страницу с маршрутами'
            )
            ->setDescription('Список маршрутов');

    }

    /**
     * @param string $method
     *
     * @return array
     */
    private function byMethod(string $method): array
    {

        $routes = [];

        foreach ($this->routes as $key => $route) {
            if (preg_match(sprintf('/^%s\-\d+$/', $method), $key)) {
                $routes[$key] = $route;
            }
        }

        return $routes;

    }

    private function collectingRouteKeys(): void
    {

        if ($this->routes !== []) {
            $routes = $this->routes;
            $this->routes = [];

            foreach ($routes as $method => $route) {
                if ($route !== []) {
                    foreach ($route as $index => $routeData) {
                        $this->routes[sprintf('%s-%s', $method, $index)] = $routeData;
                    }
                }
            }
        }

    }

    /**
     * @param int          $optionNumber
     * @param SymfonyStyle $style
     * @param array        $routes
     *
     * @return array
     */
    private function handleOptionNumber(int $optionNumber, SymfonyStyle $style, array $routes): array
    {

        if ($optionNumber < 1) {
            $style
                ->error('Опция --number должна содержать значение больше 0');
        } else {
            $numberRoutes = [];

            for ($i = 0; $i <= ($optionNumber - 1); $i++) {
                $keys = array_keys($routes);
                if (array_key_exists($i, $keys)) {
                    $numberRoutes[$keys[$i]] = $routes[$keys[$i]];
                }
            }

            $routes = $numberRoutes;
            $this->routes = $routes;
        }

        return $routes;

    }

    /**
     * @param int   $startWith
     * @param int   $endOn
     * @param array $routesKeys
     * @param array $routes
     *
     * @return array
     */
    private function handlePages(int $startWith, int $endOn, array $routesKeys, array $routes): array
    {

        $arrayDataForTable = [];

        for ($i = $startWith; $i <= $endOn; $i++) {
            if (array_key_exists($i, $routesKeys)) {
                $index = $routesKeys[$i];
                $avails = [];
                $params = [];
                $regex = [];
                $status = [];

                if ($routes[$index]->getWith() !== []) {
                    foreach ($routes[$index]->getWith() as $name => $data) {
                        $params[] = $name;
                        $regex[] = $data['regex'];
                        $status[] = $data['required'] ? 'Yes' : 'No';
                    }
                }

                if ($routes[$index]->route['avails'] !== []) {
                    foreach ($routes[$index]->route['avails'] as $avail) {
                        $avails[] = sprintf('%s:%s', $avail['avail'], $avail['method']);
                    }
                }

                $arrayDataForTable[] = [
                    $routes[$index]->route['route'],
                    $routes[$index]->route['method'],
                    implode(',' . PHP_EOL, $params) ?: '--',
                    implode(',' . PHP_EOL, $regex) ?: '--',
                    implode(',' . PHP_EOL, $status) ?: '--',
                    $routes[$index]->name,
                    implode(', ', $avails) ?: '--',
                ];
                $arrayDataForTable[] = new TableSeparator();
            }
        }

        return $arrayDataForTable;

    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $this->scanningFolderRoutes();
        $this->routes = Router::getRoutes();
        $this->collectingRouteKeys();

        $style = new SymfonyStyle($input, $output);
        $table = new Table($output);

        $routes = $this->routes;
        $optionMethod = strtoupper($input->getOption('method'));
        $optionNumber = (int)$input->getOption('number');
        $openedPage = (int)$input->getOption('open-page') ?: 1;

        if ($optionMethod) {
            $routes = $this->byMethod($optionMethod);
        }
        if ($optionNumber) {
            $routes = $this->handleOptionNumber($optionNumber, $style, $routes);
        }

        $numberPages = (int)ceil(count($routes) / self::NUMBER_BY_PAGE);
        $numberPages = $numberPages === 0 ? 1 : $numberPages;
        $openedPage = ($openedPage > $numberPages) ? $numberPages : $openedPage;
        $startWith = ($openedPage * self::NUMBER_BY_PAGE) - self::NUMBER_BY_PAGE;
        $endOn = ($openedPage * self::NUMBER_BY_PAGE) - 1;
        $routesKeys = array_keys($routes);

        $arrayDataForTable = $this->handlePages($startWith, $endOn, $routesKeys, $routes);

        array_pop($arrayDataForTable);

        $table->setColumnWidths([15, 5, 10, 10, 17, 10, 5]);
        $table->setStyle('box');
        $table
            ->setHeaders([
                'route', 'method', 'params', 'regex param', 'required param', 'route name', 'avails'
            ])
            ->setRows($arrayDataForTable);
        $table->setFooterTitle(sprintf('Page %s/%s', $openedPage, $numberPages));
        $table->render();

        return Command::SUCCESS;
    }

}