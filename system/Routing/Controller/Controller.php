<?php

namespace System\Routing\Controller;

use Kernel\Service\Exceptions\{
    ExpanderNotImplementedException,
    RegularServiceException,
    ServiceNotCreatedException,
    ServiceNotImplementedException
};
use Kernel\Service\Providers\{
    ConfigProvider,
    EnvProvider,
    RequestProvider,
    ResponseProvider,
    HeaderProvider,
    FindProvider,
    CookieProvider,
    DateProvider,
    TimeProvider
};
use Kernel\Service\ServiceProvider;
use System\FileSystem\Different\Services\{
    FileEditorProvider,
    FileInformationProvider,
    FileIsProvider,
    FileProvider,
    YamlProvider
};
use System\Routing\Exceptions\ControllerException;

/**
 * @property FileProvider            $file
 * @property YamlProvider            $yaml
 * @property ResponseProvider        $response
 * @property HeaderProvider          $header
 * @property FileInformationProvider $fileInfo
 * @property FindProvider            $find
 * @property FileEditorProvider      $fileEditor
 * @property FileIsProvider          $fileIs
 * @property RequestProvider         $request
 * @property EnvProvider             $env
 * @property ConfigProvider          $config
 * @property CookieProvider          $cookie
 * @property DateProvider            $date
 * @property TimeProvider            $time
 *
 * Class Controller
 * @package System\Routing\Controller
 *
 * @author  Codememory
 */
class Controller
{

    /**
     * @var ServiceProvider
     */
    private ServiceProvider $services;

    /**
     * Controller constructor.
     *
     * @param ServiceProvider $services
     *
     * @throws ServiceNotCreatedException
     * @throws \ReflectionException|ExpanderNotImplementedException
     */
    public function __construct(ServiceProvider $services)
    {

        $this->services = $services;
        $this->services->collect();

    }

    public function getAvails()
    {

        // TODO:

    }

    /**
     * @param $property
     *
     * @return object
     * @throws ControllerException
     * @throws RegularServiceException
     * @throws ServiceNotImplementedException
     */
    public function __get($property): object
    {

        if (!array_key_exists($property, $this->services->getAllServices())) {
            throw new ControllerException(sprintf(
                'Property | service provider <b>%s</b> not found',
                $property
            ));
        }

        return $this->services->getService($property);

    }

    /**
     * @param string $method
     * @param array  $arguments
     *
     * @throws ControllerException
     */
    public function __call(string $method, array $arguments = []): void
    {

        throw new ControllerException(
            sprintf('<b>%s::%s()</b> method does not exist.',
                get_class($this), $method)
        );

    }

}