<?php

namespace System\Routing\Exceptions;

use JetBrains\PhpStorm\Pure;

/**
 * Class IncorrectSequenceParametersException
 * @package System\Routing\Exceptions
 *
 * @author  Codememory
 */
class IncorrectSequenceParametersException extends \ErrorException
{

    /**
     * IncorrectSequenceParametersException constructor.
     */
    #[Pure] public function __construct()
    {
        parent::__construct(<<<ERROR
The sequence of parameters is not correct. 
An optional parameter is specified before the required'
ERROR
        );
    }

}