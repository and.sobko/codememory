<?php

namespace System\Routing\Exceptions;

use JetBrains\PhpStorm\Pure;

/**
 * Class InvalidParameterNameException
 * @package System\Routing\Exceptions
 *
 * @author  Codememory
 */
class InvalidParameterNameException extends \ErrorException
{

    /**
     * @var string
     */
    private $parameter;

    /**
     * InvalidParameterNameException constructor.
     *
     * @param string $parameter
     */
    #[Pure] public function __construct(string $parameter)
    {

        parent::__construct(sprintf('The parameter name for the route is incorrect. The %s parameter does not exist, or is incorrectly specified', $parameter));

        $this->parameter = $parameter;

    }

    /**
     * @return string|null
     */
    public function getParameterName(): ?string
    {

        return $this->parameter;

    }

}