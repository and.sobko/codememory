<?php

namespace System\Routing\Exceptions;

use JetBrains\PhpStorm\Pure;

/**
 * Class RouteNameExistsException
 * @package System\Routing\Exceptions
 *
 * @author  Codememory
 */
class RouteNameExistsException extends \ErrorException
{

    /**
     * @var string
     */
    private $routeName;

    /**
     * RouteNameExistsException constructor.
     *
     * @param string $routeName
     */
    #[Pure] public function __construct(string $routeName)
    {

        parent::__construct(sprintf('The name %s for the route already exists. Please enter another name', $routeName));
        $this->routeName = $routeName;

    }

    /**
     * @return string
     */
    public function getRouteName(): string
    {

        return $this->routeName;

    }

}