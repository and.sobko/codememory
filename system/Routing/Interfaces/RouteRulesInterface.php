<?php

namespace System\Routing\Interfaces;

/**
 * Interface RouteRulesInterface
 * @package System\Routing\Interfaces
 *
 * @author  Codememory
 */
interface RouteRulesInterface
{

    const NUMERIC = '([0-9]+)';
    const FLOAT = '([0-9]+\.[0-9]+)';
    const SMALL_STRING = '([a-z]+)';
    const BIG_STRING = '([A-Z]+)';
    const ALPHA = '([a-zA-Z]+)';
    const ALPHA_NUMERIC = '([a-zA-Z0-9]+)';
    const ALPHA_DASH = '([a-zA-Z0-9\-_.]+)';
    const ONLY_ONE_NUMERIC = '([0-9]{1})';

}