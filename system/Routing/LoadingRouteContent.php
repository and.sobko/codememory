<?php

namespace System\Routing;

use System\Components\DI\Interfaces\ElementsInterface;

/**
 * Class LoadingRouteContent
 * @package System\Routing
 *
 * @author  Codememory
 */
class LoadingRouteContent
{

    /**
     * @var array
     */
    private array $arguments;

    /**
     * LoadingRouteContent constructor.
     *
     * @param array $arguments
     */
    public function __construct(array $arguments)
    {

        $this->arguments = $arguments;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Loading a controller and method via a DI container along a specific route
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param ElementsInterface $element
     * @param string            $controllerNamespaces
     * @param string            $method
     *
     * @return mixed
     */
    public function controllerMethod(ElementsInterface $element, string $controllerNamespaces, string $method): mixed
    {

        $name = sprintf('%s-%s', $controllerNamespaces, $method);

        return $element
            ->setName($name)
            ->setObject($controllerNamespaces)
            ->setMethod($method)
            ->addArguments($this->arguments)
            ->collect()
            ->execute()
            ->get($name);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Loading a callback function via a DI container
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param ElementsInterface $element
     * @param callable          $callback
     *
     * @return mixed
     */
    public function callback(ElementsInterface $element, callable $callback): mixed
    {

        $name = sprintf('callbackRoute-%s-%s', rand(10000, 99999), rand(10000, 99999));

        return $element
            ->setName($name)
            ->setCallback($callback)
            ->addArguments($this->arguments)
            ->collect()
            ->execute()
            ->get($name);

    }

    public function view(string $view)
    {

        // TODO:

    }

}