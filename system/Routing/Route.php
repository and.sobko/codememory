<?php

namespace System\Routing;

use System\Components\DI\DIElements\FunctionDependencies;
use System\Components\DI\DIElements\ObjectDependency;
use System\Routing\Exceptions\IncorrectSequenceParametersException;
use System\Routing\Exceptions\InvalidParameterNameException;
use System\Routing\Interfaces\RouteInterface;
use System\Routing\Traits\WithRouteParameters;
use System\Support\Arrays\Arr;
use Url;

/**
 * Class Route
 * @package System\Routing
 *
 * @author  Codememory
 */
class Route extends RouteAssembly
{

    use WithRouteParameters;

    /**
     * @var array
     */
    public array $route;

    /**
     * @var array
     */
    private array $routes;

    /**
     * @var array
     */
    protected array $with = [];

    /**
     * @var array
     */
    public array $routeInformation = [];

    /**
     * @var string|null
     */
    public ?string $name = null;

    /**
     * @var Arr
     */
    protected Arr $array;

    /**
     * Route constructor.
     *
     * @param string $route
     * @param string $method
     * @param        $callback
     * @param array  $avails
     */
    public function __construct(string $route, string $method, $callback, array $avails)
    {

        $this->route = [
            'route'    => $route,
            'method'   => $method,
            'callback' => $callback,
            'avails'   => $avails
        ];

        $this->array = new Arr();

    }

    /**
     * @param string $parameterName
     *
     * @return bool
     */
    private function existsParameter(string $parameterName): bool
    {

        $route = $this->route['route'];
        $exists = false;

        if ($this->findParameters($route) !== []) {
            foreach ($this->findParameters($route) as $parameter) {
                if ($parameter['nameParameter'] === $parameterName) {
                    $exists = true;
                }
            }
        }

        return $exists;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method is used to process any parameters in url [@parameter]
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $parameterName
     * @param string|null $rules
     * @param bool        $required
     * @param string|null $defaultValue
     *
     * @return $this
     * @throws IncorrectSequenceParametersException
     * @throws InvalidParameterNameException
     */
    public function with(string $parameterName, ?string $rules, bool $required = true, ?string $defaultValue = null): object
    {

        $rules = $rules === null ? RouteInterface::DEFAULT_RULES : $rules;

        if ($this->existsParameter($parameterName) === false) {
            throw new InvalidParameterNameException($parameterName);
        }

        $this->with[$parameterName] = [
            'regex'    => $rules,
            'required' => $required,
            'default'  => $defaultValue
        ];

        $this->parameterSequence();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method adds a name to the route. And this name is not just for beauty, but in order to make it
     * & easier to refer to the route. For example getting url. And if suddenly url needs to be changed, then
     * & this will be done only when creating a route
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return object
     */
    public function name(string $name): object
    {

        $this->name = $name;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The avail method is used to determine the type of access to a route or group. Example: there is a
     * & route / my-products and this url should only be available if the user is authenticated. Several
     * & "avails" can be specified in the line there is an optional separator [:] it specifies which method to
     * & run. Example with one avail $ avails = "UserAuth: checkUser"
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param mixed ...$avails
     *
     * @return $this
     */
    public function avail(...$avails): object
    {

        foreach ($avails as $avail) {
            if (!preg_match('/(?<avail>[a-zA-Z_]+)+:(?<method>[a-zA-Z_]+)/', $avail, $match)) {
                $this->route['avails'][] = [
                    'avail'  => $avail,
                    'method' => 'handle'
                ];
            } else {
                $this->route['avails'][] = [
                    'avail'  => $match['avail'],
                    'method' => $match['method']
                ];
            }
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method will remove all avails from the route
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     */
    public function cancelAvails(): object
    {

        $this->route['avails'] = [];

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method removes avail from the route by name
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return object
     */
    public function cancelAvailByName(string $name): object
    {

        if ($this->route['avails'] !== []) {
            foreach ($this->route['avails'] as $index => $avail) {
                if ($avail['avail'] === $name) {
                    unset($this->route['avails'][$index]);
                }
            }
        }

        return $this;

    }

    /**
     * @param string $package
     *
     * @return array
     */
    private function separateControllerAndMethod(string $package): array
    {

        if (preg_match('/(?<controller>[^#]+)#(?<method>[^.]+)/', $package, $matches)) {
            return [
                'controller' => str_replace('.', '\\', $matches['controller']),
                'method'     => $matches['method']
            ];
        }

        return [];

    }

    /**
     * @param array $matches
     * @param array $route
     */
    private function savingRouteInformation(array $matches, array $route): void
    {

        $this->routeInformation = [
            'parameters' => $this->parametersBreakdown($matches, $route),
            'callback'   => $this->route['callback'],
            'avails'     => $this->route['avails']
        ];

    }

    /**
     * @return bool
     */
    private function match(): bool
    {

        $findParameters = $this->findParameters($this->route['route']);
        $route = $this->generateRoute($this->route['route'], $findParameters);

        if (preg_match($route['route'], Url::current(), $matches)) {
            $this->savingRouteInformation($matches, $route);

            return true;
        }

        return false;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Loading the loading method that will load the callback
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param LoadingRouteContent $loading
     *
     * @return mixed
     */
    private function callback(LoadingRouteContent $loading): mixed
    {

        return $loading->callback(
            new FunctionDependencies(),
            $this->routeInformation['callback']
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Loading the loading method which will load the controller and the method
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param LoadingRouteContent $loading
     *
     * @return mixed
     */
    private function controller(LoadingRouteContent $loading): mixed
    {

        $controllerAndMethod = $this->separateControllerAndMethod($this->routeInformation['callback']);

        return $loading->controllerMethod(
            new ObjectDependency(),
            $controllerAndMethod['controller'],
            $controllerAndMethod['method']
        );

    }

    /**
     * @param LoadingRouteContent $loading
     *
     * @return mixed
     */
    private function loadingCall(LoadingRouteContent $loading): mixed
    {

        if (is_callable($this->routeInformation['callback'])) {
            return $this->callback($loading);
        } else {
            return $this->controller($loading);
        }

    }

    /**
     * @throws \ReflectionException
     */
    private function processingAvails(): void
    {

        $avails = $this->routeInformation['avails'];
        $availsObject = new RouteAvail($avails, $this->routeInformation);

        if ($avails !== []) {
            $availsObject->request();
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The call method collects the entire route and is called in the main Router class
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function call(): mixed
    {

        if ($this->match() === true) {
            $loading = new LoadingRouteContent($this->routeInformation['parameters']);

            $this->processingAvails();

            return $this->loadingCall($loading);
        }

        return false;

    }

    /**
     * @return array
     */
    public function getWith(): array
    {

        return $this->with;

    }


}