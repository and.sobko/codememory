<?php

namespace System\Routing;

use System\Routing\Exceptions\IncorrectSequenceParametersException;
use System\Routing\Interfaces\RouteInterface;

/**
 * Class RouteAvail
 * @package System\Routing
 *
 * @author  Codememory
 */
class RouteAssembly
{

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method iterates over the route and creates a new array with a sequence of parameters
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     * @throws IncorrectSequenceParametersException
     */
    protected function parameterSequence(): bool
    {

        $lastValue = true;
        $quantityIsNotCorrect = 0;

        foreach ($this->with as $with) {
            if ($with['required'] !== $lastValue) {
                $quantityIsNotCorrect++;
            }

            $lastValue = $with['required'];
        }

        if ($quantityIsNotCorrect > 1) {
            throw new IncorrectSequenceParametersException();
        }

        return $lastValue;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Search for parameters in the route, if found then a new array with information
     * & about the parameter is added
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $route
     *
     * @return array
     */
    protected function findParameters(string $route): array
    {

        $parameters = [];

        if (preg_match_all('/@(?<params>[a-z0-9_\-.]+)/', $route, $matches)) {
            foreach ($matches['params'] as $index => $param) {
                $parameters[$param] = [
                    'nameParameter'     => $param,
                    'fullNameParameter' => sprintf('@%s', $param),
                    'position'          => $index,
                    'rules'             => $this->with[$param]['regex'] ?? null,
                    'isRequired'        => $this->with[$param]['required'] ?? false,
                    'defaultValue'      => $this->with[$param]['default'] ?? null
                ];
            }
        }

        return $parameters;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Fetch from array parameters only
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $parameters
     *
     * @return array
     */
    protected function selectParametersOnly(array $parameters): array
    {

        $params = [];

        if ($parameters !== []) {
            foreach ($parameters as $key => $value) {
                if (preg_match('/param_(?<name>[a-z0-9_\-.]+)/', $key, $match)) {
                    $params[$match['name']] = $value;
                }
            }
        }

        return $params;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method collects an array of the route with its information
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $param
     * @param array  $nameParam
     * @param array  $parameters
     * @param string $route
     *
     * @return string
     */
    private function routeAssembly(string $param, array $nameParam, array $parameters, string $route): string
    {

        $fullNameParameter = $nameParam['fullNameParameter'];

        if (array_key_exists($param, $this->with) === false) {
            $route = str_replace($fullNameParameter, RouteInterface::DEFAULT_RULES, $route);
        } else {
            $route = $this->routeAssemblyParamExists(
                $param, $nameParam, $fullNameParameter, $parameters, $route
            );
        }

        return $route;

    }

    /**
     * @param string $param
     * @param array  $nameParam
     * @param string $fullNameParameter
     * @param array  $parameters
     * @param string $route
     *
     * @return string|string[]
     */
    private function routeAssemblyParamExists(string $param, array $nameParam, string $fullNameParameter, array $parameters, string $route): ?string
    {

        $nameParameter = $nameParam['nameParameter'];
        $rules = $parameters[$param]['rules'];

        if ($nameParam['isRequired'] === false) {
            return str_replace(
                $fullNameParameter . '\/',
                sprintf('(?<param_%s>%s)?(\/)?', $nameParameter, $rules), $route
            );
        } else {
            return str_replace(
                $fullNameParameter, sprintf('(?<param_%s>%s)', $nameParameter, $rules),
                $route
            );
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Route renderer with regular expression to compare links
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string     $route
     * @param array|null $parameters
     *
     * @return array
     */
    protected function generateRoute(string $route, ?array $parameters = []): array
    {

        $route = preg_quote($route, '/');

        if (preg_match_all('/@(?<paramName>[a-z0-9_\-.]+)/', $route, $match)) {
            foreach ($match['paramName'] as $param) {
                $nameParam = $parameters[$param];
                $route = $this->routeAssembly($param, $nameParam, $parameters, $route);
            }
        }

        return [
            'parameters' => $match['paramName'],
            'route'      => sprintf('/^%s$/', $route)
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns an array of all optional parameters for the route
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    protected function getParametersNotRequired(): array
    {

        $params = [];

        if ($this->with !== []) {
            foreach ($this->with as $name => $data) {
                if ($data['required'] === false) {
                    $params[$name] = $data['default'];
                }
            }
        }

        return $params;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method breaks down parameters
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $matches
     * @param array $route
     *
     * @return array
     */
    protected function parametersBreakdown(array $matches, array $route): array
    {

        $parameters = $this->selectParametersOnly($matches);

        return $this->array->arrayDiffKeyAdding($parameters, array_flip($route['parameters']), $this->getParametersNotRequired());

    }

}