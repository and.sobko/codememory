<?php

namespace System\Routing;

use JetBrains\PhpStorm\Pure;
use Request;
use Response;

/**
 * Class Router
 * @package System\Routing
 *
 * @author  Codememory
 */
class Router
{

    /**
     * @var array
     */
    public static array $routes = [];

    /**
     * @var ?string
     */
    private static ?string $group = null;

    /**
     * @var array
     */
    public static array $names = [];

    /**
     * @var array
     */
    public static array $avail = [];

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The get method says that the request method for this route must be strictly GET
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $route
     * @param        $callable
     *
     * @return Route
     */
    public static function get(string $route, $callable): Route
    {

        return self::add($route, 'GET', $callable);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The post method says that the request method for this route must be strictly POST
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $route
     * @param        $callable
     *
     * @return Route
     */
    public static function post(string $route, $callable): Route
    {

        return self::add($route, 'POST', $callable);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The put method says that the request method for this route must be strictly PUT
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $route
     * @param        $callable
     *
     * @return Route
     */
    public static function put(string $route, $callable): Route
    {

        return self::add($route, 'PUT', $callable);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The delete method says that the request method for this route must be strictly DELETE
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $route
     * @param        $callable
     *
     * @return Route
     */
    public static function delete(string $route, $callable): Route
    {

        return self::add($route, 'DELETE', $callable);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The any method means that the request method for this route can be any
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $route
     * @param        $callable
     *
     * @return Route
     */
    public static function any(string $route, $callable): Route
    {

        return self::add($route, 'GET|POST|PUT|DELETE', $callable);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     *                              [ For all routes inside callback ]
     * & The avail method is used to determine the type of access to a route or group. Example: there is a
     * & route / my-products and this url should only be available if the user is authenticated. Several
     * & "avails" can be specified in the line there is an optional separator [:] it specifies which method to
     * & run. Example with one avail $ avails = "UserAuth: checkUser"
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param callable $callback
     * @param mixed    ...$avails
     *
     * @return bool
     */
    public static function avail(callable $callback, ...$avails): bool
    {

        foreach ($avails as $avail) {
            if (!preg_match('/(?<avail>[a-zA-Z_]+)+:(?<method>[a-zA-Z_]+)/', $avail, $match)) {
                self::$avail[] = [
                    'avail'  => $avail,
                    'method' => 'handle'
                ];
            } else {
                self::$avail[] = [
                    'avail'  => $match['avail'],
                    'method' => $match['method']
                ];
            }
        }

        call_user_func($callback);

        self::$avail = [];

        return true;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The group method is needed to create prefixed routes. That is, if you need to create 3
     * & routes, a link that should start / test, then the group comes to the rescue for this
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string   $prefix
     * @param callable $callback
     *
     * @return bool
     */
    public static function group(string $prefix, callable $callback): bool
    {

        self::$group = sprintf('%s%s%s', self::$group, '/', trim($prefix, '/'));

        call_user_func($callback);

        self::$group = null;

        return true;

    }

    /**
     * @param string $route
     *
     * @return string
     */
    #[Pure] private static function routeLinkFormation(string $route): string
    {

        if (self::$group === null && $route === '/') {
            return '/';
        } else {
            return sprintf('%s/%s/', self::$group, trim($route, '/'));
        }

    }

    /**
     * @param Route  $Route
     * @param string $method
     */
    private static function addingRoutesHandler(Route $Route, string $method): void
    {

        $methods = explode('|', $method);

        foreach ($methods as $method) {
            self::$routes[$method][] = $Route;
        }

    }

    /**
     * @param string $route
     * @param string $method
     * @param        $callable
     *
     * @return Route
     */
    private static function add(string $route, string $method, $callable): Route
    {

        $Route = new Route(
            self::routeLinkFormation($route), $method, $callable, self::$avail
        );

        self::addingRoutesHandler($Route, $method);

        return $Route;

    }

    /**
     * @param string $name
     *
     * @return bool
     */
    #[Pure] private static function existsRouteName(string $name): bool
    {

        if (array_key_exists($name, self::$names) === true) {
            return true;
        }

        return false;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns information about the route by its name
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @param array  $parameters
     *
     * @return array|null
     */
    public static function getRoute(string $name, array $parameters = []): ?string
    {

        $route = self::parametersAreNotEmpty($parameters, self::$names[$name]['path']);

        if ([] !== $parameters) {
            $route = preg_replace('/@[a-z0-9_\-.]+(\/)?/', '', $route);
        }

        return $route;

    }

    /**
     * @param array $parameters
     * @param       $route
     *
     * @return string|null
     */
    private static function parametersAreNotEmpty(array $parameters, $route): ?string
    {

        if ($parameters !== []) {
            foreach ($parameters as $parameter => $value) {
                if (preg_match(sprintf('/@%s/', $parameter), $route)) {
                    $route = preg_replace(sprintf('/@%s/', $parameter), $value, $route);
                }
            }
        }

        return $route;

    }

    /**
     * @param bool $status
     */
    private static function responseNotFound(bool $status): void
    {

        if ($status === false) {
            // Response 404
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Adding route names to array
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param Route $route
     */
    private static function addingNames(Route $route): void
    {

        if ($route->name !== null) {
            self::$names[$route->name] = [
                'path' => $route->route['route']
            ];
        }

    }

    /**
     * @return array
     */
    public static function getRoutes(): array
    {

        return self::$routes;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The most important method that processes and runs the entire chain of router metes
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     */
    public static function callRouting(): void
    {

        $status = false;
        $method = Request::method();

        foreach (self::$routes as $key => $methods) {
            foreach ($methods as $route) {
                if ($key === $method) {
                    if ($route->call() !== false) {
                        $status = true;
                    }
                }

                self::addingNames($route);
            }
        }

        self::responseNotFound($status);

    }

}