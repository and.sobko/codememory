<?php

namespace System\Support\Arrays;

/**
 * Class ArrayWork
 * @package System\Support\Arrays
 *
 * @author Codememory
 */
class Arr
{

    /**
     * @param array $main
     * @param array $substitute
     * @param mixed ...$substituteKeys
     *
     * @return array
     */
    public function replaceArrayValue(array $main, array $substitute, ...$substituteKeys): array
    {

        $newMain = [];
        $newSubstitute = [];
        $arrayData = [];
        $returnData = [];

        if($substituteKeys !== [] && array_key_exists(0, $substituteKeys) && (is_array($substituteKeys[0]))) $substituteKeys = $substituteKeys[0];

        foreach($main as $value) $newMain[] = $value;
        foreach($substitute as $value) $newSubstitute[] = $value;

        for($i = 0; $i <= count($newMain); $i++)
        {
            if(array_key_exists($i, $newSubstitute)) {
                $arrayData['main'][] = $newMain[$i];
                $arrayData['substitute'][] = $newSubstitute[$i];
            }
        }

        foreach($arrayData['main'] as $key => $valueData)
        {
            $value = (empty($valueData) || $valueData == '') ? $arrayData['substitute'][$key] : $valueData;

            if($substituteKeys !== []) {
                if(array_key_exists($key, $substituteKeys)) $returnData[$substituteKeys[$key]] = $value;
                else $returnData[] = $value;
            } else {
                $returnData[] = $value;
            }
        }

        return $returnData;

    }

    /**
     * @param array $array
     * @param array $comparisonArray
     * @param array $values
     *
     * @return array
     */
    public function arrayDiffKeyAdding(array $array, array $comparisonArray, array $values = []): array
    {

        if(count($comparisonArray) < count($array)) {
            echo 'error';
        }

        foreach ($comparisonArray as $keyComp => $valueComp)
        {
            if(array_key_exists($keyComp, $array) === false) {
                $array[$keyComp] = $values[$keyComp] ?? null;
            }
        }

        return $array;

    }

}

