<?php

namespace System\Support\DateTime;

use System\Support\Exceptions\DateTime\InvalidTimezoneException;

/**
 * Class Time
 * @package System\Support\DateTime
 *
 * @author  Codememory
 */
class Time extends DateTime implements DateTimeInterface
{

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns a date in a timestamp
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     * @throws InvalidTimezoneException
     */
    public function get(): int
    {

        return $this->datetime()->getTimestamp();
    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns the current date at a timestamp
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int|string
     * @throws InvalidTimezoneException
     */
    public function now(): int|string
    {

        return $this->datetime('now')->getTimestamp();

    }

}