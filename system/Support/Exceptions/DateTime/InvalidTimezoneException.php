<?php

namespace System\Support\Exceptions\DateTime;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class InvalidTimezoneException
 * @package System\Support\Exceptions\DateTime
 *
 * @author  Codememory
 */
class InvalidTimezoneException extends ErrorException
{

    /**
     * InvalidTimezoneException constructor.
     *
     * @param string|null $timezone
     */
    #[Pure] public function __construct(
        private ?string $timezone
    )
    {

        parent::__construct(
            sprintf('The time zone is incorrect. Currently the temporary zone is: <b>%s</b>', $this->timezone)
        );

    }

    /**
     * @return string|null
     */
    public function getTimezone(): ?string
    {

        return $this->timezone;

    }

}