const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const pathBuild = path.resolve(__dirname, 'src/Build/');
const pathAssets = './src/Assets/';

module.exports = {
    entry: pathAssets + 'js/app.js',
    output: {
        publicPath: '',
        path: pathBuild,
        filename: 'app.[contenthash].js'
    },
    plugins: [
        new CleanWebpackPlugin()
    ]
};